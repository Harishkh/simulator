package com.simulator.dao;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bringup_upgrade")
public class BringupUpgradeCommands implements java.io.Serializable {

	private Integer bringUpgradeCommandsId;
	private Boolean chmodUpgradeFile;
	private Boolean tarExtractUpgradeFile;
	private Boolean cpConfig;
	private Boolean cpStartup;
	private Boolean cpHariff;
	
	private String cpConfigvalue;
	private String cpStartupvalue;
	private String cpHariffvalue;
	
	private String fsetenvBwvalue;
	private String fsetenvIpaddrvalue;
	private String fsetenvGtpipvalue;
	private String fsetenvNetmaskvalue;
	private String fsetenvGatewayipvalue;
	private String fsetenvStartepcvalue;
	
	private Boolean chmodDb;
	private Boolean chmodUpgradeFolder;
	private Boolean chmodConfig;
	private Boolean mvUpgradeFile;
	private Boolean mountMntMnc;
	private Boolean fsetenvBw;
	private Boolean fsetenvIpaddr;
	private Boolean fsetenvGtpip;
	private Boolean fsetenvNetmask;
	private Boolean fsetenvGatewayip;
	private Boolean fsetenvStartepc;
	private Boolean fileRfconfig;
	private Boolean umountMntMnc;
	private Boolean mkdirDb;
	private Boolean sync;
	
	private Timestamp requestTime;
	private Timestamp responseTime;
	private String commandStatus;
	
	private AuditLog auditLog;

	public BringupUpgradeCommands() {
	}

	public BringupUpgradeCommands(Integer bringUpgradeCommandsId, Boolean chmodUpgradeFile,
			Boolean tarExtractUpgradeFile, Boolean cpConfig, Boolean cpStartup, Boolean cpHariff, Boolean chmodDb,
			Boolean chmodUpgradeFolder, Boolean chmodConfig, Boolean mvUpgradeFile, Boolean mountMntMnc,
			Boolean fsetenvBw, Boolean fsetenvIpaddr, Boolean fsetenvGtpip, Boolean fsetenvNetmask,
			Boolean fsetenvGatewayip, Boolean fsetenvStartepc, Boolean fileRfconfig, Boolean umountMntMnc,
			Boolean mkdirDb, Boolean sync, Timestamp requestTime, Timestamp responseTime, String commandStatus,
			AuditLog auditLog, String cpConfigvalue,String cpStartupvalue,String cpHariffvalue,
			 String fsetenvBwvalue,
	 String fsetenvIpaddrvalue,
	 String fsetenvGtpipvalue,
	 String fsetenvNetmaskvalue,
	 String fsetenvGatewayipvalue,
	 String fsetenvStartepcvalue
			
			
			) {
		super();
		this.bringUpgradeCommandsId = bringUpgradeCommandsId;
		this.chmodUpgradeFile = chmodUpgradeFile;
		this.tarExtractUpgradeFile = tarExtractUpgradeFile;
		this.cpConfig = cpConfig;
		this.cpStartup = cpStartup;
		this.cpHariff = cpHariff;
		this.chmodDb = chmodDb;
		this.chmodUpgradeFolder = chmodUpgradeFolder;
		this.chmodConfig = chmodConfig;
		this.mvUpgradeFile = mvUpgradeFile;
		this.mountMntMnc = mountMntMnc;
		this.fsetenvBw = fsetenvBw;
		this.fsetenvIpaddr = fsetenvIpaddr;
		this.fsetenvGtpip = fsetenvGtpip;
		this.fsetenvNetmask = fsetenvNetmask;
		this.fsetenvGatewayip = fsetenvGatewayip;
		this.fsetenvStartepc = fsetenvStartepc;
		this.fileRfconfig = fileRfconfig;
		this.umountMntMnc = umountMntMnc;
		this.mkdirDb = mkdirDb;
		this.sync = sync;
		this.requestTime = requestTime;
		this.responseTime = responseTime;
		this.commandStatus = commandStatus;
		this.auditLog = auditLog;
		
		this.cpConfigvalue=cpConfigvalue;
		this.cpHariffvalue=cpHariffvalue;
		this.cpStartupvalue=cpStartupvalue;
		
		this.fsetenvBwvalue=fsetenvBwvalue;
		this.fsetenvGatewayipvalue=fsetenvGatewayipvalue;
		this.fsetenvGtpipvalue=fsetenvGtpipvalue;
		this.fsetenvIpaddrvalue=fsetenvIpaddrvalue;
		this.fsetenvNetmaskvalue=fsetenvNetmaskvalue;
		this.fsetenvStartepcvalue=fsetenvStartepcvalue;
	}



	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Bringup_Upgrade_Id", unique = true, nullable = false)
	public Integer getBringUpgradeCommandsId() {
		return bringUpgradeCommandsId;
	}

	public void setBringUpgradeCommandsId(Integer bringUpgradeCommandsId) {
		this.bringUpgradeCommandsId = bringUpgradeCommandsId;
	}

	@Column(name = "Chmod_Upgrade_File", length = 45)
	public Boolean getChmodUpgradeFile() {
		return chmodUpgradeFile;
	}

	public void setChmodUpgradeFile(Boolean chmodUpgradeFile) {
		this.chmodUpgradeFile = chmodUpgradeFile;
	}

	@Column(name = "Tar_Extract_Upgrade", length = 45)
	public Boolean getTarExtractUpgradeFile() {
		return tarExtractUpgradeFile;
	}

	public void setTarExtractUpgradeFile(Boolean tarExtractUpgradeFile) {
		this.tarExtractUpgradeFile = tarExtractUpgradeFile;
	}

	@Column(name = "Cp_Config", length = 45)
	public Boolean getCpConfig() {
		return cpConfig;
	}

	public void setCpConfig(Boolean cpConfig) {
		this.cpConfig = cpConfig;
	}

	@Column(name = "Cp_Startup", length = 45)
	public Boolean getCpStartup() {
		return cpStartup;
	}

	public void setCpStartup(Boolean cpStartup) {
		this.cpStartup = cpStartup;
	}

	@Column(name = "Cp_Hariff", length = 45)
	public Boolean getCpHariff() {
		return cpHariff;
	}

	public void setCpHariff(Boolean cpHariff) {
		this.cpHariff = cpHariff;
	}

	@Column(name = "Chmod_Db", length = 45)
	public Boolean getChmodDb() {
		return chmodDb;
	}

	public void setChmodDb(Boolean chmodDb) {
		this.chmodDb = chmodDb;
	}

	@Column(name = "Chmod_Upgrade_Folder", length = 45)
	public Boolean getChmodUpgradeFolder() {
		return chmodUpgradeFolder;
	}

	public void setChmodUpgradeFolder(Boolean chmodUpgradeFolder) {
		this.chmodUpgradeFolder = chmodUpgradeFolder;
	}

	@Column(name = "Chmod_Config", length = 45)
	public Boolean getChmodConfig() {
		return chmodConfig;
	}

	public void setChmodConfig(Boolean chmodConfig) {
		this.chmodConfig = chmodConfig;
	}

	@Column(name = "Mv_Upgrade_File", length = 45)
	public Boolean getMvUpgradeFile() {
		return mvUpgradeFile;
	}

	public void setMvUpgradeFile(Boolean mvUpgradeFile) {
		this.mvUpgradeFile = mvUpgradeFile;
	}

	@Column(name = "Mount_Mnt_Mnc", length = 45)
	public Boolean getMountMntMnc() {
		return mountMntMnc;
	}

	public void setMountMntMnc(Boolean mountMntMnc) {
		this.mountMntMnc = mountMntMnc;
	}

	@Column(name = "Fsetenv_Bw", length = 45)
	public Boolean getFsetenvBw() {
		return fsetenvBw;
	}

	public void setFsetenvBw(Boolean fsetenvBw) {
		this.fsetenvBw = fsetenvBw;
	}

	@Column(name = "Fsetenv_Ipaddr", length = 45)
	public Boolean getFsetenvIpaddr() {
		return fsetenvIpaddr;
	}

	public void setFsetenvIpaddr(Boolean fsetenvIpaddr) {
		this.fsetenvIpaddr = fsetenvIpaddr;
	}

	@Column(name = "Fsetenv_Gtpip", length = 45)
	public Boolean getFsetenvGtpip() {
		return fsetenvGtpip;
	}

	public void setFsetenvGtpip(Boolean fsetenvGtpip) {
		this.fsetenvGtpip = fsetenvGtpip;
	}

	@Column(name = "Fsetenv_Netmask", length = 45)
	public Boolean getFsetenvNetmask() {
		return fsetenvNetmask;
	}

	public void setFsetenvNetmask(Boolean fsetenvNetmask) {
		this.fsetenvNetmask = fsetenvNetmask;
	}

	@Column(name = "Fsetenv_Gatewayip", length = 45)
	public Boolean getFsetenvGatewayip() {
		return fsetenvGatewayip;
	}

	public void setFsetenvGatewayip(Boolean fsetenvGatewayip) {
		this.fsetenvGatewayip = fsetenvGatewayip;
	}

	@Column(name = "Fsetenv_Startepc", length = 45)
	public Boolean getFsetenvStartepc() {
		return fsetenvStartepc;
	}

	public void setFsetenvStartepc(Boolean fsetenvStartepc) {
		this.fsetenvStartepc = fsetenvStartepc;
	}

	@Column(name = "File_Rfconfig", length = 45)
	public Boolean getFileRfconfig() {
		return fileRfconfig;
	}

	public void setFileRfconfig(Boolean fileRfconfig) {
		this.fileRfconfig = fileRfconfig;
	}

	@Column(name = "Umount_Mnt_Mnc", length = 45)
	public Boolean getUmountMntMnc() {
		return umountMntMnc;
	}

	public void setUmountMntMnc(Boolean umountMntMnc) {
		this.umountMntMnc = umountMntMnc;
	}

	@Column(name = "Mkdir_Db", length = 45)
	public Boolean getMkdirDb() {
		return mkdirDb;
	}

	public void setMkdirDb(Boolean mkdirDb) {
		this.mkdirDb = mkdirDb;
	}

	@Column(name = "Sync", length = 45)
	public Boolean getSync() {
		return sync;
	}

	public void setSync(Boolean sync) {
		this.sync = sync;
	}
	
	@Column(name = "Request_Time", length = 45)
	public Timestamp getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(Timestamp requestTime) {
		this.requestTime = requestTime;
	}

	@Column(name = "Response_Time", length = 45)
	public Timestamp getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(Timestamp responseTime) {
		this.responseTime = responseTime;
	}

	@Column(name = "Command_Status", length = 45)
	public String getCommandStatus() {
		return commandStatus;
	}

	public void setCommandStatus(String commandStatus) {
		this.commandStatus = commandStatus;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Audit_Log_Id")
	public AuditLog getAuditLog() {
		return auditLog;
	}

	public void setAuditLog(AuditLog auditLog) {
		this.auditLog = auditLog;
	}

	public String getCpConfigvalue() {
		return cpConfigvalue;
	}

	public void setCpConfigvalue(String cpConfigvalue) {
		this.cpConfigvalue = cpConfigvalue;
	}

	public String getCpStartupvalue() {
		return cpStartupvalue;
	}

	public void setCpStartupvalue(String cpStartupvalue) {
		this.cpStartupvalue = cpStartupvalue;
	}

	public String getCpHariffvalue() {
		return cpHariffvalue;
	}

	public void setCpHariffvalue(String cpHariffvalue) {
		this.cpHariffvalue = cpHariffvalue;
	}

	public String getFsetenvBwvalue() {
		return fsetenvBwvalue;
	}

	public void setFsetenvBwvalue(String fsetenvBwvalue) {
		this.fsetenvBwvalue = fsetenvBwvalue;
	}

	public String getFsetenvIpaddrvalue() {
		return fsetenvIpaddrvalue;
	}

	public void setFsetenvIpaddrvalue(String fsetenvIpaddrvalue) {
		this.fsetenvIpaddrvalue = fsetenvIpaddrvalue;
	}

	public String getFsetenvGtpipvalue() {
		return fsetenvGtpipvalue;
	}

	public void setFsetenvGtpipvalue(String fsetenvGtpipvalue) {
		this.fsetenvGtpipvalue = fsetenvGtpipvalue;
	}

	public String getFsetenvNetmaskvalue() {
		return fsetenvNetmaskvalue;
	}

	public void setFsetenvNetmaskvalue(String fsetenvNetmaskvalue) {
		this.fsetenvNetmaskvalue = fsetenvNetmaskvalue;
	}

	public String getFsetenvGatewayipvalue() {
		return fsetenvGatewayipvalue;
	}

	public void setFsetenvGatewayipvalue(String fsetenvGatewayipvalue) {
		this.fsetenvGatewayipvalue = fsetenvGatewayipvalue;
	}

	public String getFsetenvStartepcvalue() {
		return fsetenvStartepcvalue;
	}

	public void setFsetenvStartepcvalue(String fsetenvStartepcvalue) {
		this.fsetenvStartepcvalue = fsetenvStartepcvalue;
	}

	
	
}