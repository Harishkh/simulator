package com.simulator.dao;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sendconfig")
public class SendConfigCommands implements java.io.Serializable {

	private Integer rebootId;
	private Boolean mkdirSendConfig;
	private Boolean cpConfig;
	private Boolean cpStartup;
	private Boolean chmodDb;
	private Boolean chmodSendConfig;
	private Boolean chmodConfig;
	private Boolean cpHariff;
	private Boolean mountMntMnc;
	private Boolean umountMntMnc;
	private Boolean mkdirDb;
	private Boolean sync;
	private Boolean fsetenvBw;
	private Boolean fsetenvIpaddr;
	private Boolean fsetenvGtpip;
	private Boolean fsetenvNetmask;
	private Boolean fsetenvGatewayip;
	private Boolean fsetenvStartepc;
	private Boolean dfeI2cIap;
	private Boolean cdTmp;
	private Boolean cpTmp;
	private Boolean fileConfig;
	private Boolean fileStartup;
	private Boolean fileHariff;
	private Boolean fileRfconfig;
	private Boolean fileSqliteDb;
	private Timestamp requestTime;
	private Timestamp responseTime;
	private String commandStatus;
	private AuditLog auditLog;

	public SendConfigCommands() {
	}

	public SendConfigCommands(Integer rebootId, Boolean mkdirSendConfig, Boolean cpConfig, Boolean cpStartup,
			Boolean chmodDb, Boolean chmodSendConfig, Boolean chmodConfig, Boolean cpHariff, Boolean mountMntMnc,
			Boolean umountMntMnc, Boolean mkdirDb, Boolean sync, Boolean fsetenvBw, Boolean fsetenvIpaddr,
			Boolean fsetenvGtpip, Boolean fsetenvNetmask, Boolean fsetenvGatewayip, Boolean fsetenvStartepc,
			Boolean dfeI2cIap, Boolean cdTmp, Boolean cpTmp, Boolean fileConfig, Boolean fileStartup,
			Boolean fileHariff, Boolean fileRfconfig, Boolean fileSqliteDb, Timestamp requestTime,
			Timestamp responseTime, String commandStatus, AuditLog auditLog) {
		super();
		this.rebootId = rebootId;
		this.mkdirSendConfig = mkdirSendConfig;
		this.cpConfig = cpConfig;
		this.cpStartup = cpStartup;
		this.chmodDb = chmodDb;
		this.chmodSendConfig = chmodSendConfig;
		this.chmodConfig = chmodConfig;
		this.cpHariff = cpHariff;
		this.mountMntMnc = mountMntMnc;
		this.umountMntMnc = umountMntMnc;
		this.mkdirDb = mkdirDb;
		this.sync = sync;
		this.fsetenvBw = fsetenvBw;
		this.fsetenvIpaddr = fsetenvIpaddr;
		this.fsetenvGtpip = fsetenvGtpip;
		this.fsetenvNetmask = fsetenvNetmask;
		this.fsetenvGatewayip = fsetenvGatewayip;
		this.fsetenvStartepc = fsetenvStartepc;
		this.dfeI2cIap = dfeI2cIap;
		this.cdTmp = cdTmp;
		this.cpTmp = cpTmp;
		this.fileConfig = fileConfig;
		this.fileStartup = fileStartup;
		this.fileHariff = fileHariff;
		this.fileRfconfig = fileRfconfig;
		this.fileSqliteDb = fileSqliteDb;
		this.requestTime = requestTime;
		this.responseTime = responseTime;
		this.commandStatus = commandStatus;
		this.auditLog = auditLog;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Reboot_Id", unique = true, nullable = false)
	public Integer getRebootId() {
		return rebootId;
	}

	public void setRebootId(Integer rebootId) {
		this.rebootId = rebootId;
	}

	@Column(name = "Mkdir_SendConfig", length = 45)
	public Boolean getMkdirSendConfig() {
		return mkdirSendConfig;
	}

	public void setMkdirSendConfig(Boolean mkdirSendConfig) {
		this.mkdirSendConfig = mkdirSendConfig;
	}

	@Column(name = "Cp_Config", length = 45)
	public Boolean getCpConfig() {
		return cpConfig;
	}

	public void setCpConfig(Boolean cpConfig) {
		this.cpConfig = cpConfig;
	}

	@Column(name = "Cp_Startup", length = 45)
	public Boolean getCpStartup() {
		return cpStartup;
	}

	public void setCpStartup(Boolean cpStartup) {
		this.cpStartup = cpStartup;
	}

	@Column(name = "Chmod_Db", length = 45)
	public Boolean getChmodDb() {
		return chmodDb;
	}

	public void setChmodDb(Boolean chmodDb) {
		this.chmodDb = chmodDb;
	}

	@Column(name = "Chmod_SendConfig", length = 45)
	public Boolean getChmodSendConfig() {
		return chmodSendConfig;
	}

	public void setChmodSendConfig(Boolean chmodSendConfig) {
		this.chmodSendConfig = chmodSendConfig;
	}

	@Column(name = "Chmod_Config", length = 45)
	public Boolean getChmodConfig() {
		return chmodConfig;
	}

	public void setChmodConfig(Boolean chmodConfig) {
		this.chmodConfig = chmodConfig;
	}

	@Column(name = "Cp_Hariff", length = 45)
	public Boolean getCpHariff() {
		return cpHariff;
	}

	public void setCpHariff(Boolean cpHariff) {
		this.cpHariff = cpHariff;
	}

	@Column(name = "Mount_Mnt_Mnc", length = 45)
	public Boolean getMountMntMnc() {
		return mountMntMnc;
	}

	public void setMountMntMnc(Boolean mountMntMnc) {
		this.mountMntMnc = mountMntMnc;
	}

	@Column(name = "Umount_Mnt_Mnc", length = 45)
	public Boolean getUmountMntMnc() {
		return umountMntMnc;
	}

	public void setUmountMntMnc(Boolean umountMntMnc) {
		this.umountMntMnc = umountMntMnc;
	}

	@Column(name = "Mkdir_Db", length = 45)
	public Boolean getMkdirDb() {
		return mkdirDb;
	}

	public void setMkdirDb(Boolean mkdirDb) {
		this.mkdirDb = mkdirDb;
	}

	@Column(name = "Sync", length = 45)
	public Boolean getSync() {
		return sync;
	}

	public void setSync(Boolean sync) {
		this.sync = sync;
	}

	@Column(name = "Fsetenv_Bw", length = 45)
	public Boolean getFsetenvBw() {
		return fsetenvBw;
	}

	public void setFsetenvBw(Boolean fsetenvBw) {
		this.fsetenvBw = fsetenvBw;
	}

	@Column(name = "Fsetenv_Ipaddr", length = 45)
	public Boolean getFsetenvIpaddr() {
		return fsetenvIpaddr;
	}

	public void setFsetenvIpaddr(Boolean fsetenvIpaddr) {
		this.fsetenvIpaddr = fsetenvIpaddr;
	}

	@Column(name = "Fsetenv_Gtpip", length = 45)
	public Boolean getFsetenvGtpip() {
		return fsetenvGtpip;
	}

	public void setFsetenvGtpip(Boolean fsetenvGtpip) {
		this.fsetenvGtpip = fsetenvGtpip;
	}

	@Column(name = "Fsetenv_Netmask", length = 45)
	public Boolean getFsetenvNetmask() {
		return fsetenvNetmask;
	}

	public void setFsetenvNetmask(Boolean fsetenvNetmask) {
		this.fsetenvNetmask = fsetenvNetmask;
	}

	@Column(name = "Fsetenv_Gatewayip", length = 45)
	public Boolean getFsetenvGatewayip() {
		return fsetenvGatewayip;
	}

	public void setFsetenvGatewayip(Boolean fsetenvGatewayip) {
		this.fsetenvGatewayip = fsetenvGatewayip;
	}

	@Column(name = "Fsetenv_Startepc", length = 45)
	public Boolean getFsetenvStartepc() {
		return fsetenvStartepc;
	}

	public void setFsetenvStartepc(Boolean fsetenvStartepc) {
		this.fsetenvStartepc = fsetenvStartepc;
	}

	@Column(name = "Dfe_I2c_Iap", length = 45)
	public Boolean getDfeI2cIap() {
		return dfeI2cIap;
	}

	public void setDfeI2cIap(Boolean dfeI2cIap) {
		this.dfeI2cIap = dfeI2cIap;
	}

	@Column(name = "Cd_Tmp", length = 45)
	public Boolean getCdTmp() {
		return cdTmp;
	}

	public void setCdTmp(Boolean cdTmp) {
		this.cdTmp = cdTmp;
	}

	@Column(name = "Cp_Tmp", length = 45)
	public Boolean getCpTmp() {
		return cpTmp;
	}

	public void setCpTmp(Boolean cpTmp) {
		this.cpTmp = cpTmp;
	}

	@Column(name = "File_Config", length = 45)
	public Boolean getFileConfig() {
		return fileConfig;
	}

	public void setFileConfig(Boolean fileConfig) {
		this.fileConfig = fileConfig;
	}

	@Column(name = "File_Startup", length = 45)
	public Boolean getFileStartup() {
		return fileStartup;
	}

	public void setFileStartup(Boolean fileStartup) {
		this.fileStartup = fileStartup;
	}

	@Column(name = "File_Hariff", length = 45)
	public Boolean getFileHariff() {
		return fileHariff;
	}

	public void setFileHariff(Boolean fileHariff) {
		this.fileHariff = fileHariff;
	}

	@Column(name = "File_Rfconfig", length = 45)
	public Boolean getFileRfconfig() {
		return fileRfconfig;
	}

	public void setFileRfconfig(Boolean fileRfconfig) {
		this.fileRfconfig = fileRfconfig;
	}

	@Column(name = "File_Sqlite_Db", length = 45)
	public Boolean getFileSqliteDb() {
		return fileSqliteDb;
	}

	public void setFileSqliteDb(Boolean fileSqliteDb) {
		this.fileSqliteDb = fileSqliteDb;
	}

	@Column(name = "Request_Time", length = 45)
	public Timestamp getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(Timestamp requestTime) {
		this.requestTime = requestTime;
	}

	@Column(name = "Response_Time", length = 45)
	public Timestamp getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(Timestamp responseTime) {
		this.responseTime = responseTime;
	}

	@Column(name = "Command_Status", length = 45)
	public String getCommandStatus() {
		return commandStatus;
	}

	public void setCommandStatus(String commandStatus) {
		this.commandStatus = commandStatus;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Audit_Log_Id")
	public AuditLog getAuditLog() {
		return auditLog;
	}

	public void setAuditLog(AuditLog auditLog) {
		this.auditLog = auditLog;
	}

}