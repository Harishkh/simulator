package com.simulator.dao;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "reboot")
public class RebootCommands implements java.io.Serializable {

	private Integer rebootId;
	private Boolean rebootCommand;
	private Timestamp requestTime;
	private Timestamp responseTime;
	private String commandStatus;
	private AuditLog auditLog;

	public RebootCommands() {
	}

	public RebootCommands(Integer rebootId, Boolean rebootCommand, Timestamp requestTime, Timestamp responseTime,
			String commandStatus, AuditLog auditLog) {
		super();
		this.rebootId = rebootId;
		this.rebootCommand = rebootCommand;
		this.requestTime = requestTime;
		this.responseTime = responseTime;
		this.commandStatus = commandStatus;
		this.auditLog = auditLog;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Reboot_Id", unique = true, nullable = false)
	public Integer getRebootId() {
		return rebootId;
	}

	public void setRebootId(Integer rebootId) {
		this.rebootId = rebootId;
	}

	@Column(name = "Reboot_Command", length = 45)
	public Boolean getRebootCommand() {
		return rebootCommand;
	}

	public void setRebootCommand(Boolean rebootCommand) {
		this.rebootCommand = rebootCommand;
	}

	@Column(name = "Request_Time", length = 45)
	public Timestamp getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(Timestamp requestTime) {
		this.requestTime = requestTime;
	}

	@Column(name = "Response_Time", length = 45)
	public Timestamp getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(Timestamp responseTime) {
		this.responseTime = responseTime;
	}

	@Column(name = "Command_Status", length = 45)
	public String getCommandStatus() {
		return commandStatus;
	}

	public void setCommandStatus(String commandStatus) {
		this.commandStatus = commandStatus;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Audit_Log_Id")
	public AuditLog getAuditLog() {
		return auditLog;
	}

	public void setAuditLog(AuditLog auditLog) {
		this.auditLog = auditLog;
	}

}