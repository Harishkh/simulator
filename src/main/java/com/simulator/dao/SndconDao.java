package com.simulator.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class SndconDao {

	public static final String INTERFACENAME = "interfaceName";
	public static final String IPADDRESS = "ipAddress";
	
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public void save(SendConfigcmdvalues commands) {
		try {
			getCurrentSession().save(commands);
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public SendConfigcmdvalues findById(Integer id) {
		try {
			SendConfigcmdvalues commands = (SendConfigcmdvalues) getCurrentSession().get("com.simulator.dao.SendConfigcmdvalues", id);
			return commands;
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public void saveOrUpdate(SendConfigcmdvalues commands) {
		try {
			getCurrentSession().saveOrUpdate(commands);
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	public void delete(SendConfigcmdvalues commands) {
		try {
			getCurrentSession().delete(commands);
		} catch (RuntimeException re) {
			throw re;
		}
	}

	@SuppressWarnings("rawtypes")
	public List findAll() {
		try {
			String queryString = "from SendConfigcmdvalues";
			Query queryObject = getCurrentSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public List<SendConfigcmdvalues> findByInterfaceName(Object interfaceName) {
		return findByProperty(INTERFACENAME, interfaceName);
	}

	public List<SendConfigcmdvalues> findByIpAddress(Object ipAddress) {
		return findByProperty(IPADDRESS, ipAddress);
	}

	public List findByProperty(String propertyName, Object value) {
		//log.debug("finding DashboardCommands instance with property: " + propertyName + ", value: " + value);
		try {
			String queryString = "from SendConfigcmdvalues as model where model." + propertyName + "= ?";
			Query queryObject = getCurrentSession().createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re) {
			//log.error("find by property name failed", re);
			throw re;
		}
	}
	
	public SendConfigcmdvalues getCityById(Integer id) {
		return (SendConfigcmdvalues) getCurrentSession().get(SendConfigcmdvalues.class, id);
	}
	
}
