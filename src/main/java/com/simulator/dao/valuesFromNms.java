package com.simulator.dao;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="VALUES_NMS")
public class valuesFromNms {

	private Integer dashId;
	
	private String ssh;
	private String cli;
	private String dfePowerTemperatureGetAll;
	private String dfeGrequencyGet;
	private String exit;
	private String mmeEnbInfoIp;
	private AuditLog auditLog;
	
	private Timestamp requestTime;
	private Timestamp responseTime;
	private String commandStatus;
	
	public valuesFromNms() {
		
	}

	public valuesFromNms(Integer dashId, String ssh, String cli, String dfePowerTemperatureGetAll,
			String dfeGrequencyGet, String exit, String mmeEnbInfoIp, AuditLog auditLog,Timestamp requestTime, Timestamp responseTime,
			String commandStatus) {
		super();
		this.dashId = dashId;
		this.ssh = ssh;
		this.cli = cli;
		this.dfePowerTemperatureGetAll = dfePowerTemperatureGetAll;
		this.dfeGrequencyGet = dfeGrequencyGet;
		this.exit = exit;
		this.mmeEnbInfoIp = mmeEnbInfoIp;
		this.auditLog = auditLog;
		this.requestTime = requestTime;
		this.responseTime = responseTime;
		this.commandStatus = commandStatus;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Dash_Id", unique = true, nullable = false)
	public Integer getDashId() {
		return dashId;
	}

	public void setDashId(Integer dashId) {
		this.dashId = dashId;
	}

	@Column(name="d_ssh")
	public String getSsh() {
		return ssh;
	}

	public void setSsh(String ssh) {
		this.ssh = ssh;
	}

	@Column(name="d_cli")
	public String getCli() {
		return cli;
	}

	public void setCli(String cli) {
		this.cli = cli;
	}

	@Column(name="d_getDfePowerTemperatureGetAll")
	public String getDfePowerTemperatureGetAll() {
		return dfePowerTemperatureGetAll;
	}

	public void setDfePowerTemperatureGetAll(String dfePowerTemperatureGetAll) {
		this.dfePowerTemperatureGetAll = dfePowerTemperatureGetAll;
	}

	@Column(name="d_getDfeGrequencyGet")
	public String getDfeGrequencyGet() {
		return dfeGrequencyGet;
	}

	public void setDfeGrequencyGet(String dfeGrequencyGet) {
		this.dfeGrequencyGet = dfeGrequencyGet;
	}

	@Column(name="d_getExit")
	public String getExit() {
		return exit;
	}

	public void setExit(String exit) {
		this.exit = exit;
	}

	@Column(name="d_getMmeEnbInfoIp")
	public String getMmeEnbInfoIp() {
		return mmeEnbInfoIp;
	}

	public void setMmeEnbInfoIp(String mmeEnbInfoIp) {
		this.mmeEnbInfoIp = mmeEnbInfoIp;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Audit_Log_Id")
	public AuditLog getAuditLog() {
		return auditLog;
	}

	public void setAuditLog(AuditLog auditLog) {
		this.auditLog = auditLog;
	}

	public Timestamp getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(Timestamp requestTime) {
		this.requestTime = requestTime;
	}

	public Timestamp getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(Timestamp responseTime) {
		this.responseTime = responseTime;
	}

	public String getCommandStatus() {
		return commandStatus;
	}

	public void setCommandStatus(String commandStatus) {
		this.commandStatus = commandStatus;
	}
	
	
}
