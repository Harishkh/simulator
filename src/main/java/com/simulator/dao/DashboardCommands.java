package com.simulator.dao;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "dashboard")
public class DashboardCommands implements java.io.Serializable {

	private Integer dashboardId;
	private Boolean ssh;
	private Boolean cli;
	private Boolean dfePowerTemperatureGetAll;
	private Boolean dfeGrequencyGet;
	private Boolean exit;
	private Boolean mmeEnbInfoIp;
	private String mmeEnbInfoIpvalue;

	private Timestamp requestTime;
	private Timestamp responseTime;
	private String commandStatus;
	private AuditLog auditLog;

	public DashboardCommands() {
	}

	public DashboardCommands(Integer dashboardId, Boolean ssh, Boolean cli, Boolean dfePowerTemperatureGetAll,
			Boolean dfeGrequencyGet, Boolean exit, Boolean mmeEnbInfoIp,String mmeEnbInfoIpvalue, Timestamp requestTime, Timestamp responseTime,
			String commandStatus, AuditLog auditLog) {
		super();
		this.dashboardId = dashboardId;
		this.ssh = ssh;
		this.cli = cli;
		this.dfePowerTemperatureGetAll = dfePowerTemperatureGetAll;
		this.dfeGrequencyGet = dfeGrequencyGet;
		this.exit = exit;
		this.mmeEnbInfoIp = mmeEnbInfoIp;
		this.mmeEnbInfoIpvalue=mmeEnbInfoIpvalue;
		this.requestTime = requestTime;
		this.responseTime = responseTime;
		this.commandStatus = commandStatus;
		this.auditLog = auditLog;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Dashboard_Id", unique = true, nullable = false)
	public Integer getDashboardId() {
		return dashboardId;
	}

	public void setDashboardId(Integer dashboardId) {
		this.dashboardId = dashboardId;
	}

	@Column(name = "Ssh_Command", length = 45)
	public Boolean getSsh() {
		return ssh;
	}

	public void setSsh(Boolean ssh) {
		this.ssh = ssh;
	}

	@Column(name = "Cli_Command", length = 45)
	public Boolean getCli() {
		return cli;
	}

	public void setCli(Boolean cli) {
		this.cli = cli;
	}

	@Column(name = "Dfe_Power_Temperature_Get_All", length = 45)
	public Boolean getDfePowerTemperatureGetAll() {
		return dfePowerTemperatureGetAll;
	}

	public void setDfePowerTemperatureGetAll(Boolean dfePowerTemperatureGetAll) {
		this.dfePowerTemperatureGetAll = dfePowerTemperatureGetAll;
	}

	@Column(name = "Dfe_Frequency_Get", length = 45)
	public Boolean getDfeGrequencyGet() {
		return dfeGrequencyGet;
	}

	public void setDfeGrequencyGet(Boolean dfeGrequencyGet) {
		this.dfeGrequencyGet = dfeGrequencyGet;
	}

	@Column(name = "Exit_Command", length = 45)
	public Boolean getExit() {
		return exit;
	}

	public void setExit(Boolean exit) {
		this.exit = exit;
	}

	@Column(name = "Mme_Enb_Info_Ip", length = 45)
	public Boolean getMmeEnbInfoIp() {
		return mmeEnbInfoIp;
	}

	public void setMmeEnbInfoIp(Boolean mmeEnbInfoIp) {
		this.mmeEnbInfoIp = mmeEnbInfoIp;
	}

	@Column(name = "Request_Time", length = 45)
	public Timestamp getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(Timestamp requestTime) {
		this.requestTime = requestTime;
	}

	@Column(name = "Response_Time", length = 45)
	public Timestamp getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(Timestamp responseTime) {
		this.responseTime = responseTime;
	}

	@Column(name = "Command_Status", length = 45)
	public String getCommandStatus() {
		return commandStatus;
	}

	public void setCommandStatus(String commandStatus) {
		this.commandStatus = commandStatus;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Audit_Log_Id")
	public AuditLog getAuditLog() {
		return auditLog;
	}

	public void setAuditLog(AuditLog auditLog) {
		this.auditLog = auditLog;
	}

	public String getMmeEnbInfoIpvalue() {
		return mmeEnbInfoIpvalue;
	}

	public void setMmeEnbInfoIpvalue(String mmeEnbInfoIpvalue) {
		this.mmeEnbInfoIpvalue = mmeEnbInfoIpvalue;
	}

	
}