package com.simulator.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class valuesFromNmsDao {
	
	public static final String INTERFACENAME = "interfaceName";
	public static final String IPADDRESS = "ipAddress";

	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public void save(valuesFromNms commands) {
		try {
			getCurrentSession().save(commands);
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public valuesFromNms findById(Integer id) {
		try {
			valuesFromNms commands = (valuesFromNms) getCurrentSession().get("com.simulator.dao.valuesFromNms", id);
			return commands;
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public void saveOrUpdate(valuesFromNms commands) {
		try {
			getCurrentSession().saveOrUpdate(commands);
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	public void delete(DashboardCommands commands) {
		try {
			getCurrentSession().delete(commands);
		} catch (RuntimeException re) {
			throw re;
		}
	}

	@SuppressWarnings("rawtypes")
	public List findAll() {
		try {
			String queryString = "from DashboardCommands";
			Query queryObject = getCurrentSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public List<valuesFromNms> findByInterfaceName(Object interfaceName) {
		return findByProperty(INTERFACENAME, interfaceName);
	}

	public List<valuesFromNms> findByIpAddress(Object ipAddress) {
		return findByProperty(IPADDRESS, ipAddress);
	}

	public List findByProperty(String propertyName, Object value) {
		//log.debug("finding DashboardCommands instance with property: " + propertyName + ", value: " + value);
		try {
			String queryString = "from valuesFromNms as model where model." + propertyName + "= ?";
			Query queryObject = getCurrentSession().createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re) {
			//log.error("find by property name failed", re);
			throw re;
		}
	}
	
	public valuesFromNms getCityById(Integer id) {
		return (valuesFromNms) getCurrentSession().get(valuesFromNms.class, id);
	}
}
