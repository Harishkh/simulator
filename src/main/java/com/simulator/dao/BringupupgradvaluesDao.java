package com.simulator.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class BringupupgradvaluesDao {

	
	public static final String INTERFACENAME = "interfaceName";
	public static final String IPADDRESS = "ipAddress";

	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public void save(Bringupupgradvalues commands) {
		try {
			getCurrentSession().save(commands);
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public Bringupupgradvalues findById(Integer id) {
		try {
			Bringupupgradvalues commands = (Bringupupgradvalues) getCurrentSession().get("com.simulator.dao.Bringupupgradvalues", id);
			return commands;
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public void saveOrUpdate(Bringupupgradvalues commands) {
		try {
			getCurrentSession().saveOrUpdate(commands);
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public void delete(Bringupupgradvalues commands) {
		try {
			getCurrentSession().delete(commands);
		} catch (RuntimeException re) {
			throw re;
		}
	}

	@SuppressWarnings("rawtypes")
	public List findAll() {
		try {
			String queryString = "from Bringupupgradvalues";
			Query queryObject = getCurrentSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public List<Bringupupgradvalues> findByInterfaceName(Object interfaceName) {
		return findByProperty(INTERFACENAME, interfaceName);
	}

	public List<Bringupupgradvalues> findByIpAddress(Object ipAddress) {
		return findByProperty(IPADDRESS, ipAddress);
	}

	public List findByProperty(String propertyName, Object value) {
		//log.debug("finding BringupUpgradeCommands instance with property: " + propertyName + ", value: " + value);
		try {
			String queryString = "from Bringupupgradvalues as model where model." + propertyName + "= ?";
			Query queryObject = getCurrentSession().createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re) {
			//log.error("find by property name failed", re);
			throw re;
		}
	}

	public Bringupupgradvalues getCityById(Integer id) {
		return (Bringupupgradvalues) getCurrentSession().get(Bringupupgradvalues.class, id);
	}
	
}
