package com.simulator.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class AuditLogDao {
	private static final Logger log = LoggerFactory.getLogger(AuditLogDao.class);

	public static final String INTERFACENAME = "interfaceName";
	public static final String IPADDRESS = "ipAddress";

	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public void save(AuditLog auditLog) {
		try {
			getCurrentSession().save(auditLog);
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public AuditLog findById(Integer id) {
		try {
			AuditLog auditLog = (AuditLog) getCurrentSession().get("com.simulator.dao.AuditLog", id);
			return auditLog;
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public void saveOrUpdate(AuditLog auditLog) {
		try {
			getCurrentSession().saveOrUpdate(auditLog);
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public void delete(AuditLog auditLog) {
		try {
			getCurrentSession().delete(auditLog);
		} catch (RuntimeException re) {
			throw re;
		}
	}

	@SuppressWarnings("rawtypes")
	public List findAll() {
		try {
			String queryString = "from AuditLog";
			Query queryObject = getCurrentSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	@SuppressWarnings("rawtypes")
	public List findAllByDistinct() {
		try {
			String queryString = "select Distinct ipAddress from AuditLog";
			Query queryObject = getCurrentSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	public List<AuditLog> findByInterfaceName(Object interfaceName) {
		return findByProperty(INTERFACENAME, interfaceName);
	}

	public List<AuditLog> findByIpAddress(Object ipAddress) {
		return findByProperty(IPADDRESS, ipAddress);
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding AuditLog instance with property: " + propertyName + ", value: " + value);
		try {
			String queryString = "from AuditLog as model where model." + propertyName + "= ?";
			Query queryObject = getCurrentSession().createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public AuditLog getCityById(Integer id) {
		return (AuditLog) getCurrentSession().get(AuditLog.class, id);
	}

}