package com.simulator.dao;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bringupupgrade_withvalues")
public class Bringupupgradvalues implements java.io.Serializable {

	private Integer bringUpgradeCommandsId;
	private String chmodUpgradeFile;
	private String tarExtractUpgradeFile;
	private String cpConfig;
	private String cpStartup;
	private String cpHariff;
	private String chmodDb;
	private String chmodUpgradeFolder;
	private String chmodConfig;
	private String mvUpgradeFile;
	private String mountMntMnc;
	private String fsetenvBw;
	private String fsetenvIpaddr;
	private String fsetenvGtpip;
	private String fsetenvNetmask;
	private String fsetenvGatewayip;
	private String fsetenvStartepc;
	private String fileRfconfig;
	private String umountMntMnc;
	private String mkdirDb;
	private String sync;
	
	private Timestamp requestTime;
	private Timestamp responseTime;
	private String commandStatus;
	
	private AuditLog auditLog;

	public Bringupupgradvalues() {
	}

	public Bringupupgradvalues(Integer bringUpgradeCommandsId, String chmodUpgradeFile, String tarExtractUpgradeFile,
			String cpConfig, String cpStartup, String cpHariff, String chmodDb, String chmodUpgradeFolder,
			String chmodConfig, String mvUpgradeFile, String mountMntMnc, String fsetenvBw, String fsetenvIpaddr,
			String fsetenvGtpip, String fsetenvNetmask, String fsetenvGatewayip, String fsetenvStartepc,
			String fileRfconfig, String umountMntMnc, String mkdirDb, String sync, Timestamp requestTime,
			Timestamp responseTime, String commandStatus, AuditLog auditLog) {
		super();
		this.bringUpgradeCommandsId = bringUpgradeCommandsId;
		this.chmodUpgradeFile = chmodUpgradeFile;
		this.tarExtractUpgradeFile = tarExtractUpgradeFile;
		this.cpConfig = cpConfig;
		this.cpStartup = cpStartup;
		this.cpHariff = cpHariff;
		this.chmodDb = chmodDb;
		this.chmodUpgradeFolder = chmodUpgradeFolder;
		this.chmodConfig = chmodConfig;
		this.mvUpgradeFile = mvUpgradeFile;
		this.mountMntMnc = mountMntMnc;
		this.fsetenvBw = fsetenvBw;
		this.fsetenvIpaddr = fsetenvIpaddr;
		this.fsetenvGtpip = fsetenvGtpip;
		this.fsetenvNetmask = fsetenvNetmask;
		this.fsetenvGatewayip = fsetenvGatewayip;
		this.fsetenvStartepc = fsetenvStartepc;
		this.fileRfconfig = fileRfconfig;
		this.umountMntMnc = umountMntMnc;
		this.mkdirDb = mkdirDb;
		this.sync = sync;
		this.requestTime = requestTime;
		this.responseTime = responseTime;
		this.commandStatus = commandStatus;
		this.auditLog = auditLog;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Bringup_Upgrade_sdcar_Id", unique = true, nullable = false)
	public Integer getBringUpgradeCommandsId() {
		return bringUpgradeCommandsId;
	}

	public void setBringUpgradeCommandsId(Integer bringUpgradeCommandsId) {
		this.bringUpgradeCommandsId = bringUpgradeCommandsId;
	}

	public String getChmodUpgradeFile() {
		return chmodUpgradeFile;
	}

	public void setChmodUpgradeFile(String chmodUpgradeFile) {
		this.chmodUpgradeFile = chmodUpgradeFile;
	}

	public String getTarExtractUpgradeFile() {
		return tarExtractUpgradeFile;
	}

	public void setTarExtractUpgradeFile(String tarExtractUpgradeFile) {
		this.tarExtractUpgradeFile = tarExtractUpgradeFile;
	}

	public String getCpConfig() {
		return cpConfig;
	}

	public void setCpConfig(String cpConfig) {
		this.cpConfig = cpConfig;
	}

	public String getCpStartup() {
		return cpStartup;
	}

	public void setCpStartup(String cpStartup) {
		this.cpStartup = cpStartup;
	}

	public String getCpHariff() {
		return cpHariff;
	}

	public void setCpHariff(String cpHariff) {
		this.cpHariff = cpHariff;
	}

	public String getChmodDb() {
		return chmodDb;
	}

	public void setChmodDb(String chmodDb) {
		this.chmodDb = chmodDb;
	}

	public String getChmodUpgradeFolder() {
		return chmodUpgradeFolder;
	}

	public void setChmodUpgradeFolder(String chmodUpgradeFolder) {
		this.chmodUpgradeFolder = chmodUpgradeFolder;
	}

	public String getChmodConfig() {
		return chmodConfig;
	}

	public void setChmodConfig(String chmodConfig) {
		this.chmodConfig = chmodConfig;
	}

	public String getMvUpgradeFile() {
		return mvUpgradeFile;
	}

	public void setMvUpgradeFile(String mvUpgradeFile) {
		this.mvUpgradeFile = mvUpgradeFile;
	}

	public String getMountMntMnc() {
		return mountMntMnc;
	}

	public void setMountMntMnc(String mountMntMnc) {
		this.mountMntMnc = mountMntMnc;
	}

	public String getFsetenvBw() {
		return fsetenvBw;
	}

	public void setFsetenvBw(String fsetenvBw) {
		this.fsetenvBw = fsetenvBw;
	}

	public String getFsetenvIpaddr() {
		return fsetenvIpaddr;
	}

	public void setFsetenvIpaddr(String fsetenvIpaddr) {
		this.fsetenvIpaddr = fsetenvIpaddr;
	}

	public String getFsetenvGtpip() {
		return fsetenvGtpip;
	}

	public void setFsetenvGtpip(String fsetenvGtpip) {
		this.fsetenvGtpip = fsetenvGtpip;
	}

	public String getFsetenvNetmask() {
		return fsetenvNetmask;
	}

	public void setFsetenvNetmask(String fsetenvNetmask) {
		this.fsetenvNetmask = fsetenvNetmask;
	}

	public String getFsetenvGatewayip() {
		return fsetenvGatewayip;
	}

	public void setFsetenvGatewayip(String fsetenvGatewayip) {
		this.fsetenvGatewayip = fsetenvGatewayip;
	}

	public String getFsetenvStartepc() {
		return fsetenvStartepc;
	}

	public void setFsetenvStartepc(String fsetenvStartepc) {
		this.fsetenvStartepc = fsetenvStartepc;
	}

	public String getFileRfconfig() {
		return fileRfconfig;
	}

	public void setFileRfconfig(String fileRfconfig) {
		this.fileRfconfig = fileRfconfig;
	}

	public String getUmountMntMnc() {
		return umountMntMnc;
	}

	public void setUmountMntMnc(String umountMntMnc) {
		this.umountMntMnc = umountMntMnc;
	}

	public String getMkdirDb() {
		return mkdirDb;
	}

	public void setMkdirDb(String mkdirDb) {
		this.mkdirDb = mkdirDb;
	}

	public String getSync() {
		return sync;
	}

	public void setSync(String sync) {
		this.sync = sync;
	}

	public Timestamp getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(Timestamp requestTime) {
		this.requestTime = requestTime;
	}

	public Timestamp getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(Timestamp responseTime) {
		this.responseTime = responseTime;
	}

	public String getCommandStatus() {
		return commandStatus;
	}

	public void setCommandStatus(String commandStatus) {
		this.commandStatus = commandStatus;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Audit_Log_Id")
	public AuditLog getAuditLog() {
		return auditLog;
	}

	public void setAuditLog(AuditLog auditLog) {
		this.auditLog = auditLog;
	}
	
	
	
}
