package com.simulator.dao;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "audit_log")
public class AuditLog implements java.io.Serializable {
	
	private Integer auditLogId;
	private String interfaceName;
	private String ipAddress;
	private String status;
	private Timestamp requestTime;
	private Timestamp responseTime;
	private String commandStatus;
	private Set<EditCommands> editCommandsList = new HashSet<EditCommands>(0);
	private Set<BringupUpgradeCommands> bringupUpgradeCommandsList = new HashSet<BringupUpgradeCommands>(0);
	private Set<RebootCommands> rebootCommandsList = new HashSet<RebootCommands>(0);
	private Set<DashboardCommands> dashboardCommandsList = new HashSet<DashboardCommands>(0);
	private Set<SendConfigCommands> sendConfigCommandsList = new HashSet<SendConfigCommands>(0);
	private Set<valuesFromNms> nmsvalues=new HashSet<valuesFromNms>(0);
	private Set<SendConfigcmdvalues> sndconfigvalues=new HashSet<SendConfigcmdvalues>(0);
	private Set<Bringupupgradvalues> bringupupgradeval=new HashSet<Bringupupgradvalues>(0);
	
	public AuditLog() {
	}

	public AuditLog(Integer auditLogId, String interfaceName, String ipAddress, String status, Timestamp requestTime,
			Timestamp responseTime, String commandStatus, Set<EditCommands> editCommandsList,
			Set<BringupUpgradeCommands> bringupUpgradeCommandsList, Set<RebootCommands> rebootCommandsList,
			Set<DashboardCommands> dashboardCommandsList, Set<SendConfigCommands> sendConfigCommandsList,Set<valuesFromNms> nmsvalues,
			Set<SendConfigcmdvalues> sndconfigvalues,Set<Bringupupgradvalues> bringupupgradeval) {
		super();
		this.auditLogId = auditLogId;
		this.interfaceName = interfaceName;
		this.ipAddress = ipAddress;
		this.status = status;
		this.requestTime = requestTime;
		this.responseTime = responseTime;
		this.commandStatus = commandStatus;
		this.editCommandsList = editCommandsList;
		this.bringupUpgradeCommandsList = bringupUpgradeCommandsList;
		this.rebootCommandsList = rebootCommandsList;
		this.dashboardCommandsList = dashboardCommandsList;
		this.sendConfigCommandsList = sendConfigCommandsList;
		this.nmsvalues=nmsvalues;
		this.sndconfigvalues=sndconfigvalues;
		this.bringupupgradeval=bringupupgradeval;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Audit_Log_Id", unique = true, nullable = false)
	public Integer getAuditLogId() {
		return auditLogId;
	}

	public void setAuditLogId(Integer auditLogId) {
		this.auditLogId = auditLogId;
	}

	@Column(name = "Interface_Name", length = 45)
	public String getInterfaceName() {
		return interfaceName;
	}

	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}

	@Column(name = "IP_Address", length = 45)
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name = "Status", length = 45)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "Request_Time", length = 45)
	public Timestamp getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(Timestamp requestTime) {
		this.requestTime = requestTime;
	}

	@Column(name = "Response_Time", length = 45)
	public Timestamp getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(Timestamp responseTime) {
		this.responseTime = responseTime;
	}

	@Column(name = "Command_Status", length = 45)
	public String getCommandStatus() {
		return commandStatus;
	}

	public void setCommandStatus(String commandStatus) {
		this.commandStatus = commandStatus;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "auditLog")
	public Set<EditCommands> getEditCommandsList() {
		return editCommandsList;
	}

	public void setEditCommandsList(Set<EditCommands> editCommandsList) {
		this.editCommandsList = editCommandsList;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "auditLog")
	public Set<BringupUpgradeCommands> getBringupUpgradeCommandsList() {
		return bringupUpgradeCommandsList;
	}

	public void setBringupUpgradeCommandsList(Set<BringupUpgradeCommands> bringupUpgradeCommandsList) {
		this.bringupUpgradeCommandsList = bringupUpgradeCommandsList;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "auditLog")
	public Set<RebootCommands> getRebootCommandsList() {
		return rebootCommandsList;
	}

	public void setRebootCommandsList(Set<RebootCommands> rebootCommandsList) {
		this.rebootCommandsList = rebootCommandsList;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "auditLog")
	public Set<DashboardCommands> getDashboardCommandsList() {
		return dashboardCommandsList;
	}

	public void setDashboardCommandsList(Set<DashboardCommands> dashboardCommandsList) {
		this.dashboardCommandsList = dashboardCommandsList;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "auditLog")
	public Set<SendConfigCommands> getSendConfigCommandsList() {
		return sendConfigCommandsList;
	}

	public void setSendConfigCommandsList(Set<SendConfigCommands> sendConfigCommandsList) {
		this.sendConfigCommandsList = sendConfigCommandsList;
	}
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "auditLog")
	public Set<valuesFromNms> getNmsvalues() {
		return nmsvalues;
	}

	public void setNmsvalues(Set<valuesFromNms> nmsvalues) {
		this.nmsvalues = nmsvalues;
	}
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "auditLog")
	public Set<SendConfigcmdvalues> getSndconfigvalues() {
		return sndconfigvalues;
	}

	public void setSndconfigvalues(Set<SendConfigcmdvalues> sndconfigvalues) {
		this.sndconfigvalues = sndconfigvalues;
	}
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "auditLog")
	public Set<Bringupupgradvalues> getBringupupgradeval() {
		return bringupupgradeval;
	}

	public void setBringupupgradeval(Set<Bringupupgradvalues> bringupupgradeval) {
		this.bringupupgradeval = bringupupgradeval;
	}

	
	
	
}