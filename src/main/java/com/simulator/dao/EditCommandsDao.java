package com.simulator.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class EditCommandsDao {
	private static final Logger log = LoggerFactory.getLogger(EditCommandsDao.class);

	public static final String INTERFACENAME = "interfaceName";
	public static final String IPADDRESS = "ipAddress";

	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public void save(EditCommands commands) {
		try {
			getCurrentSession().save(commands);
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public EditCommands findById(Integer id) {
		try {
			EditCommands commands = (EditCommands) getCurrentSession().get("com.simulator.dao.EditCommands", id);
			return commands;
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public void saveOrUpdate(EditCommands commands) {
		try {
			getCurrentSession().saveOrUpdate(commands);
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public void delete(EditCommands commands) {
		try {
			getCurrentSession().delete(commands);
		} catch (RuntimeException re) {
			throw re;
		}
	}

	@SuppressWarnings("rawtypes")
	public List findAll() {
		try {
			String queryString = "from EditCommands";
			Query queryObject = getCurrentSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public List<EditCommands> findByInterfaceName(Object interfaceName) {
		return findByProperty(INTERFACENAME, interfaceName);
	}

	public List<EditCommands> findByIpAddress(Object ipAddress) {
		return findByProperty(IPADDRESS, ipAddress);
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding EditCommands instance with property: " + propertyName + ", value: " + value);
		try {
			String queryString = "from EditCommands as model where model." + propertyName + "= ?";
			Query queryObject = getCurrentSession().createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public EditCommands getCityById(Integer id) {
		return (EditCommands) getCurrentSession().get(EditCommands.class, id);
	}

}