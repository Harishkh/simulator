package com.simulator.dao;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sendconfig_withvalues")
public class SendConfigcmdvalues implements java.io.Serializable{

	private Integer rebootId;
	private String mkdirSendConfig;
	private String cpConfig;
	private String cpStartup;
	private String chmodDb;
	private String chmodSendConfig;
	private String chmodConfig;
	private String cpHariff;
	private String mountMntMnc;
	private String umountMntMnc;
	private String mkdirDb;
	private String sync;
	private String fsetenvBw;
	private String fsetenvIpaddr;
	private String fsetenvGtpip;
	private String fsetenvNetmask;
	private String fsetenvGatewayip;
	private String fsetenvStartepc;
	private String dfeI2cIap;
	private String cdTmp;
	private String cpTmp;
	private String fileConfig;
	private String fileStartup;
	private String fileHariff;
	private String fileRfconfig;
	private String fileSqliteDb;
	private Timestamp requestTime;
	private Timestamp responseTime;
	private String commandStatus;
	private AuditLog auditLog;
	
	public SendConfigcmdvalues() {
	}

	public SendConfigcmdvalues(Integer rebootId, String mkdirSendConfig, String cpConfig, String cpStartup,
			String chmodDb, String chmodSendConfig, String chmodConfig, String cpHariff, String mountMntMnc,
			String umountMntMnc, String mkdirDb, String sync, String fsetenvBw, String fsetenvIpaddr,
			String fsetenvGtpip, String fsetenvNetmask, String fsetenvGatewayip, String fsetenvStartepc,
			String dfeI2cIap, String cdTmp, String cpTmp, String fileConfig, String fileStartup, String fileHariff,
			String fileRfconfig, String fileSqliteDb, Timestamp requestTime, Timestamp responseTime,
			String commandStatus, AuditLog auditLog) {
		super();
		this.rebootId = rebootId;
		this.mkdirSendConfig = mkdirSendConfig;
		this.cpConfig = cpConfig;
		this.cpStartup = cpStartup;
		this.chmodDb = chmodDb;
		this.chmodSendConfig = chmodSendConfig;
		this.chmodConfig = chmodConfig;
		this.cpHariff = cpHariff;
		this.mountMntMnc = mountMntMnc;
		this.umountMntMnc = umountMntMnc;
		this.mkdirDb = mkdirDb;
		this.sync = sync;
		this.fsetenvBw = fsetenvBw;
		this.fsetenvIpaddr = fsetenvIpaddr;
		this.fsetenvGtpip = fsetenvGtpip;
		this.fsetenvNetmask = fsetenvNetmask;
		this.fsetenvGatewayip = fsetenvGatewayip;
		this.fsetenvStartepc = fsetenvStartepc;
		this.dfeI2cIap = dfeI2cIap;
		this.cdTmp = cdTmp;
		this.cpTmp = cpTmp;
		this.fileConfig = fileConfig;
		this.fileStartup = fileStartup;
		this.fileHariff = fileHariff;
		this.fileRfconfig = fileRfconfig;
		this.fileSqliteDb = fileSqliteDb;
		this.requestTime = requestTime;
		this.responseTime = responseTime;
		this.commandStatus = commandStatus;
		this.auditLog = auditLog;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Reboot_Id", unique = true, nullable = false)
	public Integer getRebootId() {
		return rebootId;
	}

	public void setRebootId(Integer rebootId) {
		this.rebootId = rebootId;
	}

	public String getMkdirSendConfig() {
		return mkdirSendConfig;
	}

	public void setMkdirSendConfig(String mkdirSendConfig) {
		this.mkdirSendConfig = mkdirSendConfig;
	}

	public String getCpConfig() {
		return cpConfig;
	}

	public void setCpConfig(String cpConfig) {
		this.cpConfig = cpConfig;
	}

	public String getCpStartup() {
		return cpStartup;
	}

	public void setCpStartup(String cpStartup) {
		this.cpStartup = cpStartup;
	}

	public String getChmodDb() {
		return chmodDb;
	}

	public void setChmodDb(String chmodDb) {
		this.chmodDb = chmodDb;
	}

	public String getChmodSendConfig() {
		return chmodSendConfig;
	}

	public void setChmodSendConfig(String chmodSendConfig) {
		this.chmodSendConfig = chmodSendConfig;
	}

	public String getChmodConfig() {
		return chmodConfig;
	}

	public void setChmodConfig(String chmodConfig) {
		this.chmodConfig = chmodConfig;
	}

	public String getCpHariff() {
		return cpHariff;
	}

	public void setCpHariff(String cpHariff) {
		this.cpHariff = cpHariff;
	}

	public String getMountMntMnc() {
		return mountMntMnc;
	}

	public void setMountMntMnc(String mountMntMnc) {
		this.mountMntMnc = mountMntMnc;
	}

	public String getUmountMntMnc() {
		return umountMntMnc;
	}

	public void setUmountMntMnc(String umountMntMnc) {
		this.umountMntMnc = umountMntMnc;
	}

	public String getMkdirDb() {
		return mkdirDb;
	}

	public void setMkdirDb(String mkdirDb) {
		this.mkdirDb = mkdirDb;
	}

	public String getSync() {
		return sync;
	}

	public void setSync(String sync) {
		this.sync = sync;
	}

	public String getFsetenvBw() {
		return fsetenvBw;
	}

	public void setFsetenvBw(String fsetenvBw) {
		this.fsetenvBw = fsetenvBw;
	}

	public String getFsetenvIpaddr() {
		return fsetenvIpaddr;
	}

	public void setFsetenvIpaddr(String fsetenvIpaddr) {
		this.fsetenvIpaddr = fsetenvIpaddr;
	}

	public String getFsetenvGtpip() {
		return fsetenvGtpip;
	}

	public void setFsetenvGtpip(String fsetenvGtpip) {
		this.fsetenvGtpip = fsetenvGtpip;
	}

	public String getFsetenvNetmask() {
		return fsetenvNetmask;
	}

	public void setFsetenvNetmask(String fsetenvNetmask) {
		this.fsetenvNetmask = fsetenvNetmask;
	}

	public String getFsetenvGatewayip() {
		return fsetenvGatewayip;
	}

	public void setFsetenvGatewayip(String fsetenvGatewayip) {
		this.fsetenvGatewayip = fsetenvGatewayip;
	}

	public String getFsetenvStartepc() {
		return fsetenvStartepc;
	}

	public void setFsetenvStartepc(String fsetenvStartepc) {
		this.fsetenvStartepc = fsetenvStartepc;
	}

	public String getDfeI2cIap() {
		return dfeI2cIap;
	}

	public void setDfeI2cIap(String dfeI2cIap) {
		this.dfeI2cIap = dfeI2cIap;
	}

	public String getCdTmp() {
		return cdTmp;
	}

	public void setCdTmp(String cdTmp) {
		this.cdTmp = cdTmp;
	}

	public String getCpTmp() {
		return cpTmp;
	}

	public void setCpTmp(String cpTmp) {
		this.cpTmp = cpTmp;
	}

	public String getFileConfig() {
		return fileConfig;
	}

	public void setFileConfig(String fileConfig) {
		this.fileConfig = fileConfig;
	}

	public String getFileStartup() {
		return fileStartup;
	}

	public void setFileStartup(String fileStartup) {
		this.fileStartup = fileStartup;
	}

	public String getFileHariff() {
		return fileHariff;
	}

	public void setFileHariff(String fileHariff) {
		this.fileHariff = fileHariff;
	}

	public String getFileRfconfig() {
		return fileRfconfig;
	}

	public void setFileRfconfig(String fileRfconfig) {
		this.fileRfconfig = fileRfconfig;
	}

	public String getFileSqliteDb() {
		return fileSqliteDb;
	}

	public void setFileSqliteDb(String fileSqliteDb) {
		this.fileSqliteDb = fileSqliteDb;
	}

	public Timestamp getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(Timestamp requestTime) {
		this.requestTime = requestTime;
	}

	public Timestamp getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(Timestamp responseTime) {
		this.responseTime = responseTime;
	}

	public String getCommandStatus() {
		return commandStatus;
	}

	public void setCommandStatus(String commandStatus) {
		this.commandStatus = commandStatus;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Audit_Log_Id")
	public AuditLog getAuditLog() {
		return auditLog;
	}

	public void setAuditLog(AuditLog auditLog) {
		this.auditLog = auditLog;
	}
	
	
	
}
