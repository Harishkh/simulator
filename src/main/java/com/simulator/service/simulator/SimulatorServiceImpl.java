package com.simulator.service.simulator;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simulator.dao.AuditLog;
import com.simulator.dao.AuditLogDao;

@Service
public class SimulatorServiceImpl implements SimulatorService {

	@Autowired
	private AuditLogDao dao;

	public List<AuditLog> findAll() {
		return dao.findAll();
	}

	public AuditLog addAuditLog(String interfaceName, String ipAddress, String status, Date requestTime,
			Date responseTime, String commandStatus) {

		AuditLog auditLog = new AuditLog();
		auditLog.setInterfaceName(interfaceName);
		auditLog.setIpAddress(ipAddress);
		auditLog.setStatus(status);
		auditLog.setRequestTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		auditLog.setResponseTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		auditLog.setCommandStatus(commandStatus);
		dao.save(auditLog);
		return auditLog;
	}

	public AuditLog findById(Integer id) {
		return dao.findById(id);
	}

	public AuditLog findByIpAddress(Object ipAddress) {
		List<AuditLog> list = dao.findByIpAddress(ipAddress);
		if (list.size() == 0) {
			System.out.println("Unable to find the address " + ipAddress);
			return null;
		}
		return list.get(0);
	}

	public Set<AuditLog> findAllAuditLogs() {
		Set<AuditLog> set = new HashSet<AuditLog>();
		List list = dao.findAllByDistinct();
		if (list != null) {
			for (Object log : list) {
				AuditLog auditLog = findByIpAddress(log);
				set.add(auditLog);
			}
		}
		return set;
	}
	
	public List<AuditLog> findAllByIpAddress(Object ipAddress) {
		List<AuditLog> list = dao.findByIpAddress(ipAddress);
		if (list.size() == 0) {
			System.out.println("Unable to find the address " + ipAddress);
			return null;
		}
		return list;
	}
}