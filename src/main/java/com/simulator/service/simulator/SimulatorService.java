package com.simulator.service.simulator;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.simulator.dao.AuditLog;

public interface SimulatorService {
	public List<AuditLog> findAll();
	
	public AuditLog addAuditLog(String interfaceName, String ipAddress, String status, Date requestTime, Date responseTime, String commandStatus);
	
	public AuditLog findById(Integer id);
	
	public AuditLog findByIpAddress(Object ipAddress);
	
	public Set<AuditLog> findAllAuditLogs();
	
	public List<AuditLog> findAllByIpAddress(Object ipAddress);
	
}