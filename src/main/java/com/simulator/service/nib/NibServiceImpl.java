package com.simulator.service.nib;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.simulator.dao.AuditLog;
import com.simulator.dao.AuditLogDao;
import com.simulator.dao.BringupUpgradeCommands;
import com.simulator.dao.BringupUpgradeCommandsDao;
import com.simulator.dao.DashboardCommands;
import com.simulator.dao.DashboardCommandsDao;
import com.simulator.dao.EditCommands;
import com.simulator.dao.EditCommandsDao;
import com.simulator.dao.RebootCommands;
import com.simulator.dao.RebootCommandsDao;

@Service
@Transactional
public class NibServiceImpl implements NibService {

	@Autowired
	private AuditLogDao dao;

	@Autowired
	private EditCommandsDao editCommandsDao;

	@Autowired
	private BringupUpgradeCommandsDao bringupDao;
	
	@Autowired
	private RebootCommandsDao rebootCommandsDao;
	
	@Autowired
	private DashboardCommandsDao dashboardCommandsDao;

	public String nibDashboard(String user, String password, String ipAddress, Integer port, String nibName, List<String> commandsList) {
		ObjectMapper mapper = new ObjectMapper();

		List<String> list = new ArrayList<String>();
		List frequencyList = new ArrayList();
		
		if ((user != null && user != "") && (password != null && password != "") && commandsList != null
				&& (ipAddress != null && ipAddress != "") && (nibName != null && nibName != "") && port != null) {
			list.add("Power amplifier 1 output power 100000");
			list.add("Power amplifier 2 output power 44411");
			list.add("Power amplifier 1 temperature 1434");
			list.add("Power amplifier 2 temperature 106666");
			list.add("Uplink Frequency is 30000");
			list.add("Downlink Frequency is 34000");
		}

		try {
			
			AuditLog auditLog = new AuditLog();
			auditLog.setInterfaceName(nibName);
			auditLog.setIpAddress(ipAddress);
			auditLog.setStatus("Active");
			auditLog.setRequestTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));

			DashboardCommands commands = new DashboardCommands();
			commands.setRequestTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
			
			String[] itemsArray = new String[commandsList.size()];
			itemsArray = (String[]) commandsList.toArray(itemsArray);

			boolean cli = false;
			boolean dfePowerTemperatureGetAll = false;
			boolean dfeGrequencyGet = false;
			boolean exit = false;
			boolean mmeEnbInfoIp = false;
			String mmeEnbInfoIpvalue = commandsList.get(5).substring(0, commandsList.get(5).length() - 1);
			
			commands.setMmeEnbInfoIpvalue(mmeEnbInfoIpvalue);
			for (int i = 0; i < itemsArray.length; i++) {
				String str = itemsArray[i];
				System.out.println(str);

				if (str.startsWith("[cli")) {
					if (str.equals("[cli")) {
						cli = true;
						commands.setCli(cli);
					} else {
						cli = false;
						commands.setCli(cli);
					}
				}
			
				if (str.startsWith("dfe_power_temperature_get all")) {
					if (str.equals("dfe_power_temperature_get all")) {
						dfePowerTemperatureGetAll = true;
						commands.setDfePowerTemperatureGetAll(dfePowerTemperatureGetAll);
					} else {
						dfePowerTemperatureGetAll = false;
						commands.setDfePowerTemperatureGetAll(dfePowerTemperatureGetAll);
					}
				}
				
				if (str.startsWith("dfe_frequency_get")) {
					if (str.equals("dfe_frequency_get")) {
						dfeGrequencyGet = true;
						commands.setDfeGrequencyGet(dfeGrequencyGet);
					} else {
						dfeGrequencyGet = false;
						commands.setDfeGrequencyGet(dfeGrequencyGet);
					}
				}
				
				if (str.startsWith("exit")) {
					if (str.equals("exit")) {
						exit = true;
						commands.setExit(exit);
					} else {
						exit = false;
						commands.setExit(exit);
					}
				}
				
				if (str.startsWith("mme_enb_info ip")) {
					if (str.equals("mme_enb_info ip " + ipAddress + "]")) {
						mmeEnbInfoIp = true;
						commands.setMmeEnbInfoIp(mmeEnbInfoIp);
					} else {
						mmeEnbInfoIp = false;
						commands.setMmeEnbInfoIp(mmeEnbInfoIp);
					}
				}
				
				if (mmeEnbInfoIp == true && exit == true && dfeGrequencyGet == true && dfePowerTemperatureGetAll == true
						&& cli == true) {

					auditLog.setCommandStatus("Success");
					auditLog.setResponseTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
					
					commands.setCommandStatus("Success");
					commands.setResponseTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
				} else {
					auditLog.setCommandStatus("Fail");
					auditLog.setResponseTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
					
					commands.setCommandStatus("Fail");
					commands.setResponseTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
				}
			}
			
			dao.save(auditLog);
			commands.setAuditLog(auditLog);
			dashboardCommandsDao.save(commands);
			
			// convert map to JSON string
			String json = mapper.writeValueAsString(list);
			System.out.println(json); // compact-print
			json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(list);
			System.out.println(json); // pretty-print
			
			return json;
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return null;
	}

	public String sendConfig(String hardwareName, String hostIpAddress, String uploadFilePath, String folderName,
			String bwValue, String enbMask, String enbGwIp, String flag, String channelBandwidth,
			List<String> commandsList) {

		AuditLog auditLog = new AuditLog();
		auditLog.setInterfaceName(hardwareName);
		auditLog.setIpAddress(hostIpAddress);
		auditLog.setStatus("Active");
		auditLog.setRequestTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));

		EditCommands commands = new EditCommands();
		commands.setRequestTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		
		String[] itemsArray = new String[commandsList.size()];
		itemsArray = (String[]) commandsList.toArray(itemsArray);
		
		String cpConfigvalue =commandsList.get(1);
		String cpStartupvalue =commandsList.get(2);
		String cpHariffvalue =commandsList.get(7);
		
		String fsetenvBwval=commandsList.get(12) ;
		String fsetenvIpaddrval=commandsList.get(13) ;
		String fsetenvGtpipval =commandsList.get(14);
		String fsetenvNetmaskval=commandsList.get(15) ;
		String fsetenvGatewayipval=commandsList.get(16) ;
		String fsetenvStartepcval=commandsList.get(17) ;
		
		boolean mkdirSendConfig = false;
		boolean cpConfig = false;
		boolean cpStartup = false;
		boolean chmodDb = false;
		boolean chmodSendConfig = false;
		boolean chmodConfig = false;
		boolean cpHariff = false;
		boolean mountMntMnc = false;
		boolean umountMntMnc = false;
		boolean sync = false;
		boolean fsetenvBw = false;
		boolean fsetenvIpaddr = false;
		boolean fsetenvGtpip = false;
		boolean fsetenvNetmask = false;
		boolean fsetenvGatewayip = false;
		boolean fsetenvStartepc = false;
		boolean dfeI2cIap = false;
		boolean cdTmp = false;
		boolean cpTmp = false;
		boolean fileRfconfig = false;
		boolean mkdirDb = false;

		commands.setCpConfigvalue(cpConfigvalue);
		commands.setCpStartupvalue(cpStartupvalue);
		//commands.setCpConfigvalue(cpConfigvalue);
		commands.setCpHariffvalue(cpHariffvalue);
		commands.setFsetenvBwvalue(fsetenvBwval);
		commands.setFsetenvIpaddrvalue(fsetenvIpaddrval);
		commands.setFsetenvGtpipvalue(fsetenvGtpipval);
		commands.setFsetenvNetmaskvalue(fsetenvNetmaskval);
		commands.setFsetenvGatewayipvalue(fsetenvGatewayipval);
		commands.setFsetenvStartepcvalue(fsetenvStartepcval);
		
		for (int i = 0; i < itemsArray.length; i++) {
			String str = itemsArray[i];
			//System.out.println(str);
			if (str.startsWith("[mkdir /opt/resonous/")) {
				if (str.equalsIgnoreCase("[mkdir /opt/resonous/" + folderName)) {
					mkdirSendConfig = true;
					commands.setMkdirSendConfig(mkdirSendConfig);
				} else {
					mkdirSendConfig = false;
					commands.setMkdirSendConfig(mkdirSendConfig);
				}
			}

			if (str.startsWith("cp /opt/resonous/config/config")) {
				if (str.equals("cp /opt/resonous/config/config" + hostIpAddress + ".xml" + " /opt/resonous/"
						+ folderName + "/config.xml")) {
					cpConfig = true;
					commands.setCpConfig(cpConfig);
				} else {
					cpConfig = false;
					commands.setCpConfig(cpConfig);
				}
			}

			if (str.startsWith("cp /opt/resonous/config/startup")) {
				if (str.equals("cp /opt/resonous/config/startup" + hostIpAddress + ".cfg" + " /opt/resonous/"
						+ folderName + "/startup.cfg")) {
					cpStartup = true;
					commands.setCpStartup(cpStartup);
				} else {
					cpStartup = false;
					commands.setCpStartup(cpStartup);
				}
			}

			if (str.startsWith("chmod -R 777 /opt/resonous/sqlite/db")) {
				if (str.equals("chmod -R 777 /opt/resonous/sqlite/db")) {
					chmodDb = true;
					commands.setChmodDb(chmodDb);
				} else {
					chmodDb = false;
					commands.setChmodDb(chmodDb);
				}
			}

			if (str.startsWith("chmod -R 777 /opt/resonous/sendConfig")) {
				if (str.equals("chmod -R 777 /opt/resonous/sendConfig")) {
					chmodSendConfig = true;
					commands.setChmodSendConfig(chmodSendConfig);
				} else {
					chmodSendConfig = false;
					commands.setChmodSendConfig(chmodSendConfig);
				}
			}

			if (str.startsWith("chmod -R 777 /opt/resonous/config")) {
				if (str.equals("chmod -R 777 /opt/resonous/config")) {
					chmodConfig = true;
					commands.setChmodConfig(chmodConfig);
				} else {
					chmodConfig = false;
					commands.setChmodConfig(chmodConfig);
				}
			}

			if (str.startsWith("cp /opt/resonous/config/hariff")) {
				if (str.equals("cp /opt/resonous/config/hariff" + hostIpAddress + ".xml" + " /opt/resonous/"
						+ folderName + "/hariff.xml")) {
					cpHariff = true;
					commands.setCpHariff(cpHariff);
				} else {
					cpHariff = false;
					commands.setCpHariff(cpHariff);
				}
			}

			if (str.startsWith("mount /dev/mmcblk0p1 /mnt/mmc/")) {
				if (str.equals("mount /dev/mmcblk0p1 /mnt/mmc/")) {
					mountMntMnc = true;
					commands.setMountMntMnc(mountMntMnc);
				} else {
					mountMntMnc = false;
					commands.setMountMntMnc(mountMntMnc);
				}
			}

			if (str.startsWith("umount /mnt/mmc/")) {
				if (str.equals("umount /mnt/mmc/")) {
					umountMntMnc = true;
					commands.setUmountMntMnc(umountMntMnc);
				} else {
					umountMntMnc = false;
					commands.setUmountMntMnc(umountMntMnc);
				}
			}

			if (str.startsWith("fsetenv bw")) {
				if (str.equals("fsetenv bw " + bwValue)) {
					fsetenvBw = true;
					commands.setFsetenvBw(fsetenvBw);
				} else {
					fsetenvBw = false;
					commands.setFsetenvBw(fsetenvBw);
				}
			}

			if (str.startsWith("fsetenv ipaddr")) {
				if (str.equals("fsetenv ipaddr " + hostIpAddress)) {
					fsetenvIpaddr = true;
					commands.setFsetenvIpaddr(fsetenvIpaddr);
				} else {
					fsetenvIpaddr = false;
					commands.setFsetenvIpaddr(fsetenvIpaddr);
				}
			}

			if (str.startsWith("fsetenv gtpip")) {
				if (str.equals("fsetenv gtpip " + hostIpAddress)) {
					fsetenvGtpip = true;
					commands.setFsetenvGtpip(fsetenvGtpip);
				} else {
					fsetenvGtpip = false;
					commands.setFsetenvGtpip(fsetenvGtpip);
				}
			}

			if (str.startsWith("fsetenv netmask")) {
				if (str.equals("fsetenv netmask " + enbMask)) {
					fsetenvNetmask = true;
					commands.setFsetenvNetmask(fsetenvNetmask);
				} else {
					fsetenvNetmask = false;
					commands.setFsetenvNetmask(fsetenvNetmask);
				}
			}

			if (str.startsWith("fsetenv gatewayip")) {
				if (str.equals("fsetenv gatewayip " + enbGwIp)) {
					fsetenvGatewayip = true;
					commands.setFsetenvGatewayip(fsetenvGatewayip);
				} else {
					fsetenvGatewayip = false;
					commands.setFsetenvGatewayip(fsetenvGatewayip);
				}
			}

			if (str.startsWith("fsetenv startepc")) {
				if (str.equals("fsetenv startepc " + flag)) {
					fsetenvStartepc = true;
					commands.setFsetenvStartepc(fsetenvStartepc);
				} else {
					fsetenvStartepc = false;
					commands.setFsetenvStartepc(fsetenvStartepc);
				}
			}

			if (str.startsWith("mkdir /mnt/mmc/db/")) {
				if (str.equals("mkdir /mnt/mmc/db/")) {
					mkdirDb = true;
					commands.setMkdirDb(mkdirDb);
				} else {
					mkdirDb = false;
					commands.setMkdirDb(mkdirDb);
				}
			}

			if (str.startsWith("sync")) {
				if (str.equals("sync")) {
					sync = true;
					commands.setSync(sync);
				} else {
					sync = false;
					commands.setSync(sync);
				}
			}

			if (str.startsWith("/opt/resonous/config/RFCONFIG.cfg")) {
				if (str.equalsIgnoreCase("/opt/resonous/config/RFCONFIG.cfg]")) {
					fileRfconfig = true;
					commands.setFileRfconfig(fileRfconfig);
				} else {
					fileRfconfig = false;
					commands.setFileRfconfig(fileRfconfig);
				}
			}

			if (str.startsWith("cp /mnt/mmc/dfe_i2c_iap /tmp")) {
				if (str.equals("cp /mnt/mmc/dfe_i2c_iap /tmp")) {
					cpTmp = true;
					commands.setCpTmp(cpTmp);
				} else {
					cpTmp = false;
					commands.setCpTmp(cpTmp);
				}
			}

			if (str.startsWith("./dfe_i2c_iap ")) {
				if (str.equals("./dfe_i2c_iap " + channelBandwidth)) {
					dfeI2cIap = true;
					commands.setDfeI2cIap(dfeI2cIap);
				} else {
					dfeI2cIap = false;
					commands.setDfeI2cIap(dfeI2cIap);
				}
			}

			if (str.startsWith("cd /tmp/;")) {
				if (str.equals("cd /tmp/; ./dfe_i2c_iap " + channelBandwidth)) {
					cdTmp = true;
					commands.setCdTmp(cdTmp);
				} else {
					cdTmp = false;
					commands.setCdTmp(cdTmp);
				}
			}

		}

		if (mkdirSendConfig == true && cpConfig == true && cdTmp == true && dfeI2cIap == true && cpTmp == true
				&& fileRfconfig == true && sync == true && mkdirDb == true && fsetenvStartepc == true
				&& fsetenvNetmask == true && fsetenvGtpip == true && fsetenvIpaddr == true && fsetenvBw == true
				&& umountMntMnc == true && mountMntMnc == true && cpHariff == true && chmodSendConfig == true
				&& chmodDb == true && cpStartup == true && chmodConfig == true && fsetenvGatewayip == true) {

			auditLog.setCommandStatus("Success");
			auditLog.setResponseTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
			
			commands.setCommandStatus("Success");
			commands.setResponseTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		} else {
			auditLog.setCommandStatus("Fail");
			auditLog.setResponseTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
			
			commands.setCommandStatus("Fail");
			commands.setResponseTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		}

		dao.save(auditLog);
		commands.setAuditLog(auditLog);
		editCommandsDao.save(commands);

		return "sshSuccess";
	}

	public String upgradeSdCard(String hostIpAddress, String fileNameWithOutExt, String fileName, String nibName,
			String bwValue, String enbMask, String enbGwIp, String flag, String channelBandwidth,
			List<String> commandsList) {

		AuditLog auditLog = new AuditLog();
		auditLog.setInterfaceName(nibName);
		auditLog.setIpAddress(hostIpAddress);
		auditLog.setStatus("Active");
		auditLog.setRequestTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));

		BringupUpgradeCommands commands = new BringupUpgradeCommands();
		commands.setRequestTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		
		String[] itemsArray = new String[commandsList.size()];
		itemsArray = (String[]) commandsList.toArray(itemsArray);

		String cpConfigvalue=commandsList.get(3);;
		String cpStartupvalue =commandsList.get(4);
		String cpHariffvalue=commandsList.get(5);
		
		String fsetenvBwvalue =commandsList.get(13) ;
		String fsetenvIpaddrvalue =commandsList.get(14);
		String fsetenvGtpipvalue=commandsList.get(15) ;
		String fsetenvNetmaskvalue=commandsList.get(16) ;
		String fsetenvGatewayipvalue=commandsList.get(17) ;
		String fsetenvStartepcvalue=commandsList.get(18) ;
		
		commands.setCpConfigvalue(cpConfigvalue);
		commands.setCpHariffvalue(cpHariffvalue);
		commands.setCpStartupvalue(cpStartupvalue);
		
		commands.setFsetenvBwvalue(fsetenvBwvalue);
		commands.setFsetenvIpaddrvalue(fsetenvIpaddrvalue);
		commands.setFsetenvGtpipvalue(fsetenvGtpipvalue);
		commands.setFsetenvNetmaskvalue(fsetenvNetmaskvalue);
		commands.setFsetenvGatewayipvalue(fsetenvGatewayipvalue);
		commands.setFsetenvStartepcvalue(fsetenvStartepcvalue);
		
		boolean chmodUpgradeFile = false;
		boolean tarExtractUpgradeFile = false;
		boolean cpConfig = false;
		boolean cpStartup = false;
		boolean cpHariff = false;
		boolean chmodDb = false;
		boolean chmodUpgradeFolder = false;
		boolean chmodConfig = false;
		boolean mvUpgradeFile = false;
		boolean mountMntMnc = false;
		boolean umountMntMnc = false;
		boolean sync = false;
		boolean fsetenvBw = false;
		boolean fsetenvIpaddr = false;
		boolean fsetenvGtpip = false;
		boolean fsetenvNetmask = false;
		boolean fsetenvGatewayip = false;
		boolean fsetenvStartepc = false;
		boolean fileRfconfig = false;
		boolean mkdirDb = false;

		for (int i = 0; i < itemsArray.length; i++) {
			String str = itemsArray[i];
			System.out.println(str);
			if (str.startsWith("[chmod 777 /opt/resonous/" + fileName)) {
				if (str.equalsIgnoreCase("[chmod 777 /opt/resonous/" + fileName)) {
					chmodUpgradeFile = true;
					commands.setChmodUpgradeFile(chmodUpgradeFile);
				} else {
					chmodUpgradeFile = false;
					commands.setChmodUpgradeFile(chmodUpgradeFile);
				}
			}

			if (str.startsWith("tar zxvf /opt/resonous/" + fileName + " -C /opt/resonous/")) {
				if (str.equalsIgnoreCase("tar zxvf /opt/resonous/" + fileName + " -C /opt/resonous/")) {
					tarExtractUpgradeFile = true;
					commands.setTarExtractUpgradeFile(tarExtractUpgradeFile);
				} else {
					tarExtractUpgradeFile = false;
					commands.setTarExtractUpgradeFile(tarExtractUpgradeFile);
				}
			}

			if (str.startsWith("cp /opt/resonous/config/config")) {
				if (str.equals("cp /opt/resonous/config/config" + hostIpAddress + ".xml" + " /opt/resonous/"
						+ fileNameWithOutExt + "/config.xml")) {
					cpConfig = true;
					commands.setCpConfig(cpConfig);
				} else {
					cpConfig = false;
					commands.setCpConfig(cpConfig);
				}
			}

			if (str.startsWith("cp /opt/resonous/config/startup")) {
				if (str.equals("cp /opt/resonous/config/startup" + hostIpAddress + ".cfg" + " /opt/resonous/"
						+ fileNameWithOutExt + "/startup.cfg")) {
					cpStartup = true;
					commands.setCpStartup(cpStartup);
				} else {
					cpStartup = false;
					commands.setCpStartup(cpStartup);
				}
			}

			if (str.startsWith("cp /opt/resonous/config/hariff")) {
				if (str.equals("cp /opt/resonous/config/hariff" + hostIpAddress + ".xml" + " /opt/resonous/"
						+ fileNameWithOutExt + "/hariff.xml")) {
					cpHariff = true;
					commands.setCpHariff(cpHariff);
				} else {
					cpHariff = false;
					commands.setCpHariff(cpHariff);
				}
			}

			if (str.startsWith("chmod -R 777 /opt/resonous/sqlite/db")) {
				if (str.equals("chmod -R 777 /opt/resonous/sqlite/db")) {
					chmodDb = true;
					commands.setChmodDb(chmodDb);
				} else {
					chmodDb = false;
					commands.setChmodDb(chmodDb);
				}
			}

			if (str.startsWith("chmod -R 777 /opt/resonous/" + fileNameWithOutExt)) {
				if (str.equals("chmod -R 777 /opt/resonous/" + fileNameWithOutExt)) {
					chmodUpgradeFolder = true;
					commands.setChmodUpgradeFolder(chmodUpgradeFolder);
				} else {
					chmodUpgradeFolder = false;
					commands.setChmodUpgradeFolder(chmodUpgradeFolder);
				}
			}

			if (str.startsWith("chmod -R 777 /opt/resonous/config")) {
				if (str.equals("chmod -R 777 /opt/resonous/config")) {
					chmodConfig = true;
					commands.setChmodConfig(chmodConfig);
				} else {
					chmodConfig = false;
					commands.setChmodConfig(chmodConfig);
				}
			}

			if (str.startsWith("mv -f /opt/resonous/" + fileNameWithOutExt + " /opt/resonous/" + fileNameWithOutExt
					+ "epc.tar.gz")) {
				if (str.equals("mv -f /opt/resonous/" + fileNameWithOutExt + " /opt/resonous/" + fileNameWithOutExt
						+ "epc.tar.gz")) {
					mvUpgradeFile = true;
					commands.setMvUpgradeFile(mvUpgradeFile);
				} else {
					mvUpgradeFile = false;
					commands.setMvUpgradeFile(mvUpgradeFile);
				}
			}

			if (str.startsWith("mount /dev/mmcblk0p1 /mnt/mmc/")) {
				if (str.equals("mount /dev/mmcblk0p1 /mnt/mmc/")) {
					mountMntMnc = true;
					commands.setMountMntMnc(mountMntMnc);
				} else {
					mountMntMnc = false;
					commands.setMountMntMnc(mountMntMnc);
				}
			}

			if (str.startsWith("umount /mnt/mmc/")) {
				if (str.equals("umount /mnt/mmc/")) {
					umountMntMnc = true;
					commands.setUmountMntMnc(umountMntMnc);
				} else {
					umountMntMnc = false;
					commands.setUmountMntMnc(umountMntMnc);
				}
			}

			if (str.startsWith("fsetenv bw")) {
				if (str.equals("fsetenv bw " + bwValue)) {
					fsetenvBw = true;
					commands.setFsetenvBw(fsetenvBw);
				} else {
					fsetenvBw = false;
					commands.setFsetenvBw(fsetenvBw);
				}
			}

			if (str.startsWith("fsetenv ipaddr")) {
				if (str.equals("fsetenv ipaddr " + hostIpAddress)) {
					fsetenvIpaddr = true;
					commands.setFsetenvIpaddr(fsetenvIpaddr);
				} else {
					fsetenvIpaddr = false;
					commands.setFsetenvIpaddr(fsetenvIpaddr);
				}
			}

			if (str.startsWith("fsetenv gtpip")) {
				if (str.equals("fsetenv gtpip " + hostIpAddress)) {
					fsetenvGtpip = true;
					commands.setFsetenvGtpip(fsetenvGtpip);
				} else {
					fsetenvGtpip = false;
					commands.setFsetenvGtpip(fsetenvGtpip);
				}
			}

			if (str.startsWith("fsetenv netmask")) {
				if (str.equals("fsetenv netmask " + enbMask)) {
					fsetenvNetmask = true;
					commands.setFsetenvNetmask(fsetenvNetmask);
				} else {
					fsetenvNetmask = false;
					commands.setFsetenvNetmask(fsetenvNetmask);
				}
			}

			if (str.startsWith("fsetenv gatewayip")) {
				if (str.equals("fsetenv gatewayip " + enbGwIp)) {
					fsetenvGatewayip = true;
					commands.setFsetenvGatewayip(fsetenvGatewayip);
				} else {
					fsetenvGatewayip = false;
					commands.setFsetenvGatewayip(fsetenvGatewayip);
				}
			}

			if (str.startsWith("fsetenv startepc")) {
				if (str.equals("fsetenv startepc " + flag)) {
					fsetenvStartepc = true;
					commands.setFsetenvStartepc(fsetenvStartepc);
				} else {
					fsetenvStartepc = false;
					commands.setFsetenvStartepc(fsetenvStartepc);
				}
			}

			if (str.startsWith("mkdir /mnt/mmc/db/")) {
				if (str.equals("mkdir /mnt/mmc/db/")) {
					mkdirDb = true;
					commands.setMkdirDb(mkdirDb);
				} else {
					mkdirDb = false;
					commands.setMkdirDb(mkdirDb);
				}
			}

			if (str.startsWith("sync")) {
				if (str.equals("sync")) {
					sync = true;
					commands.setSync(sync);
				} else {
					sync = false;
					commands.setSync(sync);
				}
			}

			if (str.startsWith("/opt/resonous/config/RFCONFIG.cfg")) {
				if (str.equalsIgnoreCase("/opt/resonous/config/RFCONFIG.cfg]")) {
					fileRfconfig = true;
					commands.setFileRfconfig(fileRfconfig);
				} else {
					fileRfconfig = false;
					commands.setFileRfconfig(fileRfconfig);
				}
			}

		}

		if (chmodUpgradeFile == true && tarExtractUpgradeFile == true && cpConfig == true && cpStartup == true
				&& cpHariff == true && chmodDb == true && chmodUpgradeFolder == true && chmodConfig == true
				&& mvUpgradeFile == true && mountMntMnc == true && umountMntMnc == true && sync == true
				&& fsetenvBw == true && fsetenvIpaddr == true && fsetenvGtpip == true && fsetenvNetmask == true
				&& fsetenvGatewayip == true && fsetenvStartepc == true && fileRfconfig == true && mkdirDb == true) {

			auditLog.setCommandStatus("Success");
			auditLog.setResponseTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
			
			commands.setCommandStatus("Success");
			commands.setResponseTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		} else {
			auditLog.setCommandStatus("Fail");
			auditLog.setResponseTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
			
			commands.setCommandStatus("Fail");
			commands.setResponseTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		}

		dao.save(auditLog);
		commands.setAuditLog(auditLog);
		bringupDao.save(commands);

		return "sshSuccess";
	}
	
	public String reboot(String user, String password, String hostIpAddress, Integer port, String rebootCommand, String nibName) {
		AuditLog auditLog = new AuditLog();
		auditLog.setInterfaceName(nibName);
		auditLog.setIpAddress(hostIpAddress);
		auditLog.setStatus("Active");
		auditLog.setRequestTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		
		RebootCommands commands = new RebootCommands();
		commands.setRequestTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		
		boolean command = false;
		
		if (rebootCommand.startsWith("reboot")) {
			if (rebootCommand.equalsIgnoreCase("reboot")) {
				command = true;
				commands.setRebootCommand(command);
			} else {
				command = false;
				commands.setRebootCommand(command);
			}
		}
		
		if (command == true ) {
			auditLog.setCommandStatus("Success");
			auditLog.setResponseTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
			
			commands.setCommandStatus("Success");
			commands.setResponseTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		} else {
			auditLog.setCommandStatus("Fail");
			auditLog.setResponseTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
			
			commands.setCommandStatus("Fail");
			commands.setResponseTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		}

		dao.save(auditLog);
		commands.setAuditLog(auditLog);
		rebootCommandsDao.save(commands);
		
		return "sshSuccess";
	}
}