package com.simulator.service.nib;

import java.util.List;

public interface Serivicefordashboard {

	public String nibDashboardvalues(String user, String password, String ipAddress, Integer port, String nibName, List<String> commandsList);

	public String sendConfig(String hardwareName, String hostIpAddress, String uploadFilePath, String folderName,
			String bwValue, String enbMask, String enbGwIp, String flag, String channelBandwidth,
			List<String> commandsList);
	
	public String upgradeSdCardvalue(String hostIpAddress, String uploadFilePath, String fileName, String nibName,
			String bwValue, String enbMask, String enbGwIp, String flag, String channelBandwidth,
			List<String> commandsList);
}
