package com.simulator.service.nib;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.simulator.dao.AuditLog;
import com.simulator.dao.AuditLogDao;
import com.simulator.dao.Bringupupgradvalues;
import com.simulator.dao.BringupupgradvaluesDao;
import com.simulator.dao.SendConfigcmdvalues;
import com.simulator.dao.SndconDao;
import com.simulator.dao.valuesFromNms;
import com.simulator.dao.valuesFromNmsDao;

@Service
@Transactional
public class Servicefordashboardimpl implements Serivicefordashboard {

	@Autowired
	private AuditLogDao dao;
	
	@Autowired
	private valuesFromNmsDao nmsvalues;
	
	@Autowired
	private SndconDao sndcfgvaluesdao;
	
	@Autowired
	private BringupupgradvaluesDao bupdao;
	
	public String nibDashboardvalues(String user, String password, String ipAddress, Integer port, String nibName,
			List<String> commandsList) {
		
		ObjectMapper mapper = new ObjectMapper();

		List<String> list = new ArrayList<String>();
		
		if ((user != null && user != "") && (password != null && password != "") && commandsList != null
				&& (ipAddress != null && ipAddress != "") && (nibName != null && nibName != "") && port != null) {
			list.add("Power amplifier 1 output power 100000");
			list.add("Power amplifier 2 output power 44411");
			list.add("Power amplifier 1 temperature 1434");
			list.add("Power amplifier 2 temperature 106666");
			list.add("Uplink Frequency is 30000");
			list.add("Downlink Frequency is 34000");
		}
		
		
		try {
		    valuesFromNms values=new valuesFromNms();
		    
		    AuditLog auditLog = new AuditLog();
			auditLog.setInterfaceName(nibName);
			auditLog.setIpAddress(ipAddress);
			auditLog.setStatus("Active");
			auditLog.setRequestTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		    
		    String cli = commandsList.get(0).substring(1);
			String dfePowerTemperatureGetAll = commandsList.get(1);
			String dfeGrequencyGet = commandsList.get(2);
			String exit = commandsList.get(3);
			String mmeEnbInfoIp = commandsList.get(5).substring(0, commandsList.get(5).length() - 1);
		    
			values.setCli(cli);
		    values.setDfePowerTemperatureGetAll(dfePowerTemperatureGetAll);
		    values.setDfeGrequencyGet(dfeGrequencyGet);
		    values.setExit(exit);
		    values.setMmeEnbInfoIp(mmeEnbInfoIp);
		    values.setRequestTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		    
		    
		    if (mmeEnbInfoIp!=null && exit!=null && dfeGrequencyGet!=null && dfePowerTemperatureGetAll !=null
					&& cli !=null) {

				auditLog.setCommandStatus("Success");
				auditLog.setResponseTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
				
				values.setCommandStatus("Success");
				values.setResponseTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
			} else {
				auditLog.setCommandStatus("Fail");
				auditLog.setResponseTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
				
				values.setCommandStatus("Fail");
				values.setResponseTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
			}
		    
		    
		    dao.save(auditLog);
		    values.setAuditLog(auditLog);
		    nmsvalues.save(values);
		   
		    
		 // convert map to JSON string
		 			String json = mapper.writeValueAsString(list);
		 			System.out.println(json); // compact-print
		 			json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(list);
		 			System.out.println(json); // pretty-print
		 			
		 			return json;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String sendConfig(String hardwareName, String hostIpAddress, String uploadFilePath, String folderName,
			String bwValue, String enbMask, String enbGwIp, String flag, String channelBandwidth,
			List<String> commandsList) {
		
		AuditLog auditLog = new AuditLog();
		auditLog.setInterfaceName(hardwareName);
		auditLog.setIpAddress(hostIpAddress);
		auditLog.setStatus("Active");
		auditLog.setRequestTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		
		SendConfigcmdvalues sndconfigvalues=new SendConfigcmdvalues();
		sndconfigvalues.setRequestTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		
		String mkdirSendConfig=commandsList.get(0).substring(1);
		String cpConfig =commandsList.get(1);
		String cpStartup =commandsList.get(2);
		String chmodDb =commandsList.get(3);
		String chmodSendConfig=commandsList.get(4) ;
		String chmodConfig=commandsList.get(5) ;
		String cpHariff =commandsList.get(7);
		System.out.println("cpHariff"+cpHariff);
		String mountMntMnc =commandsList.get(8);
		String umountMntMnc =commandsList.get(9);
		String mkdirDb=commandsList.get(10); ;
		String sync =commandsList.get(11);
		String fsetenvBw=commandsList.get(12) ;
		String fsetenvIpaddr=commandsList.get(13) ;
		String fsetenvGtpip =commandsList.get(14);
		String fsetenvNetmask=commandsList.get(15) ;
		String fsetenvGatewayip=commandsList.get(16) ;
		String fsetenvStartepc=commandsList.get(17) ;
		String dfeI2cIap =commandsList.get(18);
		String cdTmp =commandsList.get(19);
		String cpTmp =commandsList.get(20);
		String fileRfconfig=commandsList.get(21).substring(0, commandsList.get(21).length() - 1) ;
		
		//sndconfigvalues.setAuditLog(auditLog);
		sndconfigvalues.setMkdirSendConfig(mkdirSendConfig);
		sndconfigvalues.setCpConfig(cpConfig);
		sndconfigvalues.setCpStartup(cpStartup);
		sndconfigvalues.setChmodDb(chmodDb);
		sndconfigvalues.setChmodSendConfig(chmodSendConfig);
		sndconfigvalues.setChmodConfig(chmodConfig);
		sndconfigvalues.setCpHariff(cpHariff);
		sndconfigvalues.setMountMntMnc(mountMntMnc);
		sndconfigvalues.setUmountMntMnc(umountMntMnc);
		sndconfigvalues.setSync(sync);
		sndconfigvalues.setFsetenvBw(fsetenvBw);
		sndconfigvalues.setFsetenvIpaddr(fsetenvIpaddr);
		sndconfigvalues.setFsetenvGtpip(fsetenvGtpip);
		sndconfigvalues.setFsetenvNetmask(fsetenvNetmask);
		sndconfigvalues.setFsetenvGatewayip(fsetenvGatewayip);
		sndconfigvalues.setFsetenvStartepc(fsetenvStartepc);
		sndconfigvalues.setDfeI2cIap(dfeI2cIap);
		sndconfigvalues.setCdTmp(cdTmp);
		sndconfigvalues.setCpTmp(cpTmp);
		sndconfigvalues.setFileRfconfig(fileRfconfig);
		sndconfigvalues.setMkdirDb(mkdirDb);
		
		
		if (mkdirSendConfig !=null && cpConfig !=null&& cdTmp !=null && dfeI2cIap!=null && cpTmp!=null
				&& fileRfconfig!=null && sync!=null && mkdirDb!=null && fsetenvStartepc!=null
				&& fsetenvNetmask!=null && fsetenvGtpip!=null && fsetenvIpaddr!=null && fsetenvBw!=null
				&& umountMntMnc!=null&& mountMntMnc!=null && cpHariff !=null && chmodSendConfig !=null
				&& chmodDb !=null&& cpStartup !=null && chmodConfig!=null && fsetenvGatewayip !=null) {

			auditLog.setCommandStatus("Success");
			auditLog.setResponseTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
			
			sndconfigvalues.setCommandStatus("Success");
			sndconfigvalues.setResponseTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		} else {
			auditLog.setCommandStatus("Fail");
			auditLog.setResponseTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
			
			sndconfigvalues.setCommandStatus("Fail");
			sndconfigvalues.setResponseTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		}
		dao.save(auditLog);
		sndconfigvalues.setAuditLog(auditLog);
		sndcfgvaluesdao.save(sndconfigvalues);
		
		return "sndcfigsuccess";
	}

	public String upgradeSdCardvalue(String hostIpAddress, String uploadFilePath, String fileName, String nibName,
			String bwValue, String enbMask, String enbGwIp, String flag, String channelBandwidth,
			List<String> commandsList) {
		AuditLog auditLog = new AuditLog();
		auditLog.setInterfaceName(nibName);
		auditLog.setIpAddress(hostIpAddress);
		auditLog.setStatus("Active");
		auditLog.setRequestTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		
		Bringupupgradvalues values=new Bringupupgradvalues();
		values.setRequestTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		
		String chmodUpgradeFile =commandsList.get(0).substring(1);
		String tarExtractUpgradeFile =commandsList.get(1);
		String mvUpgradeFile=commandsList.get(2);
		String cpConfig =commandsList.get(3);
		String cpStartup =commandsList.get(4);
		String cpHariff =commandsList.get(5);
		String chmodDb =commandsList.get(6);
		String chmodUpgradeFolder =commandsList.get(7);
		String chmodConfig=commandsList.get(8) ;
		
		String mountMntMnc=commandsList.get(9);
		String umountMntMnc =commandsList.get(10);
		String mkdirDb =commandsList.get(11);
		String sync=commandsList.get(12) ;
		String fsetenvBw =commandsList.get(13) ;
		String fsetenvIpaddr =commandsList.get(14);
		String fsetenvGtpip=commandsList.get(15) ;
		String fsetenvNetmask=commandsList.get(16) ;
		String fsetenvGatewayip=commandsList.get(17) ;
		String fsetenvStartepc=commandsList.get(18) ;
		String fileRfconfig=commandsList.get(19).substring(0, commandsList.get(19).length() - 1) ;
		
		values.setChmodUpgradeFile(chmodUpgradeFile);
		values.setTarExtractUpgradeFile(tarExtractUpgradeFile);
		values.setMvUpgradeFile(mvUpgradeFile);
		values.setCpConfig(cpConfig);
		values.setCpHariff(cpHariff);
		values.setCpStartup(cpStartup);
		values.setChmodDb(chmodDb);
		values.setChmodUpgradeFolder(chmodUpgradeFolder);
		values.setChmodConfig(chmodConfig);
		values.setMountMntMnc(mountMntMnc);
		values.setUmountMntMnc(umountMntMnc);
		
		values.setMkdirDb(mkdirDb);
		values.setSync(sync);
		values.setFsetenvBw(fsetenvBw);
		values.setFsetenvIpaddr(fsetenvIpaddr);
		values.setFsetenvGtpip(fsetenvGtpip);
		values.setFsetenvNetmask(fsetenvNetmask);
		values.setFsetenvGatewayip(fsetenvGatewayip);
		values.setFsetenvStartepc(fsetenvStartepc);
		values.setFileRfconfig(fileRfconfig);
		
		
		if (chmodUpgradeFile !=null && tarExtractUpgradeFile!=null  && cpConfig !=null && cpStartup !=null 
				&& cpHariff!=null  && chmodDb !=null  && chmodUpgradeFolder !=null  && chmodConfig !=null 
				&& mvUpgradeFile !=null  && mountMntMnc !=null  && umountMntMnc !=null  && sync !=null 
				&& fsetenvBw !=null  && fsetenvIpaddr !=null  && fsetenvGtpip !=null  && fsetenvNetmask!=null 
				&& fsetenvGatewayip !=null && fsetenvStartepc !=null  && fileRfconfig !=null  && mkdirDb !=null ) {

			auditLog.setCommandStatus("Success");
			auditLog.setResponseTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
			
			values.setCommandStatus("Success");
			values.setResponseTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		} else {
			auditLog.setCommandStatus("Fail");
			auditLog.setResponseTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
			
			values.setCommandStatus("Fail");
			values.setResponseTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		}
		dao.save(auditLog);
		values.setAuditLog(auditLog);
		bupdao.save(values);
		
		return "upgradesuccess";
	}

}
