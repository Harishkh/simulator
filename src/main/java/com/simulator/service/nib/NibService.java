package com.simulator.service.nib;

import java.util.List;

public interface NibService {
	public String reboot(String user,String password, String hostIpAddress, Integer port, String rebootCommand, String nibName);

	public String nibDashboard(String user, String password, String ipAddress, Integer port, String nibName, List<String> commandsList);

	public String sendConfig(String hardwareName, String hostIpAddress, String uploadFilePath, String folderName,
			String bwValue, String enbMask, String enbGwIp, String flag, String channelBandwidth,
			List<String> commandsList);

	public String upgradeSdCard(String hostIpAddress, String uploadFilePath, String fileName, String nibName,
			String bwValue, String enbMask, String enbGwIp, String flag, String channelBandwidth,
			List<String> commandsList);
}