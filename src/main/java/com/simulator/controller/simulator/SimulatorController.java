package com.simulator.controller.simulator;

import java.net.InetAddress;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.simulator.dao.AuditLog;
import com.simulator.service.simulator.SimulatorService;

@Controller
public class SimulatorController {
	private static final Logger log = LoggerFactory.getLogger(SimulatorController.class);

	@Autowired
	private SimulatorService simulatirService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String welcomePage(Model model, HttpServletRequest request, HttpServletResponse response) {
		log.info("Inside welcomePage");
		
		try {
			
			Set<AuditLog> list = simulatirService.findAllAuditLogs();
			model.addAttribute("list", list);

		} catch (Exception e) {
			log.error("welcomePage", e);
			e.printStackTrace();
		}
		
		log.info("End of welcomePage");
		return "listOfUsers";

	}

	@RequestMapping(value = "/viewPage", method = RequestMethod.GET)
	public String viewPage(Model model, HttpServletRequest request, HttpServletResponse response) {
		log.info("Inside viewPage");

		try {

			String id = request.getParameter("id");
			Integer auditId = Integer.parseInt(id);
			AuditLog auditLog = simulatirService.findById(auditId);
			
			String ip = request.getParameter("ip");
			Object ipAddress = ip;
			List<AuditLog> auditLogList = simulatirService.findAllByIpAddress(ipAddress);

			
			
			model.addAttribute("auditLog", auditLog);
			//model.addAttribute("auditLogvalues", auditLog.getNmsvalues());
			model.addAttribute("auditLogList", auditLogList);

		} catch (Exception e) {
			log.error("viewPage", e);
			e.printStackTrace();
		}

		log.info("End of viewPage");
		return "viewPage";
	}
	
	@RequestMapping(value = "/listOfUsers", method = RequestMethod.GET)
	public String listOfUsers(Model model, HttpServletRequest request, HttpServletResponse response) {
		log.info("Inside viewPage");

		try {

			Set<AuditLog> list = simulatirService.findAllAuditLogs();
			model.addAttribute("list", list);

		} catch (Exception e) {
			log.error("welcomePage", e);
			e.printStackTrace();
		}

		log.info("End of welcomePage");
		return "listOfUsers";
	}

}