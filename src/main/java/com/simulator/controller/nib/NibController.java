package com.simulator.controller.nib;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.simulator.service.nib.NibService;
import com.simulator.service.nib.Serivicefordashboard;

@RestController
public class NibController {
	private static final Logger log = LoggerFactory.getLogger(NibController.class);

	@Autowired
	private NibService nibService;
	
	@Autowired
	private Serivicefordashboard nmsservice;

	@ResponseBody
	@RequestMapping(value = "/sendConfigThroughApi", method = RequestMethod.GET, produces = "application/json")
	public String sendConfigThroughApi(@RequestParam("hardwareName") String hardwareName,
			@RequestParam("hostIpAddress") String hostIpAddress, @RequestParam("uploadFilePath") String uploadFilePath,
			@RequestParam("folderName") String folderName, @RequestParam("bwValue") String bwValue,
			@RequestParam("enbMask") String enbMask, @RequestParam("enbGwIp") String enbGwIp,
			@RequestParam("flag") String flag, @RequestParam("channelBandwidth") String channelBandwidth,
			@RequestParam("commandsList") List<String> commandsList,Model model) {

		log.info("start of nibDashboardThroughApi");
		if ((hardwareName != null && hardwareName != "") && (hostIpAddress != null && hostIpAddress != "")
				&& (uploadFilePath != null && uploadFilePath != "") && (folderName != null && folderName != "")
				&& (commandsList != null)) {
			
			nmsservice.sendConfig(hardwareName, hostIpAddress, uploadFilePath, folderName, bwValue, enbMask, enbGwIp, flag, channelBandwidth, commandsList);
			
			return nibService.sendConfig(hardwareName, hostIpAddress, uploadFilePath, folderName, bwValue, enbMask,
					enbGwIp, flag, channelBandwidth, commandsList);
		}
		log.info("end of nibDashboardThroughApi");
		return null;
	}

	@ResponseBody
	@RequestMapping(value = "/upgradeSdCardThroughApi", method = RequestMethod.GET, produces = "application/json")
	public String upgradeSdCardThroughApi(@RequestParam("hostIpAddress") String hostIpAddress,
			@RequestParam("uploadFilePath") String uploadFilePath, @RequestParam("fileName") String fileName,
			@RequestParam("nibName") String nibName, @RequestParam("bwValue") String bwValue,
			@RequestParam("enbMask") String enbMask, @RequestParam("enbGwIp") String enbGwIp,
			@RequestParam("flag") String flag, @RequestParam("channelBandwidth") String channelBandwidth,
			@RequestParam("commandsList") List<String> commandsList,Model themodel) {

		log.info("nibDashboardThroughApi");
		if ((nibName != null && nibName != "") && (hostIpAddress != null && hostIpAddress != "")
				&& (uploadFilePath != null && uploadFilePath != "") && (fileName != null && fileName != "")) {
		
			nmsservice.upgradeSdCardvalue(hostIpAddress, uploadFilePath, fileName, nibName, bwValue, enbMask, enbGwIp, flag, channelBandwidth, commandsList);
			
			return nibService.upgradeSdCard(hostIpAddress, uploadFilePath, fileName, nibName, bwValue, enbMask, enbGwIp,
					flag, channelBandwidth, commandsList);
		}
		return null;
	}

	@ResponseBody
	@RequestMapping(value = "/rebootThroughApi", method = RequestMethod.GET, produces = "application/json")
	public String rebootThroughApi(@RequestParam("user") String user, @RequestParam("password") String password,
			@RequestParam("hostIpAddress") String hostIpAddress, @RequestParam("port") Integer port,
			@RequestParam("rebootCommand") String rebootCommand, @RequestParam("nibName") String nibName) {

		log.info("rebootThroughApi : " + hostIpAddress);
		if ((user != null && user != "") && (password != null && password != "") && (nibName != null && nibName != "")
				&& (hostIpAddress != null && hostIpAddress != "") && rebootCommand != null && port != null) {
			//System.out.println("rebootCommand:"+rebootCommand);
			return nibService.reboot(user, password, hostIpAddress, port, rebootCommand, nibName);
		}
		return null;
	}

	@ResponseBody
	@RequestMapping(value = "/nibDashboardThroughApi", method = RequestMethod.GET, produces = "application/json")
	public String nibDashboardThroughApi(@RequestParam("user") String user, @RequestParam("password") String password,
			@RequestParam("hostIpAddress") String hostIpAddress, @RequestParam("port") Integer port,
			@RequestParam("nibName") String nibName, @RequestParam("commandsList") List<String> commandsList,Model model) {

		log.info("nibDashboardThroughApi");
		if ((user != null && user != "") && (password != null && password != "")
				&& (hostIpAddress != null && hostIpAddress != "") && (hostIpAddress != null && hostIpAddress != "") && commandsList != null && port != null) {
			
			nmsservice.nibDashboardvalues(user, password, hostIpAddress, port, nibName, commandsList);
			return nibService.nibDashboard(user, password, hostIpAddress, port, nibName, commandsList);
		}
		log.info("nibDashboardThroughApi");
		return null;
	}

	@ResponseBody
	@RequestMapping(value = "/nibDashboardParameters", method = RequestMethod.GET, produces = "application/json")
	public String nibDashboardParameters(@RequestParam("status") String status) {
		return "success";
	}
	
}