<%@page import="java.util.List"%>
<%@ page import="com.simulator.dao.*"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Meta data -->
		<meta charset="UTF-8">
		<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
		<meta name="keywords" content="SMS Alert"/>

		<!-- Title -->
		<title>NMS Simulator | Dashboard</title>
		<!-- ================== BEGIN BASE CSS STYLE ================== -->
		<jsp:include page="../simulator/commonCss.jsp" />
		<!-- ================== END PAGE LEVEL STYLE ================== -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		
	</head>

	<body class="app " onload="myFunction()">
		<!--loader -->
		<div id="spinner">
			<img src="/Nms_Simulator/resources/assets/img/svgs/loader.svg" alt="loader">
		</div>

		<!--app open-->
		<div id="app" class="page">
			<div class="main-wrapper" >
				<div class="top-header" >
					<!--nav open-->
					<nav class="navbar navbar-expand-lg main-navbar" style="background-color: #5e10ac;">
						<div class="container">
							<a id="horizontal-navtoggle" class="animated-arrow hor-toggle" ><span></span></a>
							<h2 style="font-color: white;color: white;padding-left: 352px;"><b>NMS SIMULATOR</b></h2>
							<a class="header-brand" href="index.html"></a>
						</div>
					</nav>
					<!--nav closed-->
				</div>

                <!--app-content open-->
				<div class="container app-content">
					<section class="section">
						<div class="page-header">
							<div>
								<h4 class="page_header_h4">
									<ol class="breadcrumb"><!-- breadcrumb -->
										
									</ol><!-- End breadcrumb -->
								</h4>							
							</div>
						</div> 
						<div class="section-body">
                            <!--row open-->
							<div class="row">
								<div class="col-12">
									<div class="card">
										<div class="card-header">
											<h2 class="googlefont1">NMS Simulator</h2>
										</div>
										<div class="card-body">
											<div id="accordion">
												<div class="accordion">
													<div class="accordion-header" data-toggle="" data-target="#panel-body-1" style="background-color: #1997c7;">
														<h4 style="font-color: white; color: white;">${auditLog.ipAddress}</h4>
													</div>
													<div class="accordion-body collapse show border border-top-0" id="panel-body-1" data-parent="#accordion" >
														<div class="row">
															<div class="col-12">
																<div class="row row-space-10">
                            										<div class="col-lg-7 col-md-7 col-sm-7">
                                										<label>Name :</label>
                            										</div>
                            										<div class="col-lg-5 col-md-5 col-sm-5">
                                										${auditLog.interfaceName} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="badge badge-success badge-pill">${auditLog.status}</span>
                            										</div>
                        										</div>
                        									</div>
														</div>
														
														<div class="row">
															<div class="col-12">
																<div class="row row-space-10">
                            										<div class="col-lg-7 col-md-7 col-sm-7">
                                										<label>IP Address :</label>
                            										</div>
                            										<div class="col-lg-5 col-md-5 col-sm-5">
                                										${auditLog.ipAddress}
                            										</div>
                        										</div>
                        									</div>
														</div>
														
														<br>
														
														<!--row open-->
														<div class="row">
															<div class="col-12">
																<div class="card">
																	<div class="card-header">
																		<h3>User Activity</h3>
																	</div>
																		
																	<div class="card-body">
																		<ul class="nav nav-tabs" id="myTab" role="tablist">
																			<li class="nav-item ">
																				<a class="nav-link active gfontforlink" id="view-tab" data-toggle="tab" href="#view" role="tab" aria-controls="home" aria-selected="true">View</a>
																			</li>
																			<!-- <li class="nav-item">
																				<a class="nav-link" id="edit-tab" data-toggle="tab" href="#edit" role="tab" aria-controls="profile" aria-selected="false">Edit</a>
																			</li> -->
																			<li class="nav-item">
																				<a class="nav-link gfontforlink" id="bringup-tab" data-toggle="tab" href="#bringup" role="tab" aria-controls="contact" aria-selected="false">BringUp & Upgrade</a>
																			</li>
																			<li class="nav-item">
																				<a class="nav-link gfontforlink" id="sendConfig-tab" data-toggle="tab" href="#sendConfig" role="tab" aria-controls="home" aria-selected="true">SendConfig</a>
																			</li>
																			<li class="nav-item">
																				<a class="nav-link gfontforlink" id="reboot-tab" data-toggle="tab" href="#reboot" role="tab" aria-controls="profile" aria-selected="false">Reboot</a>
																			</li>
																			<li class="nav-item">
																				<a class="nav-link gfontforlink" id="dashboard-tab" data-toggle="tab" href="#dashboard" role="tab" aria-controls="contact" aria-selected="false">Dashboard</a>
																			</li>
																		</ul>
																			
											
																		<div class="tab-content  p-3" id="myTabContent">
																			<!-- view-tab open -->
																			<div class="tab-pane fade show active p-0" id="view" role="tabpanel" aria-labelledby="view-tab">
																				<table>
																					<c:if test = "${not empty auditLogList}">
			                                											<c:forEach var="auditLog" items="${auditLogList}">
			                                											
				                                												<c:forEach items="${auditLog.editCommandsList}" var="commands" >
																									<c:if test = "${not empty commands}">
																										<tr class="googlefontfortd">
																											<td>SendConfig Module Request Received time : ${commands.requestTime}</td>
																											<td>
																												<c:if test="${commands.commandStatus=='Success'}">
																										 			<span class="badge badge-success badge-pill">Success</span>
																										 		</c:if>
																										 		<c:if test="${commands.commandStatus=='Fail'}">
																										 			<span class="badge badge-secondary badge-pill">Failed</span>
																										 		</c:if>
																											</td>
																										</tr>
																									<tr>
																										<td>&nbsp;</td>
																									</tr>
																								</c:if>
																							</c:forEach>
																							
																							<c:forEach items="${auditLog.rebootCommandsList}" var="rebootCommands" >
																									<c:if test = "${not empty rebootCommands}">
																										<tr class="googlefontfortd">
																											<td width="900px">Reboot Module Request Received time : ${rebootCommands.requestTime}</td>
																											<td>
																												<c:if test="${rebootCommands.commandStatus=='Success'}">
																										 			<span class="badge badge-success badge-pill">Success</span>
																										 		</c:if>
																										 		<c:if test="${rebootCommands.commandStatus=='Fail'}">
																										 			<span class="badge badge-secondary badge-pill">Failed</span>
																										 		</c:if>
																											</td>
																										</tr>
																									<tr>
																										<td>&nbsp;</td>
																									</tr>
																								</c:if>
																							</c:forEach>
																							
																							<c:forEach items="${auditLog.bringupUpgradeCommandsList}" var="bringupCommands" >
																									<c:if test = "${not empty bringupCommands}">
																										<tr class="googlefontfortd">
																											<td width="900px">BringUp & Upgrade Module Request Received time : ${bringupCommands.requestTime}</td>
																											<td>
																												<c:if test="${bringupCommands.commandStatus=='Success'}">
																										 			<span class="badge badge-success badge-pill">Success</span>
																										 		</c:if>
																										 		<c:if test="${bringupCommands.commandStatus=='Fail'}">
																										 			<span class="badge badge-secondary badge-pill">Failed</span>
																										 		</c:if>
																											</td>
																										</tr>
																									<tr>
																										<td>&nbsp;</td>
																									</tr>
																								</c:if>
																							</c:forEach>
																							
																							<c:forEach items="${auditLog.dashboardCommandsList}" var="dashboardCommands" >
																									<c:if test = "${not empty dashboardCommands}">
																										<tr class="googlefontfortd">
																											<td width="900px">Dashboard Module Request Received time : ${dashboardCommands.requestTime}</td>
																											<td>
																												<c:if test="${dashboardCommands.commandStatus=='Success'}">
																										 			<span class="badge badge-success badge-pill">Success</span>
																										 		</c:if>
																										 		<c:if test="${dashboardCommands.commandStatus=='Fail'}">
																										 			<span class="badge badge-secondary badge-pill">Failed</span>
																										 		</c:if>
																											</td>
																										</tr>
																									<tr>
																										<td>&nbsp;</td>
																									</tr>
																								</c:if>
																							</c:forEach>
																							
			                                											</c:forEach>
			                                										</c:if>
																				</table>
																			</div>
																			<!-- view-tab close -->
																			
																			<!--###  -->
																			
																			
																			
																			
																			<!-- edit-tab open-->
																			<%-- 
																			<div class="tab-pane fade p-0" id="edit" role="tabpanel" aria-labelledby="edit-tab">
																				<table>
																					<c:if test = "${not empty auditLogList}">
			                                											<c:forEach var="auditLog" items="${auditLogList}">
																								
																								<c:forEach items="${auditLog.editCommandsList}" var="commands" >
																									<c:if test = "${not empty commands}">
																									
																										<tr>
																											<td width="900px"> Request Received time : ${commands.requestTime}</td>
																											<td>
																												<c:if test="${commands.commandStatus=='Success'}">
																										 			<span class="badge badge-success badge-pill">Success</span>
																										 		</c:if>
																										 		<c:if test="${commands.commandStatus=='Fail'}">
																										 			<span class="badge badge-secondary badge-pill">Failed</span>
																										 		</c:if>
																											</td>
																										</tr>
																								
																										<tr>
																											<td>Check List
																												<c:if test = "${not empty commands.mkdirSendConfig}">
																													<ol>mkdir /opt/resonous/sendConfig &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.mkdirSendConfig == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.mkdirSendConfig == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty commands.cpConfig}">
																													<ol>cp /opt/resonous/config/" + configFileName + " /opt/resonous/" + folderName + "/config.xml &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.cpConfig == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.cpConfig == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty commands.cpStartup}">
																													<ol>cp /opt/resonous/config/" + startupFileName + " /opt/resonous/" + folderName + "/startup.cfg &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.cpStartup == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.cpStartup == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty commands.chmodDb}">
																													<ol>chmod -R 777 /opt/resonous/sqlite/db &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.chmodDb == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.chmodDb == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty commands.chmodSendConfig}">
																													<ol>chmod -R 777 /opt/resonous/" + folderName + " " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.chmodSendConfig == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.chmodSendConfig == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty commands.chmodConfig}">
																													<ol>chmod -R 777 /opt/resonous/config &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.chmodConfig == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.chmodConfig == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty commands.cpHariff}">
																													<ol>cp /opt/resonous/config/" + ClientFolderName + " /opt/resonous/" + folderName + "/hariff.xml &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.cpHariff == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.cpHariff == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty commands.mountMntMnc}">
																													<ol>mount /dev/mmcblk0p1 /mnt/mmc/ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.mountMntMnc == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.mountMntMnc == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
												
																												<c:if test = "${not empty commands.mkdirDb}">
																													<ol>mkdir /mnt/mmc/db/ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.mkdirDb == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.mkdirDb == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																																												
																												<c:if test = "${not empty commands.umountMntMnc}">
																													<ol>umount /mnt/mmc/ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.umountMntMnc == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.umountMntMnc == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty commands.sync}">
																													<ol>sync &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.sync == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.sync == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty commands.fsetenvBw}">
																													<ol>fsetenv bw &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.fsetenvBw == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.fsetenvBw == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty commands.fsetenvIpaddr}">
																													<ol>fsetenv ipaddr &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.fsetenvIpaddr == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.fsetenvIpaddr == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty commands.fsetenvGtpip}">
																													<ol>fsetenv gtpip &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.fsetenvGtpip == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.fsetenvGtpip == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty commands.fsetenvNetmask}">
																													<ol>fsetenv netmask &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.fsetenvNetmask == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.fsetenvNetmask == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty commands.fsetenvGatewayip}">
																													<ol>fsetenv gatewayip &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.fsetenvGatewayip == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.fsetenvGatewayip == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty commands.fsetenvStartepc}">
																													<ol>fsetenv startepc &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.fsetenvStartepc == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.fsetenvStartepc == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty commands.fileRfconfig}">
																													<ol>/opt/resonous/config/RFCONFIG.cfg &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.fileRfconfig == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.fileRfconfig == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																											</td>
																										</tr>
																										
																										<tr>
																											<td width="900px">
																												<c:if test="${commands.commandStatus=='Success'}">
																									 				Response time :  ${commands.responseTime} Status: Edit Module Updated Successfully
																									 			</c:if>
																									 			<c:if test="${commands.commandStatus=='Fail'}">
																									 				Response time :  ${commands.responseTime} Status: Edit Module Updated Failed
																									 			</c:if> 
																											</td>
																										</tr>
																										<tr>
																											<td>&nbsp;</td>
																										</tr>
																									</c:if>
																								</c:forEach>
																						</c:forEach>
			                                										</c:if>
																				</table>
																				
																			</div>
																			 --%>
																			<!-- edit-tab open-->
																			<!--$$$$  -->
																			<div class="tab-pane fade p-0" id="bringup" role="tabpanel" aria-labelledby="bringup-tab">
																			<table id="data-table" class="table table-striped table-bordered"> 
																			<thead class="thead-dark">
			                                    										<tr class="gfont">
			                                    									     <th>Request & Response Time</th>
		                                        								         <th>Excecuted Commands </th>
		                                        								         <th>Status</th>
			                                    										</tr>
			                                								</thead>
			                                									<tbody>
			                                									<c:if test = "${not empty auditLogList}">
			                                											<c:forEach var="auditLog" items="${auditLogList}">
																								
																								<%-- <c:forEach items="${auditLog.bringupupgradeval}" var="values" >
																									<c:if test = "${not empty values}">
																									values:${values.cpConfig}
																								</c:if>
																									</c:forEach> --%>
																								
																								<c:forEach items="${auditLog.bringupUpgradeCommandsList}" var="bringupCommands" >
																									<c:if test = "${not empty bringupCommands}">
																									
																										<tr >
																										
																											<td class="googlefontfortd"> Request Received time : ${bringupCommands.requestTime}
																											<div>
																											<c:if test="${bringupCommands.commandStatus=='Success'}">
																									 				Response time :  ${bringupCommands.responseTime} Status: BringUp & Update Module Updated Successfully
																									 			</c:if>
																									 			<c:if test="${bringupCommands.commandStatus=='Fail'}">
																									 				Response time :  ${bringupCommands.responseTime} Status: BringUp & Update Module Updated Failed
																									 			</c:if> 
																											</div>
																											</td>
																											
																											<td class="googlefontfortd">
																											<div>
																												<c:if test = "${not empty bringupCommands.chmodUpgradeFile}">
																												<span>chmod 777 /opt/resonous/Upgrade_Flash.tar.gz &nbsp;</span>
																														 <c:if test="${bringupCommands.chmodUpgradeFile == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.chmodUpgradeFile == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if> 
																														<%-- <c:choose>
																												<c:when test="${bringupCommands.chmodUpgradeFile eq 'chmod 777 /opt/resonous/Upgrade_Flash.tar.gz'}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																														
																												</c:if>
																												</div><br>
																												<div>
																												
																												<c:if test = "${not empty bringupCommands.cpConfigvalue}">
																												<span>${bringupCommands.cpConfigvalue} &nbsp;</span>
																														<c:if test="${bringupCommands.cpConfig == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.cpConfig == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if> 
																														<%-- <c:choose>
																												<c:when test="${bringupCommands.cpConfig eq bringupCommands.cpConfig}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																											
																												</div><br>
																												<div>
																												<c:if test = "${not empty bringupCommands.cpStartupvalue}">
																												<span>${bringupCommands.cpStartupvalue} &nbsp;</span>
																														 <c:if test="${bringupCommands.cpStartup == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.cpStartup == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																														<%-- <c:choose>
																												<c:when test="${bringupCommands.cpStartup eq bringupCommands.cpStartup}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div><br>
																												<div>
																												<c:if test = "${not empty bringupCommands.cpHariffvalue}">
																												<span>${bringupCommands.cpHariffvalue} &nbsp;</span>
																														 <c:if test="${bringupCommands.cpHariff == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.cpHariff == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if> 
																														<%-- <c:choose>
																												<c:when test="${bringupCommands.cpHariff eq bringupCommands.cpHariff}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div><br>
																												<div>
																												<c:if test = "${not empty bringupCommands.chmodDb}">
																												<span>chmod -R 777 /opt/resonous/sqlite/db &nbsp;</span>
																														 <c:if test="${bringupCommands.chmodDb == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.chmodDb == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if> 
																														<%-- <c:choose>
																												<c:when test="${bringupCommands.chmodDb eq 'chmod -R 777 /opt/resonous/sqlite/db'}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div><br>
																												<div>
																												<c:if test = "${not empty bringupCommands.chmodUpgradeFolder}">
																												<span>chmod -R 777 /opt/resonous/Upgrade_Flash/ &nbsp;</span>
																													<c:if test="${bringupCommands.chmodUpgradeFolder == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.chmodUpgradeFolder == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																														<%-- <c:choose>
																												<c:when test="${bringupCommands.chmodUpgradeFolder eq 'chmod -R 777 /opt/resonous/Upgrade_Flash/'}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div><br>
																												<div>
																												<span>chmod -R 777 /opt/resonous/config &nbsp;</span>
																												<c:if test = "${not empty bringupCommands.chmodConfig}">
																														<c:if test="${bringupCommands.chmodConfig == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.chmodConfig == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																														<%-- <c:choose>
																												<c:when test="${bringupCommands.chmodConfig eq 'chmod -R 777 /opt/resonous/config'}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div><br>
																												<div>
																												<span>mv -f /opt/resonous/Upgrade_Flash/ /opt/resonous/Upgrade_Flash/epc.tar.gz &nbsp;</span>
																												<c:if test = "${not empty bringupCommands.mvUpgradeFile}">
																														 <c:if test="${bringupCommands.mvUpgradeFile == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.mvUpgradeFile == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if> 
																														<%-- <c:choose>
																												<c:when test="${bringupCommands.mvUpgradeFile eq 'mv -f /opt/resonous/Upgrade_Flash/ /opt/resonous/Upgrade_Flash/epc.tar.gz'}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div><br>
																												<div>
																												<span>mount /dev/mmcblk0p1 /mnt/mmc/ &nbsp;</span>
																												<c:if test = "${not empty bringupCommands.mountMntMnc}">
																														 <c:if test="${bringupCommands.mountMntMnc == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.mountMntMnc == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if> 
																														<%-- <c:choose>
																												<c:when test="${bringupCommands.mountMntMnc eq bringupCommands.mountMntMnc}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div><br>
																													<div>
																													<span>mkdir /mnt/mmc/db/&nbsp;</span>
																												<c:if test = "${not empty bringupCommands.mkdirDb}">
																														<c:if test="${bringupCommands.mkdirDb == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.mkdirDb == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																														<%-- <c:choose>
																												<c:when test="${bringupCommands.mkdirDb eq 'mkdir /mnt/mmc/db/'}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																													</div><br>	
																													<div>
																												<span>umount /mnt/mmc/  &nbsp;</span>													
																												<c:if test = "${not empty bringupCommands.umountMntMnc}">
																														 <c:if test="${bringupCommands.umountMntMnc == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.umountMntMnc == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if> 
																														<%-- <c:choose>
																												<c:when test="${bringupCommands.umountMntMnc eq bringupCommands.umountMntMnc}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div><br>
																												<div>
																												<span>sync &nbsp;</span>
																												<c:if test = "${not empty bringupCommands.sync}">
																														<c:if test="${bringupCommands.sync == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.sync == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if> 
																														<%-- <c:choose>
																												<c:when test="${bringupCommands.sync eq 'sync'}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div><br>
																												<div>
																												<span>${bringupCommands.fsetenvBwvalue } &nbsp;</span>
																												<c:if test = "${not empty bringupCommands.fsetenvBw}">
																														<c:if test="${bringupCommands.fsetenvBw == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.fsetenvBw == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if> 
																														<%-- <c:choose>
																												<c:when test="${bringupCommands.fsetenvBw eq bringupCommands.fsetenvBw}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div><br>
																												<div>
																												<span>${bringupCommands.fsetenvIpaddrvalue} &nbsp;</span>
																												<c:if test = "${not empty bringupCommands.fsetenvIpaddr}">
																														 <c:if test="${bringupCommands.fsetenvIpaddr == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.fsetenvIpaddr == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if> 
																														<%-- <c:choose>
																												<c:when test="${bringupCommands.fsetenvIpaddr eq bringupCommands.fsetenvIpaddr}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div><br>
																												<div>
																												<span>${bringupCommands.fsetenvGtpipvalue} &nbsp;</span>
																												<c:if test = "${not empty bringupCommands.fsetenvGtpip}">
																														 <c:if test="${bringupCommands.fsetenvGtpip == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.fsetenvGtpip == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																														<%-- <c:choose>
																												<c:when test="${bringupCommands.fsetenvGtpip eq bringupCommands.fsetenvGtpip}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div><br>
																												<div>
																												<span>${bringupCommands.fsetenvNetmaskvalue} &nbsp;</span>
																												<c:if test = "${not empty bringupCommands.fsetenvNetmask}">
																														<c:if test="${bringupCommands.fsetenvNetmask == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.fsetenvNetmask == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if> 
																														<%-- <c:choose>
																												<c:when test="${bringupCommands.fsetenvNetmask eq bringupCommands.fsetenvNetmask}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div><br>
																												<div>
																												<span>${bringupCommands.fsetenvGatewayipvalue} &nbsp;</span>
																												<c:if test = "${not empty bringupCommands.fsetenvGatewayip}">
																														 <c:if test="${bringupCommands.fsetenvGatewayip == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.fsetenvGatewayip == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																														<%-- <c:choose>
																												<c:when test="${bringupCommands.fsetenvGatewayip eq bringupCommands.fsetenvGatewayip}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div><br>
																												<div>
																												<span>${bringupCommands.fsetenvStartepcvalue} &nbsp;</span>
																												<c:if test = "${not empty bringupCommands.fsetenvStartepc}">
																														 <c:if test="${bringupCommands.fsetenvStartepc == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.fsetenvStartepc == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																														<%-- <c:choose>
																												<c:when test="${bringupCommands.fsetenvStartepc eq bringupCommands.fsetenvStartepc}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div><br>
																												<div>
																												<span>/opt/resonous/config/RFCONFIG.cfg &nbsp;</span>
																												<c:if test = "${not empty bringupCommands.fileRfconfig}">
																														 <c:if test="${bringupCommands.fileRfconfig == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.fileRfconfig == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																														<%-- <c:choose>
																												<c:when test="${bringupCommands.fileRfconfig eq '/opt/resonous/config/RFCONFIG.cfg'}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div><br>
																											</td>
																											<td>
																												<c:if test="${bringupCommands.commandStatus=='Success'}">
																											 		<span class="badge badge-success badge-pill">Success</span>
																											 	</c:if>
																											 	<c:if test="${bringupCommands.commandStatus=='Fail'}">
																											 		<span class="badge badge-secondary badge-pill">Failed</span>
																											 	</c:if>
																											</td>
																										</tr>
																									</c:if>
																								</c:forEach>
																								
																								
																						</c:forEach>
			                                										</c:if>
			                                									
																			</tbody>
																			</table>
																			</div>
																			
																			<!-- bringup-tab open -->
																			<%-- <div class="tab-pane fade p-0" id="bringup" role="tabpanel" aria-labelledby="bringup-tab">
																				<table>
																					<c:if test = "${not empty auditLogList}">
			                                											<c:forEach var="auditLog" items="${auditLogList}">
																								
																								<c:forEach items="${auditLog.bringupUpgradeCommandsList}" var="bringupCommands" >
																									<c:if test = "${not empty bringupCommands}">
																										<tr>
																											<td width="900px"> Request Received time : ${bringupCommands.requestTime}</td>
																											<td>
																												<c:if test="${bringupCommands.commandStatus=='Success'}">
																											 		<span class="badge badge-success badge-pill">Success</span>
																											 	</c:if>
																											 	<c:if test="${bringupCommands.commandStatus=='Fail'}">
																											 		<span class="badge badge-secondary badge-pill">Failed</span>
																											 	</c:if>
																											</td>
																										</tr>
																								
																										<tr>
																											<td>Check List
																												<c:if test = "${not empty bringupCommands.chmodUpgradeFile}">
																													<ol>chmod 777 /opt/resonous/" + fileName + "";  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${bringupCommands.chmodUpgradeFile == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.chmodUpgradeFile == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty bringupCommands.cpConfig}">
																													<ol>cp /opt/resonous/config/" + configFileName + " /opt/resonous/" + folderName + "/config.xml &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${bringupCommands.cpConfig == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.cpConfig == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty bringupCommands.cpStartup}">
																													<ol>cp /opt/resonous/config/" + startupFileName + " /opt/resonous/" + folderName + "/startup.cfg &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${bringupCommands.cpStartup == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.cpStartup == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty bringupCommands.cpHariff}">
																													<ol>cp /opt/resonous/config/" + ClientFolderName + " /opt/resonous/" + folderName + "/hariff.xml &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${bringupCommands.cpHariff == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.cpHariff == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty bringupCommands.chmodDb}">
																													<ol>chmod -R 777 /opt/resonous/sqlite/db &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${bringupCommands.chmodDb == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.chmodDb == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty bringupCommands.chmodUpgradeFolder}">
																													<ol>chmod -R 777 /opt/resonous/" + Upgrade_Flash + " " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${bringupCommands.chmodUpgradeFolder == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.chmodUpgradeFolder == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty bringupCommands.chmodConfig}">
																													<ol>chmod -R 777 /opt/resonous/config &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${bringupCommands.chmodConfig == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.chmodConfig == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty bringupCommands.mvUpgradeFile}">
																													<ol>mv -f /opt/resonous/" + Upgrade_Flash + epcName  + " /opt/resonous/" + Upgrade_Flash/ + "epc.tar.gz &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${bringupCommands.mvUpgradeFile == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.mvUpgradeFile == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty bringupCommands.mountMntMnc}">
																													<ol>mount /dev/mmcblk0p1 /mnt/mmc/ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${bringupCommands.mountMntMnc == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.mountMntMnc == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
												
																												<c:if test = "${not empty bringupCommands.mkdirDb}">
																													<ol>mkdir /mnt/mmc/db/ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${bringupCommands.mkdirDb == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.mkdirDb == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																																												
																												<c:if test = "${not empty bringupCommands.umountMntMnc}">
																													<ol>umount /mnt/mmc/ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${bringupCommands.umountMntMnc == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.umountMntMnc == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty bringupCommands.sync}">
																													<ol>sync &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${bringupCommands.sync == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.sync == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty bringupCommands.fsetenvBw}">
																													<ol>fsetenv bw &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${bringupCommands.fsetenvBw == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.fsetenvBw == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty bringupCommands.fsetenvIpaddr}">
																													<ol>fsetenv ipaddr &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${bringupCommands.fsetenvIpaddr == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.fsetenvIpaddr == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty bringupCommands.fsetenvGtpip}">
																													<ol>fsetenv gtpip &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${bringupCommands.fsetenvGtpip == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.fsetenvGtpip == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty bringupCommands.fsetenvNetmask}">
																													<ol>fsetenv netmask &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${bringupCommands.fsetenvNetmask == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.fsetenvNetmask == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty bringupCommands.fsetenvGatewayip}">
																													<ol>fsetenv gatewayip &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${bringupCommands.fsetenvGatewayip == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.fsetenvGatewayip == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty bringupCommands.fsetenvStartepc}">
																													<ol>fsetenv startepc &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${bringupCommands.fsetenvStartepc == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.fsetenvStartepc == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty bringupCommands.fileRfconfig}">
																													<ol>/opt/resonous/config/RFCONFIG.cfg &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${bringupCommands.fileRfconfig == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${bringupCommands.fileRfconfig == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																											</td>
																										</tr>
																										
																										<tr>
																											<td width="900px">
																												<c:if test="${bringupCommands.commandStatus=='Success'}">
																									 				Response time :  ${bringupCommands.responseTime} Status: BringUp & Update Module Updated Successfully
																									 			</c:if>
																									 			<c:if test="${bringupCommands.commandStatus=='Fail'}">
																									 				Response time :  ${bringupCommands.responseTime} Status: BringUp & Update Module Updated Failed
																									 			</c:if> 
																											</td>
																										</tr>
																										<tr>
																											<td>&nbsp;</td>
																										</tr>
																									</c:if>
																								</c:forEach>
																						</c:forEach>
			                                										</c:if>
																				</table>
																			</div> --%>
																			<!-- bringup-tab close -->
																			
																			
																			<!-- %%%% -->
																			
																			
																			<div class="tab-pane fade p-0" id="sendConfig" role="tabpanel" aria-labelledby="sendConfig-tab">
																			<table id="data-table" class="table table-striped table-bordered"> 
																			<thead class="thead-dark">
			                                    										<tr class="gfont">
			                                    									     <th>Request & Response Time</th>
		                                        								         <th>Excecuted Commands </th>
		                                        								         <th>Status</th>
			                                    										</tr>
			                                								</thead>
			                                									<tbody>
			                                									<c:if test = "${not empty auditLogList}">
			                                											<c:forEach var="auditLog" items="${auditLogList}">
																								
																								<c:forEach items="${auditLog.editCommandsList}" var="commands" >
																									<c:if test = "${not empty commands}">
																									
																										<tr>
																											<td class="googlefontfortd"> Request Received time : ${commands.requestTime}
																											<div>
																											<c:if test="${commands.commandStatus=='Success'}">
																									 				Response time :  ${commands.responseTime} Status: SendConfig Module Updated Successfully
																									 			</c:if>
																									 			<c:if test="${commands.commandStatus=='Fail'}">
																									 				Response time :  ${commands.responseTime} Status: SendConfig Module Updated Failed
																									 			</c:if> 
																											</div>
																											</td>
																											
																											<td class="googlefontfortd">
																											
																											<div>
																												<c:if test = "${not empty commands.mkdirSendConfig}">
																												<span>mkdir /opt/resonous/sendConfig</span>
																														<c:if test="${commands.mkdirSendConfig == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.mkdirSendConfig == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if> 
																														<%-- <c:choose>
																												<c:when test="${commands.mkdirSendConfig eq 'mkdir /opt/resonous/sendConfig' }">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div>
																												<br>
																												
																												<div>
																												<c:if test = "${not empty commands.cpConfigvalue}">
																												<span> ${commands.cpConfigvalue}&nbsp;</span>
																														<c:if test="${commands.cpConfig == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.cpConfig == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if> 
																														<%-- <c:choose>
																												<c:when test="${commands.cpConfig eq commands.cpConfig }">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div>
																												<br>
																												<div>
																												<c:if test = "${not empty commands.cpStartup}">
																												<span> ${commands.cpStartupvalue}&nbsp;</span>
																														 <c:if test="${commands.cpStartup == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.cpStartup == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if> 
																														<%-- <c:choose>
																												<c:when test="${commands.cpStartup eq commands.cpStartup }">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div><br>
																												<div>
																												<c:if test = "${not empty commands.chmodDb}">
																												<span>chmod -R 777 /opt/resonous/sqlite/db &nbsp;</span>
																														 <c:if test="${commands.chmodDb == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.chmodDb == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																														<%-- <c:choose>
																												<c:when test="${commands.chmodDb eq 'chmod -R 777 /opt/resonous/sqlite/db' }">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div><br>
																												
																												<div>
																												<c:if test = "${not empty commands.chmodSendConfig}">
																												<span>chmod -R 777 /opt/resonous/sendConfig &nbsp;</span>
																														 <c:if test="${commands.chmodSendConfig == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.chmodSendConfig == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if> 
																														<%-- <c:choose>
																												<c:when test="${commands.chmodSendConfig eq 'chmod -R 777 /opt/resonous/sendConfig'}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div><br>
																												
																												<div>
																												<c:if test = "${not empty commands.chmodConfig}">
																												<span>chmod -R 777 /opt/resonous/config &nbsp;</span>
																														 <c:if test="${commands.chmodConfig == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.chmodConfig == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if> 
																														<%-- <c:choose>
																												<c:when test="${commands.chmodConfig eq 'chmod -R 777 /opt/resonous/config'}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div><br>
																												<div>															
																												<c:if test = "${not empty commands.cpHariff}">
																												<span>${commands.cpHariffvalue} &nbsp;</span>
																														 <c:if test="${commands.cpHariff == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.cpHariff == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																														<%-- <c:choose>
																												<c:when test="${commands.cpHariff eq commands.cpHariff}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div>	<br>
																												
																												<div>
																												<c:if test = "${not empty commands.mountMntMnc}">
																												<span>mount /dev/mmcblk0p1 /mnt/mmc/ &nbsp;</span>
																														<c:if test="${commands.mountMntMnc == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.mountMntMnc == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if> 
																														<%-- <c:choose>
																												<c:when test="${commands.mountMntMnc eq commands.mountMntMnc}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div><br>
																														<div>
																												<c:if test = "${not empty commands.mkdirDb}">
																												<span>mkdir /mnt/mmc/db/ &nbsp;</span>
																														 <c:if test="${commands.mkdirDb == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.mkdirDb == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if> 
																														<%-- <c:choose>
																												<c:when test="${commands.mkdirDb eq 'mkdir /mnt/mmc/db/'}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																															</div><br>	
																															<div>											
																												<c:if test = "${not empty commands.umountMntMnc}">
																												<span>umount /mnt/mmc/ &nbsp;</span>
																														 <c:if test="${commands.umountMntMnc == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.umountMntMnc == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if> 
																														<%-- <c:choose>
																												<c:when test="${commands.umountMntMnc eq 'umount /mnt/mmc/'}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div>	<br>
																												<div>
																												<c:if test = "${not empty commands.sync}">
																												<span>sync &nbsp;</span>
																													<c:if test="${commands.sync == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.sync == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																														<%-- <c:choose>
																												<c:when test="${commands.sync eq 'sync'}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div><br>
																												<div>
																												<c:if test = "${not empty commands.fsetenvBw}">
																												<span>${commands.fsetenvBwvalue} &nbsp;</span>
																														<c:if test="${commands.fsetenvBw == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.fsetenvBw == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if> 
																														<%-- <c:choose>
																												<c:when test="${commands.fsetenvBw eq commands.fsetenvBw}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div><br>
																												
																												<div>
																												<c:if test = "${not empty commands.fsetenvIpaddr}">
																												<span>${commands.fsetenvIpaddrvalue} &nbsp;</span>
																														<c:if test="${commands.fsetenvIpaddr == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.fsetenvIpaddr == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																														<%-- <c:choose>
																												<c:when test="${commands.fsetenvIpaddr eq commands.fsetenvIpaddr}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div><br>
																												<div>
																												<c:if test = "${not empty commands.fsetenvGtpip}">
																												<span>${commands.fsetenvGtpipvalue} &nbsp;</span>
																														 <c:if test="${commands.fsetenvGtpip == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.fsetenvGtpip == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if> 
																														<%-- <c:choose>
																												<c:when test="${commands.fsetenvGtpip eq commands.fsetenvGtpip}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div><br>
																												<div>
																												<c:if test = "${not empty commands.fsetenvNetmask}">
																												<span>${commands.fsetenvNetmaskvalue} &nbsp;</span>
																														<c:if test="${commands.fsetenvNetmask == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.fsetenvNetmask == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if> 
																														<%-- <c:choose>
																												<c:when test="${commands.fsetenvNetmask eq commands.fsetenvNetmask}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div><br>
																												
																												<div>
																												<c:if test = "${not empty commands.fsetenvGatewayip}">
																												<span>${commands.fsetenvGatewayipvalue } &nbsp;</span>
																														 <c:if test="${commands.fsetenvGatewayip == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.fsetenvGatewayip == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if> 
																														<%-- <c:choose>
																												<c:when test="${commands.fsetenvGatewayip  eq commands.fsetenvGatewayip }">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div><br>
																												<div>
																												<c:if test = "${not empty commands.fsetenvStartepc}">
																												<span>${commands.fsetenvStartepcvalue} &nbsp;</span>
																														<c:if test="${commands.fsetenvStartepc == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.fsetenvStartepc == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if> 
																														<%-- <c:choose>
																												<c:when test="${commands.fsetenvStartepc  eq commands.fsetenvStartepc }">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div><br>
																												<div>
																												<c:if test = "${not empty commands.fileRfconfig}">
																												<span>/opt/resonous/config/RFCONFIG.cfg &nbsp;</span>
																														 <c:if test="${commands.fileRfconfig == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.fileRfconfig == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																														<%-- <c:choose>
																												<c:when test="${commands.fileRfconfig  eq '/opt/resonous/config/RFCONFIG.cfg' }">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div><br>
																											</td>
																											
																											<td>
																												<c:if test="${commands.commandStatus=='Success'}">
																										 			<span class="badge badge-success badge-pill">Success</span>
																										 		</c:if>
																										 		<c:if test="${commands.commandStatus=='Fail'}">
																										 			<span class="badge badge-secondary badge-pill">Failed</span>
																										 		</c:if>
																											</td>
																											
																										</tr>
																								
																										<tr>
																											
																										</tr>
																									</c:if>
																								</c:forEach>
																						</c:forEach>
			                                										</c:if>
			                                									
			                                									</tbody>
																			</table>
																			</div>
																			
																			
																			<!-- sendConfig-tab open -->
																			<%-- <div class="tab-pane fade p-0" id="sendConfig" role="tabpanel" aria-labelledby="sendConfig-tab">
																				<table>
																					<c:if test = "${not empty auditLogList}">
			                                											<c:forEach var="auditLog" items="${auditLogList}">
																								
																								<c:forEach items="${auditLog.editCommandsList}" var="commands" >
																									<c:if test = "${not empty commands}">
																									
																										<tr>
																											<td width="900px"> Request Received time : ${commands.requestTime}</td>
																											<td>
																												<c:if test="${commands.commandStatus=='Success'}">
																										 			<span class="badge badge-success badge-pill">Success</span>
																										 		</c:if>
																										 		<c:if test="${commands.commandStatus=='Fail'}">
																										 			<span class="badge badge-secondary badge-pill">Failed</span>
																										 		</c:if>
																											</td>
																										</tr>
																								
																										<tr>
																											<td>Check List
																												<c:if test = "${not empty commands.mkdirSendConfig}">
																													<ol>mkdir /opt/resonous/sendConfig &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.mkdirSendConfig == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.mkdirSendConfig == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty commands.cpConfig}">
																													<ol>cp /opt/resonous/config/" + configFileName + " /opt/resonous/" + folderName + "/config.xml &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.cpConfig == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.cpConfig == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty commands.cpStartup}">
																													<ol>cp /opt/resonous/config/" + startupFileName + " /opt/resonous/" + folderName + "/startup.cfg &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.cpStartup == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.cpStartup == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty commands.chmodDb}">
																													<ol>chmod -R 777 /opt/resonous/sqlite/db &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.chmodDb == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.chmodDb == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty commands.chmodSendConfig}">
																													<ol>chmod -R 777 /opt/resonous/" + folderName + " " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.chmodSendConfig == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.chmodSendConfig == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty commands.chmodConfig}">
																													<ol>chmod -R 777 /opt/resonous/config &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.chmodConfig == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.chmodConfig == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty commands.cpHariff}">
																													<ol>cp /opt/resonous/config/" + ClientFolderName + " /opt/resonous/" + folderName + "/hariff.xml &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.cpHariff == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.cpHariff == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty commands.mountMntMnc}">
																													<ol>mount /dev/mmcblk0p1 /mnt/mmc/ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.mountMntMnc == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.mountMntMnc == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
												
																												<c:if test = "${not empty commands.mkdirDb}">
																													<ol>mkdir /mnt/mmc/db/ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.mkdirDb == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.mkdirDb == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																																												
																												<c:if test = "${not empty commands.umountMntMnc}">
																													<ol>umount /mnt/mmc/ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.umountMntMnc == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.umountMntMnc == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty commands.sync}">
																													<ol>sync &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.sync == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.sync == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty commands.fsetenvBw}">
																													<ol>fsetenv bw &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.fsetenvBw == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.fsetenvBw == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty commands.fsetenvIpaddr}">
																													<ol>fsetenv ipaddr &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.fsetenvIpaddr == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.fsetenvIpaddr == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty commands.fsetenvGtpip}">
																													<ol>fsetenv gtpip &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.fsetenvGtpip == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.fsetenvGtpip == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty commands.fsetenvNetmask}">
																													<ol>fsetenv netmask &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.fsetenvNetmask == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.fsetenvNetmask == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty commands.fsetenvGatewayip}">
																													<ol>fsetenv gatewayip &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.fsetenvGatewayip == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.fsetenvGatewayip == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty commands.fsetenvStartepc}">
																													<ol>fsetenv startepc &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.fsetenvStartepc == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.fsetenvStartepc == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty commands.fileRfconfig}">
																													<ol>/opt/resonous/config/RFCONFIG.cfg &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${commands.fileRfconfig == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${commands.fileRfconfig == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																											</td>
																										</tr>
																										
																										<tr>
																											<td width="900px">
																												<c:if test="${commands.commandStatus=='Success'}">
																									 				Response time :  ${commands.responseTime} Status: SendConfig Module Updated Successfully
																									 			</c:if>
																									 			<c:if test="${commands.commandStatus=='Fail'}">
																									 				Response time :  ${commands.responseTime} Status: SendConfig Module Updated Failed
																									 			</c:if> 
																											</td>
																										</tr>
																										<tr>
																											<td>&nbsp;</td>
																										</tr>
																									</c:if>
																								</c:forEach>
																						</c:forEach>
			                                										</c:if>
																				</table>
																			</div> --%>
																			<!-- sendConfig-tab close -->
																			
																			<!-- ### -->
																			<div class="tab-pane fade p-0" id="reboot" role="tabpanel" aria-labelledby="reboot-tab">
																			<table id="data-table" class="table table-striped table-bordered"> 
																			<thead class="thead-dark">
			                                    										<tr class="gfont">
			                                    									     <th>Request & Response Time</th>
		                                        								         <th>Excecuted Commands </th>
		                                        								         <th>Status</th>
			                                    										</tr>
			                                								</thead>
																			<tbody>
																			<c:if test = "${not empty auditLogList}">
																			<c:forEach var="auditLog" items="${auditLogList}">
																			<c:forEach items="${auditLog.rebootCommandsList}" var="rebootCommands" >
																									<c:if test = "${not empty rebootCommands}">
																										
																										<tr>
																											<td class="googlefontfortd"> Request Received time : ${rebootCommands.requestTime}
																											<div>
																											<c:if test="${rebootCommands.commandStatus=='Success'}">
																									 				Response time :  ${rebootCommands.responseTime} Status: Reboot Module Updated Successfully
																									 			</c:if>
																									 			<c:if test="${rebootCommands.commandStatus=='Fail'}">
																									 				Response time :  ${rebootCommands.responseTime} Status: Reboot Module Updated Failed
																									 			</c:if> 
																											</div>
																											</td> 
																											
																											<td class="googlefontfortd">
																												<c:if test = "${not empty rebootCommands.rebootCommand}">
																														<span>reboot &nbsp;</span>
																														<c:if test="${rebootCommands.rebootCommand == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${rebootCommands.rebootCommand == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																												</c:if>
																											</td>
																											<td>
																												<c:if test="${rebootCommands.commandStatus=='Success'}">
																											 		<span class="badge badge-success badge-pill">Success</span>
																											 	</c:if>
																											 	<c:if test="${rebootCommands.commandStatus=='Fail'}">
																											 		<span class="badge badge-secondary badge-pill">Failed</span>
																											 	</c:if>
																											</td>
																										</tr>
																										<tr>
																											
																										
																									</c:if>
																								</c:forEach>
																			</c:forEach>
																			</c:if>
																			</tbody>
																			</table>
																			</div>
																			
																			
																			<!-- reboot-tab open -->
																			 <%-- <div class="tab-pane fade p-0" id="reboot" role="tabpanel" aria-labelledby="reboot-tab">
																				<table id="data-table" class="table table-striped table-bordered"> 
																					<c:if test = "${not empty auditLogList}">
			                                											<c:forEach var="auditLog" items="${auditLogList}">
																								
																								<c:forEach items="${auditLog.rebootCommandsList}" var="rebootCommands" >
																									<c:if test = "${not empty rebootCommands}">
																										
																										<tr>
																											<td width="900px"> Request Received time : ${rebootCommands.requestTime}</td>
																											<td>
																												<c:if test="${rebootCommands.commandStatus=='Success'}">
																											 		<span class="badge badge-success badge-pill">Success</span>
																											 	</c:if>
																											 	<c:if test="${rebootCommands.commandStatus=='Fail'}">
																											 		<span class="badge badge-secondary badge-pill">Failed</span>
																											 	</c:if>
																											</td>
																										</tr>
																								
																										<tr>
																											<td>Check List
																												<c:if test = "${not empty rebootCommands.rebootCommand}">
																													<ol>reboot &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${rebootCommands.rebootCommand == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${rebootCommands.rebootCommand == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																											</td>
																										</tr>
																										
																										<tr>
																											<td width="900px">
																												<c:if test="${rebootCommands.commandStatus=='Success'}">
																									 				Response time :  ${rebootCommands.responseTime} Status: Reboot Module Updated Successfully
																									 			</c:if>
																									 			<c:if test="${rebootCommands.commandStatus=='Fail'}">
																									 				Response time :  ${rebootCommands.responseTime} Status: Reboot Module Updated Failed
																									 			</c:if> 
																											</td>
																										</tr>
																										<tr>
																											<td>&nbsp;</td>
																										</tr>
																									</c:if>
																								</c:forEach>
																						</c:forEach>
			                                										</c:if>
																				</table>
																			</div>  --%>
																			<!-- reboot-tab close -->
																			
																			
																			<!--####  -->
																			 <div class="tab-pane fade p-0" id="dashboard" role="tabpanel" aria-labelledby="dashboard-tab">
																			<table id="data-table" class="table table-striped table-bordered"> 
																			<thead class="thead-dark">
			                                    										<tr class="gfont">
			                                    										 
			                                    									     <th>Request & Response Time</th>
			                                    									      <th>Excecuted Commands </th>
			                                    									       <th>Status</th>
			                                    										</tr>
			                                								</thead>
																			<tbody>
																			<c:if test = "${not empty auditLogList}">
			                                											<c:forEach var="auditLog" items="${auditLogList}">
																							<tr>
																							           
																									 		
																								<c:forEach items="${auditLog.dashboardCommandsList}" var="dashboardCommands" >
																									<c:if test = "${not empty dashboardCommands}">
																									
																										<%--  <td class="googlefontfortd">
																							<c:forEach items="${auditLog.nmsvalues}" var="dd" >
																									            <c:if test = "${not empty dd}">
																									            	<div>
																												<c:if test = "${not empty dd.cli}">
																												<span>${dd.cli}</span>
																												</c:if>
																												</div>
																												
																									            </c:if>
																									            </c:forEach>  
																									</td>  --%>															
																											<td class="googlefontfortd"> Request Received time : ${dashboardCommands.requestTime}
																											<div>
																												<c:if test="${dashboardCommands.commandStatus=='Success'}">
																									 				Response time :  ${dashboardCommands.responseTime} Status: Dashboard Module Updated Successfully
																									 			</c:if>
																									 			<c:if test="${dashboardCommands.commandStatus=='Fail'}">
																									 				Response time :  ${dashboardCommands.responseTime} Status: Dashboard Module Updated Failed
																									 			</c:if> 
																									 			</div>
																											</td>
																											
																											
																											<td class="googlefontfortd">
																												
																											<div>
																											
																												
																											
																												<c:if test = "${not empty dashboardCommands.ssh}">
																														<c:if test="${dashboardCommands.ssh == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${dashboardCommands.ssh == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																												</c:if>
																												
																												</div>
																												<div>
																												<c:if test = "${not empty dashboardCommands.cli}">
																												<span >cli</span>
																												
																												<%-- <c:choose>
																												<c:when test="${dashboardCommands.cli == 'cli'}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												
																														<c:if test="${dashboardCommands.cli == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${dashboardCommands.cli == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if> 
																												</c:if>
																												</div>
																												<div>
																												<c:if test = "${not empty dashboardCommands.dfePowerTemperatureGetAll}">
																													<span>dfe_power_temperature_get all &nbsp;</span>
																													
																													<%-- <c:choose>
																												<c:when test="${dashboardCommands.dfePowerTemperatureGetAll == 'dfe_power_temperature_get all'}">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																														 <c:if test="${dashboardCommands.dfePowerTemperatureGetAll == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${dashboardCommands.dfePowerTemperatureGetAll == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if> 
																												</c:if>
																												</div>
																												<div>
																												<c:if test = "${not empty dashboardCommands.dfeGrequencyGet}">
																												<span>dfe_frequency_get &nbsp;</span>
																														<c:if test="${dashboardCommands.dfeGrequencyGet == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${dashboardCommands.dfeGrequencyGet == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if> 
																														<%-- <c:choose>
																												<c:when test="${dashboardCommands.dfeGrequencyGet == 'dfe_frequency_get' }">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																														
																												</c:if>
																												</div>
																												<div>
																												<c:if test = "${not empty dashboardCommands.exit}">
																												<span>exit &nbsp;
																												</span>
																														 <c:if test="${dashboardCommands.exit == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${dashboardCommands.exit == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if> 
																														
																														<%-- <c:choose>
																												<c:when test="${dashboardCommands.exit =='exit' }">
																												<input type="checkbox" id="dcexit" name="vehicle1" value="np" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="dcexit" name="vehicle1" value="p" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																												</c:if>
																												</div>
																												
																												<div>
																												
																												<c:if test = "${not empty dashboardCommands.mmeEnbInfoIp}">
																												<span>${dashboardCommands.mmeEnbInfoIpvalue} &nbsp; </span>
																														 <c:if test="${dashboardCommands.mmeEnbInfoIp == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${dashboardCommands.mmeEnbInfoIp == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if> 
																														<%-- <c:choose>
																												<c:when test="${dashboardCommands.mmeEnbInfoIp eq dashboardCommands.mmeEnbInfoIp }">
																												<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																												</c:when>
																												  <c:otherwise>
																												  <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																												  </c:otherwise>
																												</c:choose> --%>
																														
																												</c:if>
																												</div>
																												
																											</td>
																											<td>
																											 <c:if test="${dashboardCommands.commandStatus=='Success'}">
																											 		<span class="badge badge-success badge-pill">Success</span>
																											 	</c:if>
																											 	<c:if test="${dashboardCommands.commandStatus=='Fail'}">
																											 		<span class="badge badge-secondary badge-pill">Failed</span>
																											 	</c:if> 
																											 	
																											</td>
																										
																										
																										
																									</c:if>
																									</c:forEach>
																									</tr>
																								</c:forEach>
			                                										</c:if>
																			
																			</tbody>
																			</table>
																			</div> 
																			
																			
																			<!-- dashboard-tab open -->
																		 <%-- <div class="tab-pane fade p-0" id="dashboard" role="tabpanel" aria-labelledby="dashboard-tab">
																				<table>
																					<c:if test = "${not empty auditLogList}">
			                                											<c:forEach var="auditLog" items="${auditLogList}">
																								
																								<c:forEach items="${auditLog.dashboardCommandsList}" var="dashboardCommands" >
																									<c:if test = "${not empty dashboardCommands}">
																										
																										<tr>
																											<td width="900px"> Request Received time : ${dashboardCommands.requestTime}</td>
																											<td>
																												<c:if test="${dashboardCommands.commandStatus=='Success'}">
																											 		<span class="badge badge-success badge-pill">Success</span>
																											 	</c:if>
																											 	<c:if test="${dashboardCommands.commandStatus=='Fail'}">
																											 		<span class="badge badge-secondary badge-pill">Failed</span>
																											 	</c:if>
																											</td>
																										</tr>
																								
																										<tr>
																											<td>Check List
																												<c:if test = "${not empty dashboardCommands.ssh}">
																													<ol>Ssh &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${dashboardCommands.ssh == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${dashboardCommands.ssh == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty dashboardCommands.cli}">
																													<ol>cli &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${dashboardCommands.cli == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${dashboardCommands.cli == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty dashboardCommands.dfePowerTemperatureGetAll}">
																													<ol>dfe_power_temperature_get all &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${dashboardCommands.dfePowerTemperatureGetAll == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${dashboardCommands.dfePowerTemperatureGetAll == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty dashboardCommands.dfeGrequencyGet}">
																													<ol>dfe_frequency_get &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${dashboardCommands.dfeGrequencyGet == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${dashboardCommands.dfeGrequencyGet == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty dashboardCommands.exit}">
																													<ol>exit &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${dashboardCommands.exit == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${dashboardCommands.exit == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																												<c:if test = "${not empty dashboardCommands.mmeEnbInfoIp}">
																													<ol>mme_enb_info ip &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																														<c:if test="${dashboardCommands.mmeEnbInfoIp == true}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;>
																														</c:if>
																														<c:if test="${dashboardCommands.mmeEnbInfoIp == false}">
																															<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" disabled background-color:red;>
																														</c:if>
																													</ol>
																												</c:if>
																												
																											</td>
																										</tr>
																										
																										<tr>
																											<td width="900px">
																												<c:if test="${dashboardCommands.commandStatus=='Success'}">
																									 				Response time :  ${dashboardCommands.responseTime} Status: Dashboard Module Updated Successfully
																									 			</c:if>
																									 			<c:if test="${dashboardCommands.commandStatus=='Fail'}">
																									 				Response time :  ${dashboardCommands.responseTime} Status: Dashboard Module Updated Failed
																									 			</c:if> 
																											</td>
																										</tr>
																										<tr>
																											<td>&nbsp;</td>
																										</tr>
																									</c:if>
																								</c:forEach>
																						</c:forEach>
			                                										</c:if>
																				</table>
																			</div>  --%>
																			<!-- dashboard-tab close -->
																		</div>
																		
																	</div>
																</div>
															</div>
														</div><!--row closed-->
														
														
													</div><!--accordion-body-->
													
												</div>
											</div>
											<center>
												<a href="<%=request.getContextPath()%>/listOfUsers" class="btn btn-icon btn-info text-white mr-2">Back</a>
											</center>
										</div>
										
									</div>
								</div>
							</div>
						</div>
												
					</section>
				</div>
				<!--app-content closed-->

				<!-- Side-bar -->
				<div class="sidebar sidebar-right sidebar-animate">
					<div class="m-3">
						<a href="#" class="text-right float-right" data-toggle="sidebar-right" data-target=".sidebar-right"><i class="fe fe-x"></i></a>
					</div>
					<div class="rightmenu ">
						<h5>Latest News</h5>
						<div class="p-2">
							<div class="latest-timeline">
								<ul class="timeline mb-0">
									<li class="mt-0">
										<a target="_blank" href="#">Sed ut perspiciatis </a>
										<a href="#" class="float-right text-dark">23 Apr, 2019</a>
										<p class="text-muted">sed do eiusmod tempor incididunt ut labore et dolore. </p>
									</li>
									<li>
										<a href="#">Nam libero tempore</a>
										<a href="#" class="float-right text-dark">21 Apr, 2019</a>
										<p class="text-muted">sed do eiusmod tempor  magna aliqua nisi ut aliquip. </p>
									</li>
									<li>
										<a href="#">Excepteur  cupidatat</a>
										<a href="#" class="float-right text-dark">18 Apr, 2019</a>
										<p class="text-muted">sed do eiusmod tempor incididunt ut labore  ut aliquip.</p>
									</li>
									<li class="mb-0">
										<a href="#">Neque porro est</a>
										<a href="#" class="float-right text-dark">17 Apr, 2019</a>
										<p class="text-muted">sed do eiusmod tempor  aliqua nisi ut aliquip . </p>
									</li>
								</ul>
							</div>
							<div class="text-center">
								<a href="#" class="btn btn-primary btn-block mb-4">Adding</a>
							</div>
						</div>
						<h5>Visitors</h5>
						<div class="card-body p-0">
							<ul class="visitor list-unstyled list-unstyled-border">
								<li class="media p-2 mb-0">
									<img class="mr-3 rounded-circle" width="50" src="/Nms_Simulator/resources/assets/img/avatar/6.jpg" alt="avatar">
									<div class="media-body">
										<div class="float-right"><small>10-4-2018</small></div>
										<div class="media-title">Blake Vanessa</div>
										<small>sed do eiusmod tempor incididunt ut labore</small>
									</div>
								</li>
								<li class="media p-2 mb-0">
									<img class="mr-3 rounded-circle" width="50" src="/Nms_Simulator/resources/assets/img/avatar/2.jpg" alt="avatar">
									<div class="media-body">
										<div class="float-right"><small>15-4-2018</small></div>
										<div class="media-title">Keith Rutherford</div>
										<small>sed do eiusmod tempor incididunt ut labore</small>
									</div>
								</li>
								<li class="media p-2 mb-0">
									<img class="mr-3 rounded-circle" width="50" src="/Nms_Simulator/resources/assets/img/avatar/3.jpg" alt="avatar">
									<div class="media-body">
										<div class="float-right"><small>17-4-2018</small></div>
										<div class="media-title">Elizabeth Parsons</div>
										<small>sed do eiusmod tempor incididunt ut labore</small>
									</div>
								</li>
								<li class="media border-b0 p-2 mb-0">
									<img class="mr-3 rounded-circle" width="50" src="/Nms_Simulator/resources/assets/img/avatar/4.jpg" alt="avatar">
									<div class="media-body">
										<div class="float-right"><small>22-3-2018</small></div>
										<div class="media-title">Nicola Lambert</div>
										<small>sed do eiusmod tempor incididunt ut labore</small>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!--side-bar Closed -->

				<!--Footer-->
				<footer class="main-footer hor-footer">
					<div class="text-center">
						Copyright &copy; NMS 2021. 
					</div>
				</footer>
				<!--/Footer-->

			</div>
		</div>
		<!--app closed-->

		<!-- Back to top -->
		<a href="#top" id="back-to-top" ><i class="fa fa-long-arrow-up"></i></a>

		<!-- ================== BEGIN BASE JS LEVEL ================== -->
		<jsp:include page="../simulator/commonJs.jsp" />
		<!-- ================== END PAGE JS LEVEL ================== -->

	
<%--   <c:forEach items="${auditLog.nmsvalues}" var="dd">
	<c:if test="${not empty dd}">
		<div>
			<c:if test="${not empty dd.cli}">
				<c:set var="selectedQuery" value="${dd.cli}" />
			</c:if>
		</div>

	</c:if>
</c:forEach>  --%>



	</body>
</html>
