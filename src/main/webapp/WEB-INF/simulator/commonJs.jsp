<!--Jquery.min js-->
		<script src="/Nms_Simulator/resources/assets/js/jquery.min.js"></script>

		<!--popper js-->
		<script src="/Nms_Simulator/resources/assets/js/popper.js"></script>

		<!--Bootstrap.min js-->
		<script src="/Nms_Simulator/resources/assets/plugins/bootstrap/js/bootstrap.min.js"></script>

		<!--Tooltip js-->
		<script src="/Nms_Simulator/resources/assets/js/tooltip.js"></script>

		<!-- Jquery star rating-->
		<script src="/Nms_Simulator/resources/assets/plugins/rating/jquery.rating-stars.js"></script>

		<!--Horizontalmenu js-->
		<script src="/Nms_Simulator/resources/assets/plugins/horizontal-menu/horizontal-menu.js"></script>

		<!-- Sidebar js-->
		<script src="/Nms_Simulator/resources/assets/plugins/sidebar/sidebar.js"></script>

		<!--P-Scrollbar js-->
		<script src="/Nms_Simulator/resources/assets/plugins/p-scroll/p-scroll.js"></script>


		<!--Othercharts js-->
		<script src="/Nms_Simulator/resources/assets/plugins/othercharts/jquery.knob.js"></script>
		<script src="/Nms_Simulator/resources/assets/plugins/othercharts/jquery.sparkline.min.js"></script>

		<!--OtherCharts js-->
		<script src="/Nms_Simulator/resources/assets/js/othercharts.js"></script>

		<!-- ECharts js -->
		<script src="/Nms_Simulator/resources/assets/plugins/echarts/echarts.js"></script>

		<!--Chart js-->
		<script src="/Nms_Simulator/resources/assets/plugins/Chart.js/dist/Chart.min.js"></script>
		<script src="/Nms_Simulator/resources/assets/plugins/Chart.js/dist/Chart.extension.js"></script>

		<!-- Vector Plugin -->
		<script src="/Nms_Simulator/resources/assets/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
        <script src="/Nms_Simulator/resources/assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <script src="/Nms_Simulator/resources/assets/plugins/jvectormap/gdp-data.js"></script>
        <script src="/Nms_Simulator/resources/assets/plugins/jvectormap/jquery-jvectormap-us-aea-en.js"></script>
        <script src="/Nms_Simulator/resources/assets/plugins/jvectormap/jquery-jvectormap-uk-mill-en.js"></script>
        <script src="/Nms_Simulator/resources/assets/plugins/jvectormap/jquery-jvectormap-au-mill.js"></script>
        <script src="/Nms_Simulator/resources/assets/plugins/jvectormap/jquery-jvectormap-ca-lcc.js"></script>
		<script src="/Nms_Simulator/resources/assets/js/jvectormap.js"></script>

		<!-- SWITCHER JS -->
		<script src="/Nms_Simulator/resources/assets/switcher/js/switcher.js"></script>

		<!-- STICKY JS -->
		<script src="/Nms_Simulator/resources/assets/js/stiky.js"></script>

		<!--Dashboard js-->
		<script src="/Nms_Simulator/resources/assets/js/index2.js"></script>

		<!--Showmore js-->
		<script src="/Nms_Simulator/resources/assets/js/jquery.showmore.js"></script>

		<!--Scripts js-->
		<script src="/Nms_Simulator/resources/assets/js/scripts1.js"></script>