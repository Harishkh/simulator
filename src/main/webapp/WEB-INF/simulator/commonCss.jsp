		<!--Bootstrap.min css-->
		<link rel="stylesheet" href="/Nms_Simulator/resources/assets/plugins/bootstrap/css/bootstrap.min.css">

		<!--Style css-->
		<link rel="stylesheet" href="/Nms_Simulator/resources/assets/css/style.css">
		<link href="/Nms_Simulator/resources/assets/css/dark-style.css" rel="stylesheet" />
		<link href="/Nms_Simulator/resources/assets/css/skin-mode.css" rel="stylesheet" />

		<!--Icons css-->
		<link rel="stylesheet" href="/Nms_Simulator/resources/assets/css/icons.css">

		<!-- P-Scrollbar css-->
		<link rel="stylesheet" href="/Nms_Simulator/resources/assets/plugins/p-scroll/p-scroll.css">

		<!-- Horizontal-menu Css -->
		<link id="effect" href="/Nms_Simulator/resources/assets/plugins/horizontal-menu/dropdown-effects/fade-up.css" rel="stylesheet" media="all">
		<link href="/Nms_Simulator/resources/assets/plugins/horizontal-menu/horizontal-menu.css" rel="stylesheet" media="all">

		<!-- Siderbar css-->
		<link href="/Nms_Simulator/resources/assets/plugins/sidebar/sidebar.css" rel="stylesheet">

		<!-- jvectormap CSS -->
        <link href="/Nms_Simulator/resources/assets/plugins/jvectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" />

		<!-- Color-skins css -->
		<link id="theme" rel="stylesheet" type="text/css" media="all" href="/Nms_Simulator/resources/assets/css/color-skins/color4.css" />

		<!-- Switcher css -->
		<link  href="/Nms_Simulator/resources/assets/switcher/css/switcher.css" rel="stylesheet" id="switcher-css" type="text/css" media="all"/>
		<link  href="/Nms_Simulator/resources/assets/switcher/demo.css" rel="stylesheet"/>