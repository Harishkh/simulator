<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Meta data -->
		<meta charset="UTF-8">
		<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
		<meta name="keywords" content="SMS Alert"/>

		<!-- Title -->
		<title>NMS Simulator | Dashboard</title>
		<!-- ================== BEGIN BASE CSS STYLE ================== -->
		<jsp:include page="../simulator/commonCss.jsp" />
		<!-- ================== END PAGE LEVEL STYLE ================== -->
	</head>

	<body class="app ">
		<!--loader -->
		<div id="spinner">
			<img src="/Nms_Simulator/resources/assets/img/svgs/loader.svg" alt="loader">
		</div>

		<!--app open-->
		<div id="app" class="page">
			<div class="main-wrapper" >
				<!--top-header open -->
				<div class="top-header" >
					<!--nav open-->
					<nav class="navbar navbar-expand-lg main-navbar" style="background-color: #5e10ac;">
						<div class="container">
							<a id="horizontal-navtoggle" class="animated-arrow hor-toggle" ><span></span></a>
							<h2 style="font-color: white;color: white;padding-left: 352px;"><b>NMS SIMULATOR</b></h2>
							<a class="header-brand" href="index.html"></a>
						</div>
					</nav>
					<!--nav closed-->
				</div>
				<!--top-header close -->
				
				
                <!--app-content open-->
				<div class="container app-content">
					<section class="section">
						<div class="page-header">
							<div>
								<h4 class="page_header_h4">
									<ol class="breadcrumb"><!-- breadcrumb -->
										
									</ol><!-- End breadcrumb -->
								</h4>							
							</div>
						</div> 
						<div class="section-body">
                            <!--row open-->
							<div class="row">
								<div class="col-12">
									<div class="card googlefont1">
										<div class="card-header">
											<h2 class="googlefont1">NMS Simulator</h2>
											
										</div>
										<!--card-body open-->
										<div class="card-body">
											<div id="accordion">
												<div class="accordion">
													<div class="accordion-header" data-toggle="" data-target="#panel-body-1" style="background-color: #1997c7;">
														<h4 style="font-color: white; color: white;">List of Users</h4>
													</div>
													<div class="accordion-body collapse show border border-top-0" id="panel-body-1" data-parent="#accordion" >
														 <table id="data-table" class="table table-striped table-bordered">
															<thead class="thead-dark">
			                                    				<tr>
			                                    					<th>Connected User</th>
			                                        				<th>IP Address</th>
			                                         				<th>Action</th>
			                                   					</tr>
			                                				</thead>
			                                				 <tbody>
			                                				 	<c:if test = "${not empty list}">
			                                						<c:forEach var="auditLog" items="${list}">
			                                							<tr>
			                                				 				<td>${auditLog.interfaceName}</td>
		                                				 					<td>${auditLog.ipAddress}</td>
			                                				 				<td>
			                                				 					<a href="<%=request.getContextPath()%>/viewPage?id=${auditLog.auditLogId}&ip=${auditLog.ipAddress}"><span class="badge badge-success badge-pill" >View</span></a>
			                                				 				</td>
			                                				 			</tr>
			                                						</c:forEach>
			                                					</c:if>
			                                				 </tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
										<!--card-body close-->
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
				<!--app-content closed-->

<!-- Side-bar -->
				<div class="sidebar sidebar-right sidebar-animate">
					<div class="m-3">
						<a href="#" class="text-right float-right" data-toggle="sidebar-right" data-target=".sidebar-right"><i class="fe fe-x"></i></a>
					</div>
					<div class="rightmenu ">
						<h5>Latest News</h5>
						<div class="p-2">
							<div class="latest-timeline">
								<ul class="timeline mb-0">
									<li class="mt-0">
										<a target="_blank" href="#">Sed ut perspiciatis </a>
										<a href="#" class="float-right text-dark">23 Apr, 2019</a>
										<p class="text-muted">sed do eiusmod tempor incididunt ut labore et dolore. </p>
									</li>
									<li>
										<a href="#">Nam libero tempore</a>
										<a href="#" class="float-right text-dark">21 Apr, 2019</a>
										<p class="text-muted">sed do eiusmod tempor  magna aliqua nisi ut aliquip. </p>
									</li>
									<li>
										<a href="#">Excepteur  cupidatat</a>
										<a href="#" class="float-right text-dark">18 Apr, 2019</a>
										<p class="text-muted">sed do eiusmod tempor incididunt ut labore  ut aliquip.</p>
									</li>
									<li class="mb-0">
										<a href="#">Neque porro est</a>
										<a href="#" class="float-right text-dark">17 Apr, 2019</a>
										<p class="text-muted">sed do eiusmod tempor  aliqua nisi ut aliquip . </p>
									</li>
								</ul>
							</div>
							<div class="text-center">
								<a href="#" class="btn btn-primary btn-block mb-4">Adding</a>
							</div>
						</div>
						<h5>Visitors</h5>
						<div class="card-body p-0">
							<ul class="visitor list-unstyled list-unstyled-border">
								<li class="media p-2 mb-0">
									<img class="mr-3 rounded-circle" width="50" src="/Nms_Simulator/resources/assets/img/avatar/6.jpg" alt="avatar">
									<div class="media-body">
										<div class="float-right"><small>10-4-2018</small></div>
										<div class="media-title">Blake Vanessa</div>
										<small>sed do eiusmod tempor incididunt ut labore</small>
									</div>
								</li>
								<li class="media p-2 mb-0">
									<img class="mr-3 rounded-circle" width="50" src="/Nms_Simulator/resources/assets/img/avatar/2.jpg" alt="avatar">
									<div class="media-body">
										<div class="float-right"><small>15-4-2018</small></div>
										<div class="media-title">Keith Rutherford</div>
										<small>sed do eiusmod tempor incididunt ut labore</small>
									</div>
								</li>
								<li class="media p-2 mb-0">
									<img class="mr-3 rounded-circle" width="50" src="/Nms_Simulator/resources/assets/img/avatar/3.jpg" alt="avatar">
									<div class="media-body">
										<div class="float-right"><small>17-4-2018</small></div>
										<div class="media-title">Elizabeth Parsons</div>
										<small>sed do eiusmod tempor incididunt ut labore</small>
									</div>
								</li>
								<li class="media border-b0 p-2 mb-0">
									<img class="mr-3 rounded-circle" width="50" src="/Nms_Simulator/resources/assets/img/avatar/4.jpg" alt="avatar">
									<div class="media-body">
										<div class="float-right"><small>22-3-2018</small></div>
										<div class="media-title">Nicola Lambert</div>
										<small>sed do eiusmod tempor incididunt ut labore</small>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!--side-bar Closed -->
				
				<!--Footer-->
				<footer class="main-footer hor-footer">
					<div class="text-center">
						Copyright &copy; NMS 2021. 
					</div>
				</footer>
				<!--/Footer-->

			</div>
		</div>
		<!--app closed-->

		<!-- Back to top -->
		<!-- <a href="#top" id="back-to-top" ><i class="fa fa-long-arrow-up"></i></a> -->

		<!-- ================== BEGIN BASE JS LEVEL ================== -->
		<jsp:include page="../simulator/commonJs.jsp" />
		<!-- ================== END PAGE JS LEVEL ================== -->

	<script>
	
		$(document).ready(function() {
			App.init();
		 	$("#data-table").DataTable();
		 	//TableManageDefault.init();
		)};
	</script>
	
	</body>
</html>
