
<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Meta data -->
		<meta charset="UTF-8">
		<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
		<meta name="keywords" content="SMS Alert"/>

		<!-- Title -->
		<title>NMS Simulator | Dashboard</title>
		<!-- ================== BEGIN BASE CSS STYLE ================== -->
		<jsp:include page="../simulator/commonCss.jsp" />
		<!-- ================== END PAGE LEVEL STYLE ================== -->
	</head>

	<body class="app ">
		<!--loader -->
		<!-- <div id="spinner">
			<img src="/Nms_Simulator/resources/assets/img/svgs/loader.svg" alt="loader">
		</div> -->

		<!--app open-->
		<div id="app" class="page">
			<div class="main-wrapper" >
				<div class="top-header" >
					<!--nav open-->
					<nav class="navbar navbar-expand-lg main-navbar" style="background-color: #5e10ac;">
						<div class="container">
							<a id="horizontal-navtoggle" class="animated-arrow hor-toggle" ><span></span></a>
							<h3 style="font-color: white;color: white;padding-left: 352px;"><b>NMS SIMULATOR</b></h3>
							<a class="header-brand" href="index.html"></a>
						</div>
					</nav>
					<!--nav closed-->
				</div>

                <!--app-content open-->
				<div class="container app-content">
					<section class="section">
						<div class="section-body">
                            <!--row open-->
							<div class="row">
								<div class="col-12">
									<div class="card">
										<div class="card-header">
											<h4>NMS Simulator</h4>
										</div>
										<div class="card-body">
											<div id="accordion">
												<div class="accordion">
													<div class="accordion-header" data-toggle="collapse" data-target="#panel-body-1" >
														<h4>192.168.12.1</h4>
													</div>
													<div class="accordion-body collapse show border border-top-0" id="panel-body-1" data-parent="#accordion" >
														<table>
														<tr>
															<td width="0px">Status : </td>
															<td width="350px">NMS &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="badge badge-success badge-pill">Active</span></td>
														</tr>
														<tr>
															<td>IP Address :</td>
															<td width="350px">192.168.12.1</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td><div class="row">
																	<div class="col-12">
																		<div class="card">
																			<div class="card-header">
																				<h4>User Activity</h4>
																			</div>
																			<div class="card-body">
																				<ul class="nav nav-tabs" id="myTab" role="tablist">
																					<li class="nav-item">
																						<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">View</a>
																					</li>
																					<li class="nav-item">
																						<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">BringUp&Upgrade</a>
									
																					</li>
									
																					<li class="nav-item">
									
																						<a class="nav-link" id="contact-tab" data-toggle="tab" href="#reboot" role="tab" aria-controls="contact" aria-selected="false">Reboot</a>
									
																					</li>
									<li class="nav-item">
									
																						<a class="nav-link" id="contact-tab" data-toggle="tab" href="#sendconfig" role="tab" aria-controls="contact" aria-selected="false">SendConfig</a>
																					</li>
									<li class="nav-item">
									
																						<a class="nav-link" id="contact-tab" data-toggle="tab" href="#dashboard" role="tab" aria-controls="contact" aria-selected="false">Dashboard</a>
																					</li>
									
																				</ul>

																			<div class="tab-content  p-3" id="myTabContent">
																				<div class="tab-pane fade show active p-0" id="home" role="tabpanel" aria-labelledby="home-tab">
																					<table>
																					<tr>
																					<td width="900px">
																					Request Recived time : 12/01/2021 14:30 
																					<td><span class="badge badge-success badge-pill">Success</span></td>
																					</tr>
																					<tr>
																					<td>&nbsp;</td>
																					</tr>
																					<tr>
																					<td width="900px">
																					Request Recived time : 12/01/2021 16:30
																					<td><span class="badge badge-secondary badge-pill">Failed</span></td>
																					</tr>
																					
																					</table>

																				</div>

																				<div class="tab-pane fade p-0" id="profile" role="tabpanel" aria-labelledby="profile-tab">
																					<table>
																						<tr>
																						<td width="900px">
																						Request Recived time : 12/01/2021 14:30 
																						<td><span class="badge badge-success badge-pill">Success</span></td>
																						</tr>
																						<tr>
																						<td>&nbsp;</td>
																						</tr>
																						<tr>
																						<td>Check List <ol>chmod 777 /opt/resonous/" + fileName + ""&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;></ol><ol>tar zxvf /opt/resonous/" + fileName + " -C /opt/resonous/&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;></ol></td>
																						
																						</tr>
																						<tr>
																						<td>&nbsp;</td>
																						</tr>
																						<tr>
																						<td width="900px">
																						Response time : 12/01/2021 14:40 Status: Successfully Updated
																						<!--<td><span class="badge badge-secondary badge-pill">Failed</span></td>-->
																						</tr>
																						
																					</table>
												</div>
												<div class="tab-pane fade p-0" id="reboot" role="tabpanel" aria-labelledby="contact-tab">
													<table>
<tr>
<td width="900px">
Request Recived time : 14/01/2021 11:30 
<td><span class="badge badge-success badge-pill">Success</span></td>
</tr>
<tr>
<td>Check List <ol>Reboot Commands &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;></ol>
	
</td>

</tr>
<tr>
<td width="900px">
Response time : 14/01/2021 14:35 Status: Rebooted Successfully
<!--<td><span class="badge badge-secondary badge-pill">Failed</span></td>-->
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>


</table>
												</div>
												<div class="tab-pane fade p-0" id="sendconfig" role="tabpanel" aria-labelledby="contact-tab">
													<table>
<tr>
<td width="900px">
Request Recived time : 12/01/2021 14:30 
<td><span class="badge badge-success badge-pill">Success</span></td>
</tr>
<tr>
<td>Check List <ol>"config" + hostIpAddress + ".xml" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;></ol>
	<ol>/opt/resonous/config/" + configFileName&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;></ol>
</td>

</tr>
<tr>
<td width="900px">
Response time : 12/01/2021 14:40 Status: SendConfig Updated Successfully
<!--<td><span class="badge badge-secondary badge-pill">Failed</span></td>-->
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td width="900px">
Request Recived time : 12/01/2021 11:35
<td><span class="badge badge-secondary badge-pill">Failed</span></td>
</tr>
<tr>
<td>Check List <ol>"config" + hostIpAddress + ".xml" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" unchecked disabled background-color:red;></ol>
	<ol>/opt/resonous/config/" + configFileName&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox"  id="vehicle1" name="vehicle1" value="Bike" background-color:red;></ol>
</td>

</tr>
<tr>
<td width="900px">
Response time : 12/01/2021 14:40 Status: SendConfig Updated Failed
<!--<td><span class="badge badge-secondary badge-pill">Failed</span></td>-->
</tr>
<tr>


</table>
												</div>
												<div class="tab-pane fade p-0" id="dashboard" role="tabpanel" aria-labelledby="contact-tab">
													<table>
<tr>
<td width="900px">
Request Recived time : 12/01/2021 14:30 
<td><span class="badge badge-success badge-pill">Success</span></td>
</tr>
<tr>
<td>Check List <ol>dfe_power_temperature_get all &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;></ol>
	<ol>dfe_frequency_get &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;></ol>
</td>

</tr>
<tr>
<td width="900px">
Response time : 12/01/2021 14:40 
<!--<td><span class="badge badge-secondary badge-pill">Failed</span></td>-->
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>


</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div></td>

</tr>
</table>
													</div>
												</div>
												<div class="accordion">
													<div class="accordion-header " data-toggle="collapse" data-target="#panel-body-2">
														<h4>192.168.12.11</h4>
													</div>
													<div class="accordion-body collapse border border-top-0" id="panel-body-2" data-parent="#accordion">
														<table>
<tr>
<td width="0px">Status : </td>
<td width="350px">NMS &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="badge badge-success badge-pill">Active</span></td>
</tr>
<tr>
<td>IP Address :</td>
<td width="350px">192.168.12.11</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td><div class="row">
								<div class="col-12">
									<div class="card">
										<div class="card-header">
											<h4>User Activity</h4>
										</div>
										<div class="card-body">
											<ul class="nav nav-tabs" id="myTab" role="tablist">
												<li class="nav-item">
													<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home1" role="tab" aria-controls="home" aria-selected="true">View</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile1" role="tab" aria-controls="profile" aria-selected="false">BringUp&Upgrade</a>

												</li>

												<li class="nav-item">

													<a class="nav-link" id="contact-tab" data-toggle="tab" href="#reboot1" role="tab" aria-controls="contact" aria-selected="false">Reboot</a>

												</li>
<li class="nav-item">

													<a class="nav-link" id="contact-tab" data-toggle="tab" href="#sendconfig1" role="tab" aria-controls="contact" aria-selected="false">SendConfig</a>
												</li>
<li class="nav-item">

													<a class="nav-link" id="contact-tab" data-toggle="tab" href="#dashboard1" role="tab" aria-controls="contact" aria-selected="false">Dashboard</a>
												</li>

											</ul>

											<div class="tab-content  p-3" id="myTabContent">

												<div class="tab-pane fade show active p-0" id="home1" role="tabpanel" aria-labelledby="home-tab">

<table>
<tr>
<td width="900px">
Request Recived time : 12/01/2021 14:30 
<td><span class="badge badge-success badge-pill">Success</span></td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td width="900px">
Request Recived time : 12/01/2021 16:30
<td><span class="badge badge-secondary badge-pill">Failed</span></td>
</tr>

</table>

												</div>

												<div class="tab-pane fade p-0" id="profile1" role="tabpanel" aria-labelledby="profile-tab">

													<table>
<tr>
<td width="900px">
Request Recived time : 12/01/2021 14:30 
<td><span class="badge badge-success badge-pill">Success</span></td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td>Check List <ol>chmod 777 /opt/resonous/" + fileName + ""&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;></ol><ol>tar zxvf /opt/resonous/" + fileName + " -C /opt/resonous/&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;></ol></td>

</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td width="900px">
Response time : 12/01/2021 14:40 Status: Successfully Updated
<!--<td><span class="badge badge-secondary badge-pill">Failed</span></td>-->
</tr>

</table>
												</div>
												<div class="tab-pane fade p-0" id="reboot1" role="tabpanel" aria-labelledby="contact-tab">
													<table>
<tr>
<td width="900px">
Request Recived time : 14/01/2021 11:30 
<td><span class="badge badge-success badge-pill">Success</span></td>
</tr>
<tr>
<td>Check List <ol>Reboot Commands &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;></ol>
	
</td>

</tr>
<tr>
<td width="900px">
Response time : 14/01/2021 14:35 Status: Rebooted Successfully
<!--<td><span class="badge badge-secondary badge-pill">Failed</span></td>-->
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>


</table>
												</div>
												<div class="tab-pane fade p-0" id="sendconfig1" role="tabpanel" aria-labelledby="contact-tab">
													<table>
<tr>
<td width="900px">
Request Recived time : 12/01/2021 14:30 
<td><span class="badge badge-success badge-pill">Success</span></td>
</tr>
<tr>
<td>Check List <ol>"config" + hostIpAddress + ".xml" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;></ol>
	<ol>/opt/resonous/config/" + configFileName&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;></ol>
</td>

</tr>
<tr>
<td width="900px">
Response time : 12/01/2021 14:40 Status: SendConfig Updated Successfully
<!--<td><span class="badge badge-secondary badge-pill">Failed</span></td>-->
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td width="900px">
Request Recived time : 12/01/2021 11:35
<td><span class="badge badge-secondary badge-pill">Failed</span></td>
</tr>
<tr>
<td>Check List <ol>"config" + hostIpAddress + ".xml" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" unchecked disabled background-color:red;></ol>
	<ol>/opt/resonous/config/" + configFileName&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox"  id="vehicle1" name="vehicle1" value="Bike" background-color:red;></ol>
</td>

</tr>
<tr>
<td width="900px">
Response time : 12/01/2021 14:40 Status: SendConfig Updated Failed
<!--<td><span class="badge badge-secondary badge-pill">Failed</span></td>-->
</tr>
<tr>


</table>
												</div>
												<div class="tab-pane fade p-0" id="dashboard1" role="tabpanel" aria-labelledby="contact-tab">
													<table>
<tr>
<td width="900px">
Request Recived time : 12/01/2021 14:30 
<td><span class="badge badge-success badge-pill">Success</span></td>
</tr>
<tr>
<td>Check List <ol>dfe_power_temperature_get all &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;></ol>
	<ol>dfe_frequency_get &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;></ol>
</td>

</tr>
<tr>
<td width="900px">
Response time : 12/01/2021 14:40 
<!--<td><span class="badge badge-secondary badge-pill">Failed</span></td>-->
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>


</table>
													</div>
												</div>
											</div>
												
											</div>

										</div>
									</div>
								</div>
							</div>
							<!--row closed-->

                            <!--row open-->
							<!--<div class="row">
								<div class="col-12">
									<div class="card">
										<div class="card-header">
											<h4>Badges</h4>
										</div>
										<div class="card-body">
											<div class="row">
												<div class="col-lg-6">
													<h2 class="section-title mt-0">Heading</h2>
													<h1>Example heading <span class="badge badge-primary">New</span></h1>
													<h2>Example heading <span class="badge badge-primary">New</span></h2>
													<h3>Example heading <span class="badge badge-primary">New</span></h3>
													<h4>Example heading <span class="badge badge-primary">New</span></h4>
													<h5>Example heading <span class="badge badge-primary">New</span></h5>
													<h6 class="mb-0">Example heading <span class="badge badge-primary">New</span></h6>
												</div>
												<div class="col-lg-6">
													<h2 class="section-title">Button</h2>
													<button type="button" class="btn btn-primary">
													  Notifications <span class="badge badge-light">4</span>
													</button>
													<h2 class="section-title">Pill Badges</h2>
													<span class="badge badge-primary m-b-5">Primary</span>
													<span class="badge badge-secondary m-b-5">Secondary</span>
													<span class="badge badge-success m-b-5">Success</span>
													<span class="badge badge-danger m-b-5">Danger</span>
													<span class="badge badge-warning m-b-5">Warning</span>
													<span class="badge badge-info m-b-5">Info</span>
													<span class="badge badge-light m-b-5">Light</span>
													<span class="badge badge-dark m-b-5">Dark</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>-->
							<!--row closed-->

                            <!--row open-->
							<!--<div class="row">
								<div class="col-lg-6 col-md-12">
									<div class="card">
										<div class="card-header">
											<h4>Pagination Style1</h4>
										</div>
										<div class="card-body">
											<h2 class="section-title mt-0">Quick Example</h2>
											<nav aria-label="Page navigation example">
												<ul class="pagination  mb-0">
													<li class="page-item">
														<a class="page-link" href="#">Prev</a>
													</li>
													<li class="page-item">
														<a class="page-link" href="#">1</a>
													</li>
													<li class="page-item">
														<a class="page-link" href="#">2</a>
													</li>
													<li class="page-item">
														<a class="page-link" href="#">3</a>
													</li>
													<li class="page-item">
														<a class="page-link" href="#">Nex</a>
													</li>
												</ul>
											</nav>
										</div>
									</div>
								</div>

								<div class="col-lg-6 col-md-12">
									<div class="card">
										<div class="card-header">
											<h4>Pagination style2</h4>
										</div>
										<div class="card-body">
											<h2 class="section-title mt-0">Working With Icons</h2>
											<nav aria-label="Page navigation example">
												<ul class="pagination mb-0">
													<li class="page-item">
														<a class="page-link" href="#" aria-label="Previous">
															<span aria-hidden="true">&laquo;</span>
															<span class="sr-only">Prev</span>
														</a>
													</li>
													<li class="page-item">
														<a class="page-link" href="#">1</a>
													</li>
													<li class="page-item">
														<a class="page-link" href="#">2</a>
													</li>
													<li class="page-item">
														<a class="page-link" href="#">3</a>
													</li>
													<li class="page-item">
														<a class="page-link" href="#" aria-label="Next">
															<span aria-hidden="true">&raquo;</span>
															<span class="sr-only">Nex</span>
														</a>
													</li>
												</ul>
											</nav>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-12">
									<div class="card">
										<div class="card-header">
											<h4>Pagination style3</h4>
										</div>
										<div class="card-body">
											<h2 class="section-title mt-0">Active State</h2>
											<nav>
												<ul class="pagination mb-0">
													<li class="page-item disabled">
														<span class="page-link">Prev</span>
													</li>
													<li class="page-item">
														<a class="page-link" href="#">1</a>
													</li>
													<li class="page-item active">
														<span class="page-link">
															2
															<span class="sr-only">(current)</span>
														</span>
													</li>
													<li class="page-item">
														<a class="page-link" href="#">3</a>
													</li>
													<li class="page-item">
														<a class="page-link" href="#">Nex</a>
													</li>
												</ul>
											</nav>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-12">
									<div class="card">
										<div class="card-header">
											<h4>Pagination Sizing</h4>
										</div>
										<div class="card-body">
											<h2 class="section-title mt-0" >Sizing</h2>
											<nav>
												<ul class="pagination pagination-lg mb-0">
													<li class="page-item disabled">
														<a class="page-link" href="#" tabindex="-1">1</a>
													</li>
													<li class="page-item">
														<a class="page-link" href="#">2</a>
													</li>
													<li class="page-item">
														<a class="page-link" href="#">3</a>
													</li>
												</ul>
											</nav>
										</div>
									</div>
								</div>
							</div>-->
							<!--row closed-->

                            <!--row open-->
							<!--<div class="row">
								<div class="col-lg-12 col-xl-6 col-md-12 col-12 col-sm-12">
									<div class="card">
										<div class="card-header">
											<h4>Simple navs</h4>
										</div>
										<div class="card-body">
											<div  class="active">
												<ul class="nav">
													<li class="nav-item">
														<a class="nav-link active" href="#">Active</a>
													</li>
													<li class="nav-item">
														<a class="nav-link" href="#">Link</a>
													</li>
													<li class="nav-item">
														<a class="nav-link" href="#">Link</a>
													</li>
													<li class="nav-item">
														<a class="nav-link disabled" href="#">Disabled</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-12 col-xl-6 col-md-12 col-12 col-sm-12">
									<div class="card">
										<div class="card-header">
											<h4>Navs Pills</h4>
										</div>
										<div class="card-body">
											<div  class="active">
												<ul class="nav nav-pills">
													<li class="nav-item">
														<a class="nav-link active" href="#">Active</a>
													</li>
													<li class="nav-item">
														<a class="nav-link" href="#">Link</a>
													</li>
													<li class="nav-item">
														<a class="nav-link" href="#">Link</a>
													</li>
													<li class="nav-item">
														<a class="nav-link disabled" href="#">Disabled</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>-->
							<!--row closed-->

                            <!--row open-->
							<!--<div class="row">
								<div class="col-lg-12 col-xl-6 col-md-12 col-12 col-sm-12">
									<div class="card">
										<div class="card-header">
											<h4>Navs Vertical</h4>
										</div>
										<div class="card-body">
											<div id="navs-vertical">
												<ul class="nav flex-column">
													<li class="nav-item">
														<a class="nav-link active" href="#">Active</a>
													</li>
													<li class="nav-item">
														<a class="nav-link" href="#">Link</a>
													</li>
													<li class="nav-item">
														<a class="nav-link" href="#">Link</a>
													</li>
													<li class="nav-item">
														<a class="nav-link disabled" href="#">Disabled</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-12 col-xl-6 col-md-12 col-12 col-sm-12">
									<div class="card">
										<div class="card-header">
											<h4>Navs Tabs</h4>
										</div>
										<div class="card-body">
											<div id="navs-tabs">
												<ul class="nav nav-tabs mb-4">
													<li class="nav-item">
													<a class="nav-link active" href="#">Active</a>
													</li>
													<li class="nav-item dropdown">
													<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Dropdown</a>
													<div class="dropdown-menu">
													<a class="dropdown-item" href="#">Action</a>
													<a class="dropdown-item" href="#">Another action</a>
													<a class="dropdown-item" href="#">Something else here</a>
													<div class="dropdown-divider"></div>
													<a class="dropdown-item" href="#">Separated link</a>
													</div>
													</li>
													<li class="nav-item">
													<a class="nav-link" href="#">Link</a>
													</li>
													<li class="nav-item">
													<a class="nav-link disabled" href="#">Disabled</a>
													</li>
												</ul>
											</div>
											<h2 class="section-title ">Navs Pill-Dropdowns</h2>
											<div id="navs-pills-dropdown">
												<ul class="nav nav-pills mt-3">
													<li class="nav-item">
														<a class="nav-link active" href="#">Active</a>
													</li>
													<li class="nav-item dropdown">
														<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Dropdown</a>
														<div class="dropdown-menu">
															<a class="dropdown-item" href="#">Action</a>
															<a class="dropdown-item" href="#">Another action</a>
															<a class="dropdown-item" href="#">Something else here</a>
															<div class="dropdown-divider"></div>
															<a class="dropdown-item" href="#">Separated link</a>
														</div>
													</li>
													<li class="nav-item">
														<a class="nav-link" href="#">Link</a>
													</li>
													<li class="nav-item">
														<a class="nav-link disabled" href="#">Disabled</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>-->
							<!--row closed-->

                            <!--row open-->
							<!--<div class="row">
								<div class="col-lg-12 col-xl-6 col-md-12 col-12 col-sm-12">
									<div class="card">
										<div class="card-header">
											<h4>Dropdowns</h4>
										</div>
										<div class="card-body">
											<div class="dropdown d-inline">
												<button class="btn btn-primary m-b-5 m-t-5 dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													Easy Dropdown
												</button>
												<div class="dropdown-menu">
													<a class="dropdown-item" href="#">Action</a>
													<a class="dropdown-item" href="#">Another action</a>
													<a class="dropdown-item" href="#">Something else here</a>
												</div>
											</div>
											<div class="btn-group">
												<button type="button" class="btn btn-danger m-b-5 m-t-5">Split Dropdown</button>
												<button type="button" class="btn btn-danger m-b-5 m-t-5 dropdown-toggle dropdown-toggle-split" data-toggle="dropdown">
													<span class="sr-only">Toggle Dropdown</span>
												</button>
												<div class="dropdown-menu">
													<a class="dropdown-item" href="#">Action</a>
													<a class="dropdown-item" href="#">Another action</a>
													<a class="dropdown-item" href="#">Something else here</a>
													<div class="dropdown-divider"></div>
													<a class="dropdown-item" href="#">Separated link</a>
												</div>
											</div>
											<div class="dropdown d-inline">
												<button class="btn btn-success dropdown-toggle m-b-5 m-t-5" type="button" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													With Icon
												</button>
												<div class="dropdown-menu">
													<a class="dropdown-item has-icon" href="#"><i class="ion ion-heart"></i> Action</a>
													<a class="dropdown-item has-icon" href="#"><i class="ion ion-document"></i> Another action</a>
													<a class="dropdown-item has-icon" href="#"><i class="ion ion-clock"></i> Something else here</a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-12 col-xl-6 col-md-12 col-12 col-sm-12">
									<div class="card">
										<div class="card-header">
											<h4>DropUps</h4>
										</div>
										<div class="card-body">
											<div class="btn-group dropup">
												<button type="button" class="btn btn-primary m-b-5 m-t-5 dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													Dropup
												</button>
												<div class="dropdown-menu">
													<a class="dropdown-item" href="#">Action</a>
													<a class="dropdown-item" href="#">Another action</a>
													<a class="dropdown-item" href="#">Something else here</a>
													<div class="dropdown-divider"></div>
													<a class="dropdown-item" href="#">Separated link</a>
												</div>
											</div>
											<div class="btn-group dropup">
												<button type="button" class="btn btn-success m-b-5 m-t-5">
													Split dropup
												</button>
												<button type="button" class="btn btn-success m-b-5 m-t-5 dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													<span class="sr-only">Toggle Dropdown</span>
												</button>
												<div class="dropdown-menu">
													<a class="dropdown-item" href="#">Action</a>
													<a class="dropdown-item" href="#">Another action</a>
													<a class="dropdown-item" href="#">Something else here</a>
													<div class="dropdown-divider"></div>
													<a class="dropdown-item" href="#">Separated link</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--row closed-->

                            <!--row open-->
							<!--<div class="row">
								<div class="col-lg-12 col-xl-6 col-md-12 col-12 col-sm-12">
									<div class="card">
										<div class="card-header">
											<h4>Dropdown Sizes</h4>
										</div>
										<div class="card-body">
											<div class="btn-group">
												<button class="btn btn-secondary btn-lg m-b-5 m-t-5 dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													Large button
												</button>
												<div class="dropdown-menu">
													<a class="dropdown-item" href="#">Action</a>
													<a class="dropdown-item" href="#">Another action</a>
													<a class="dropdown-item" href="#">Something else here</a>
													<div class="dropdown-divider"></div>
													<a class="dropdown-item" href="#">Separated link</a>
												</div>
											</div>
											<div class="btn-group">
												<button class="btn btn-secondary btn-lg m-b-5 m-t-5" type="button">
													Split button
												</button>
												<button type="button" class="btn btn-lg btn-secondary m-b-5 m-t-5 dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													<span class="sr-only">Toggle Dropdown</span>
												</button>
												<div class="dropdown-menu">
													<a class="dropdown-item" href="#">Action</a>
													<a class="dropdown-item" href="#">Another action</a>
													<a class="dropdown-item" href="#">Something else here</a>
													<div class="dropdown-divider"></div>
													<a class="dropdown-item" href="#">Separated link</a>
												</div>
											</div>
											<div class="btn-group">
												<button class="btn btn-primary btn-sm m-b-5 m-t-5 dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													Small button
												</button>
												<div class="dropdown-menu">
													<a class="dropdown-item" href="#">Action</a>
													<a class="dropdown-item" href="#">Another action</a>
													<a class="dropdown-item" href="#">Something else here</a>
													<div class="dropdown-divider"></div>
													<a class="dropdown-item" href="#">Separated link</a>
												</div>
											</div>
											<div class="btn-group">
												<button class="btn btn-primary btn-sm m-b-5 m-t-5" type="button">
													Split button
												</button>
												<button type="button" class="btn btn-sm btn-primary m-b-5 m-t-5 dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													<span class="sr-only">Toggle Dropdown</span>
												</button>
												<div class="dropdown-menu">
													<a class="dropdown-item" href="#">Action</a>
													<a class="dropdown-item" href="#">Another action</a>
													<a class="dropdown-item" href="#">Something else here</a>
													<div class="dropdown-divider"></div>
													<a class="dropdown-item" href="#">Separated link</a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-12 col-xl-6 col-md-12 col-12 col-sm-12">
									<div class="card">
										<div class="card-header">
											<h4>Dropdown Variation</h4>
										</div>
										<div class="card-body">
											<div class="btn-group dropright">
												<button type="button" class="btn btn-primary m-b-5 m-t-5 dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													Dropright
												</button>
												<div class="dropdown-menu">
													<a class="dropdown-item" href="#">Action</a>
													<a class="dropdown-item" href="#">Another action</a>
													<a class="dropdown-item" href="#">Something else here</a>
													<div class="dropdown-divider"></div>
													<a class="dropdown-item" href="#">Separated link</a>
												</div>
											</div>
											<div class="btn-group dropright">
												<button type="button" class="btn btn-primary m-b-5 m-t-5">
													Split dropright
												</button>
												<button type="button" class="btn btn-primary m-b-5 m-t-5 dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													<span class="sr-only">Toggle Dropright</span>
												</button>
												<div class="dropdown-menu">
													<a class="dropdown-item" href="#">Action</a>
													<a class="dropdown-item" href="#">Another action</a>
													<a class="dropdown-item" href="#">Something else here</a>
													<div class="dropdown-divider"></div>
													<a class="dropdown-item" href="#">Separated link</a>
												</div>
											</div><br>
											<div class="btn-group dropleft">
												<button type="button" class="btn btn-secondary m-b-5 m-t-5 dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													dropleft
												</button>
												<div class="dropdown-menu">
													<a class="dropdown-item" href="#">Action</a>
													<a class="dropdown-item" href="#">Another action</a>
													<a class="dropdown-item" href="#">Something else here</a>
													<div class="dropdown-divider"></div>
													<a class="dropdown-item" href="#">Separated link</a>
												</div>
											</div>
											<div class="btn-group dropleft">
												<button type="button" class="btn btn-secondary m-b-5 m-t-5">
													Split dropleft
												</button>
												<button type="button" class="btn btn-secondary m-b-5 m-t-5 dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													<span class="sr-only">Toggle Dropright</span>
												</button>
												<div class="dropdown-menu">
													<a class="dropdown-item" href="#">Action</a>
													<a class="dropdown-item" href="#">Another action</a>
													<a class="dropdown-item" href="#">Something else here</a>
													<div class="dropdown-divider"></div>
													<a class="dropdown-item" href="#">Separated link</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>-->
							<!--row closed-->

                            <!--row open-->
							<!--<div class="row">
								<div class="col-lg-12 col-xl-6 col-md-12 col-12 col-sm-12">
									<div class="card">
										<div class="card-header">
											<h4>Simple Lists</h4>
										</div>
										<div class="card-body">
											<div class="active" id="lg-simple">
												<ul class="list-group">
													<li class="list-group-item">Cras justo odio</li>
													<li class="list-group-item">Dapibus ac facilisis in</li>
													<li class="list-group-item">Morbi leo risus</li>
													<li class="list-group-item">Porta ac consectetur ac</li>
													<li class="list-group-item">Vestibulum at eros</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-12 col-xl-6 col-md-12 col-12 col-sm-12">
									<div class="card">
										<div class="card-header">
											<h4>Active Lists</h4>
										</div>
										<div class="card-body">
											<div id="lg-active">
												<ul class="list-group">
													<li class="list-group-item">Cras justo odio</li>
													<li class="list-group-item active">Dapibus ac facilisis in</li>
													<li class="list-group-item">Morbi leo risus</li>
													<li class="list-group-item">Porta ac consectetur ac</li>
													<li class="list-group-item">Vestibulum at eros</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>-->
							<!--row closed-->

                            <!--row open-->
							<!--<div class="row">
								<div class="col-lg-12 col-xl-6 col-md-12 col-12 col-sm-12">
									<div class="card">
										<div class="card-header">
											<h4>Clickable Lists</h4>
										</div>
										<div class="card-body">
											<div id="lg-clickable">
												<div class="list-group">
													<a href="#" class="list-group-item list-group-item-action active">
														Cras justo odio
													</a>
													<a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a>
													<a href="#" class="list-group-item list-group-item-action">Morbi leo risus</a>
													<a href="#" class="list-group-item list-group-item-action">Porta ac consectetur ac</a>
													<a href="#" class="list-group-item list-group-item-action disabled">Vestibulum at eros</a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-12 col-xl-6 col-md-12 col-12 col-sm-12">
									<div class="card">
										<div class="card-header">
											<h4>Flush Lists</h4>
										</div>
										<div class="card-body">
											<div id="lg-flush">
												<ul class="list-group list-group-flush">
													<li class="list-group-item">Cras justo odio</li>
													<li class="list-group-item">Dapibus ac facilisis in</li>
													<li class="list-group-item">Morbi leo risus</li>
													<li class="list-group-item">Porta ac consectetur ac</li>
													<li class="list-group-item">Vestibulum at eros</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>-->
							<!--row closed-->

                            <!--row open-->
							<!--<div class="row">
								<div class="col-md-12 col-12 col-sm-12">
									<div class="card">
										<div class="card-header">
											<h4>Badges Lists</h4>
										</div>
										<div class="card-body">
											<div class="row">
												<div class="col-lg-6">
													<div id="lg-badges">
														<ul class="list-group">
															<li class="list-group-item d-flex justify-content-between align-items-center">
																Cras justo odio
																<span class="badge badge-info badge-pill">14</span>
															</li>
															<li class="list-group-item d-flex justify-content-between align-items-center">
																Dapibus ac facilisis in
																<span class="badge badge-secondary badge-pill">2</span>
															</li>
															<li class="list-group-item d-flex justify-content-between align-items-center">
																Morbi leo risus
																<span class="badge badge-warning badge-pill">1</span>
															</li>
														</ul>
													</div>
												</div>
												<div class="col-lg-6">
													<div id="lg-badges1" class="mt-4 mt-lg-0">
														<ul class="list-group">
															<li class="list-group-item d-flex justify-content-between align-items-center">
																Cras justo odio
																<span class="badge badge-primary badge-pill">14</span>
															</li>
															<li class="list-group-item d-flex justify-content-between align-items-center">
																Dapibus ac facilisis in
																<span class="badge badge-success badge-pill">2</span>
															</li>
															<li class="list-group-item d-flex justify-content-between align-items-center">
																Morbi leo risus
																<span class="badge badge-danger badge-pill">1</span>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>-->
							<!--row closed-->

                            <!--row open-->
							<!--<div class="row">
								<div class="col-md-12 col-12 col-sm-12">
									<div class="card">
										<div class="card-header">
											<h4>Custom Lists</h4>
										</div>
										<div class="card-body">
											<div id="lg-custom">
												<div class="list-group">
													<a href="#" class="list-group-item list-group-item-action flex-column align-items-start active">
														<div class="d-sm-flex w-100 justify-content-between">
															<h5 class="mb-1">List group item heading</h5>
															<small>3 days ago</small>
														</div>
														<p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
														<small>Donec id elit non mi porta.</small>
													</a>
													<a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
														<div class="d-sm-flex w-100 justify-content-between">
															<h5 class="mb-1">List group item heading</h5>
															<small class="text-muted">3 days ago</small>
														</div>
														<p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
														<small class="text-muted">Donec id elit non mi porta.</small>
													</a>
													<a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
														<div class="d-sm-flex w-100 justify-content-between">
															<h5 class="mb-1">List group item heading</h5>
															<small class="text-muted">3 days ago</small>
														</div>
														<p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
														<small class="text-muted">Donec id elit non mi porta.</small>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>-->
							<!--row closed-->

                            <!--row open-->
							<!--<div class="row">
								<div class="col-12">
									<div class="card">
										<div class="card-header">
											<h4>Tooltips</h4>
										</div>
										<div class="card-body">
											<button type="button" class="btn btn-info m-b-5 m-t-5" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
												Tooltip on top
											</button>
											<button type="button" class="btn btn-primary m-b-5 m-t-5" data-toggle="tooltip" data-placement="right" title="Tooltip on right">
												Tooltip on right
											</button>
											<button type="button" class="btn btn-success m-b-5 m-t-5" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom">
												Tooltip on bottom
											</button>
											<button type="button" class="btn btn-danger m-b-5 m-t-5" data-toggle="tooltip" data-placement="left" title="Tooltip on left">
												Tooltip on left
											</button>
										</div>
									</div>
								</div>
							</div>-->
							<!--row closed-->

                            <!--row open-->
							<!--<div class="row">
								<div class="col-12">
									<div class="card">
										<div class="card-header">
											<h4>Popover</h4>
										</div>
										<div class="card-body">
											<button type="button" class="btn btn-primary m-b-5" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
												Popover on top
											</button>
											<button type="button" class="btn btn-success m-b-5" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
												Popover on right
											</button>
											<button type="button" class="btn btn-secondary m-b-5" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Vivamus
											sagittis lacus vel augue laoreet rutrum faucibus.">
												Popover on bottom
											</button>
											<button type="button" class="btn btn-warning m-b-5" data-container="body" data-toggle="popover" data-placement="left" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
												Popover on left
											</button>
											<h2 class="section-title">Dismiss popover</h2>
											<a tabindex="0" class="btn btn-lg btn-danger" role="button" data-toggle="popover" data-trigger="focus" title="Dismissible popover" data-content="And here's some amazing content. It's very engaging. Right?">Dismiss popover</a>
										</div>
									</div>
								</div>
							</div>-->
							<!--row closed-->

                            <!--row open-->
							<!--<div class="row">
								<div class="col-lg-12 col-xl-6 col-md-12 col-12 col-sm-12">
									<div class="card">
										<div class="card-header">
											<h4>Simple Progress bars</h4>
										</div>
										<div class="card-body">
											<div id="progress-simple"  class="active">
												<div class="progress mb-3">
													<div class="progress-bar " role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												<div class="progress mb-3">
													<div class="progress-bar bg-primary w-30" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												<div class="progress mb-3">
													<div class="progress-bar bg-primary w-50" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												<div class="progress mb-3">
													<div class="progress-bar bg-primary w-70" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												<div class="progress">
													<div class="progress-bar bg-primary w-100" role="progressbar"  aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-12 col-xl-6 col-md-12 col-12 col-sm-12">
									<div class="card">
										<div class="card-header">
											<h4>Labels Progress bars</h4>
										</div>
										<div class="card-body">
											<div id="progress-labels">
												<div class="progress mb-3">
													<div class="progress-bar bg-primary w-30" role="progressbar" aria-valuemin="0" aria-valuemax="100">30%</div>
												</div>
												<div class="progress mb-3">
													<div class="progress-bar bg-primary w-40" role="progressbar" aria-valuemin="0" aria-valuemax="100">40%</div>
												</div>
												<div class="progress mb-3">
													<div class="progress-bar bg-primary w-50" role="progressbar" aria-valuemin="0" aria-valuemax="100">50%</div>
												</div>
												<div class="progress mb-3">
													<div class="progress-bar bg-primary w-80" role="progressbar" aria-valuemin="0" aria-valuemax="100">80%</div>
												</div>
												<div class="progress ">
													<div class="progress-bar bg-primary w-50" role="progressbar" aria-valuemin="0" aria-valuemax="100">50%</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>-->
							<!--row close-->

                            <!--row open-->
							<!--<div class="row">
								<div class="col-lg-12 col-xl-6 col-md-12 col-12 col-sm-12">
									<div class="card">
										<div class="card-header">
											<h4>Progress bar Backgrounds</h4>
										</div>
										<div class="card-body">
											<div id="progress-background">
												<div class="progress mb-3">
													<div class="progress-bar bg-cyan w-20" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												<div class="progress mb-3">
													<div class="progress-bar bg-primary w-50" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												<div class="progress mb-3">
													<div class="progress-bar bg-secondary w-60" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												<div class="progress">
													<div class="progress-bar bg-success w-100" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-12 col-xl-6 col-md-12 col-12 col-sm-12">
									<div class="card">
										<div class="card-header">
											<h4>Progress bar Multiple</h4>
										</div>
										<div class="card-body">
											<div id="progress-multiple">
												<div class="progress mb-3">
													<div class="progress-bar bg-primary w-10" role="progressbar" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
													<div class="progress-bar bg-success w-10" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
													<div class="progress-bar bg-info w-20" role="progressbar"  aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												<div class="progress mb-3">
													<div class="progress-bar bg-success w-10" role="progressbar"  aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
													<div class="progress-bar bg-primary w-20" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
													<div class="progress-bar bg-secondary w-10" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												<div class="progress mb-3">
													<div class="progress-bar bg-dark w-10" role="progressbar" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
													<div class="progress-bar bg-cyan w-30" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
													<div class="progress-bar bg-warning w-20" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												<div class="progress mb-0">
													<div class="progress-bar bg-gray w-20" role="progressbar" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
													<div class="progress-bar bg-primary w-10" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
													<div class="progress-bar bg-success w-40" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>-->
							<!--row closed-->

                            <!--row open-->
							<!--<div class="row">
								<div class="col-lg-12 col-xl-6 col-md-12 col-12 col-sm-12">
									<div class="card">
										<div class="card-header">
											<h4>Progress bar Stripted</h4>
										</div>
										<div class="card-body">
											<div id="progress-striped">
												<div class="progress mb-3">
													<div class="progress-bar progress-bar-striped w-10" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												<div class="progress mb-3">
													<div class="progress-bar progress-bar-striped bg-success w-20" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												<div class="progress mb-3">
													<div class="progress-bar progress-bar-striped bg-secondary w-50" role="progressbar"  aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												<div class="progress">
													<div class="progress-bar progress-bar-striped bg-warning w-70" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-12 col-xl-6 col-md-12 col-12 col-sm-12">
									<div class="card">
										<div class="card-header">
											<h4>Progress bar Animation Striped</h4>
										</div>
										<div class="card-body">
											<div id="progress-striped1">
												<div class="progress mb-3">
													<div class="progress-bar progress-bar-striped progress-bar-animated w-10" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												<div class="progress mb-3">
													<div class="progress-bar progress-bar-striped bg-success progress-bar-animated w-20" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												<div class="progress mb-3">
													<div class="progress-bar progress-bar-striped bg-secondary progress-bar-animated w-50" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												<div class="progress">
													<div class="progress-bar progress-bar-striped bg-warning progress-bar-animated w-70" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>-->
							<!--row closed-->

                            <!--row open-->
							<!--<div class="row">
								<div class="col-12">
									<div class="card">
										<div class="card-header">
											<h4>Default Alerts</h4>
										</div>
										<div class="card-body">
											<div class="row">
												<div class="col-lg-6">
													<div  class="active" id="default">
														<div class="alert alert-primary">
															This is a primary alert.
														</div>
														<div class="alert alert-secondary">
															This is a secondary alert.
														</div>
														<div class="alert alert-success">
															This is a success alert.
														</div>
														<div class="alert alert-danger mb-lg-0">
															This is a danger alert.
														</div>
													</div>
												</div>
												<div class="col-lg-6">
													<div  class="active" id="default1">
														<div class="alert alert-warning">
															This is a warning alert.
														</div>
														<div class="alert alert-info">
															This is a info alert.
														</div>
														<div class="alert alert-light">
															This is a light alert.
														</div>
														<div class="alert alert-dark mb-0">
															This is a dark alert.
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>-->
							<!--row closed-->

                            <!--row open-->
							<!--<div class="row">
								<div class="col-12">
									<div class="card">
										<div class="card-header">
											<h4>Title Alerts</h4>
										</div>
										<div class="card-body">
											<div class="row ">
												<div class="col-lg-6">
													<div  id="with-title">
														<div class="alert alert-primary">
															<div class="alert-title">Primary</div>
															This is a primary alert.
														</div>
														<div class="alert alert-secondary">
															<div class="alert-title">Secondary</div>
															This is a secondary alert.
														</div>
														<div class="alert alert-success">
															<div class="alert-title">Success</div>
															This is a success alert.
														</div>
														<div class="alert alert-danger mb-lg-0">
															<div class="alert-title">Danger</div>
															This is a danger alert.
														</div>
													</div>
												</div>
												<div class="col-lg-6">
													<div  id="with-title1">
														<div class="alert alert-warning">
															<div class="alert-title">Warning</div>
															This is a warning alert.
														</div>
														<div class="alert alert-info">
															<div class="alert-title">Info</div>
															This is a info alert.
														</div>
														<div class="alert alert-light">
															<div class="alert-title">Light</div>
															This is a light alert.
														</div>
														<div class="alert alert-dark mb-0">
															<div class="alert-title">Dark</div>
															This is a dark alert.
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>-->
							<!--row closed-->

                            <!--row open-->
							<!--<div class="row">
								<div class="col-12">
									<div class="card">
										<div class="card-header">
											<h4>Alerts With icons</h4>
										</div>
										<div class="card-body">
											<div class="row">
												<div class="col-lg-6">
													<div  id="with-icon">
														<div class="alert alert-primary alert-has-icon">
															<div class="alert-icon"><i class="ion ion-ios-lightbulb-outline"></i></div>
															<div class="alert-body">
																<div class="alert-title">Primary</div>
																This is a primary alert.
															</div>
														</div>
														<div class="alert alert-secondary alert-has-icon">
															<div class="alert-icon"><i class="ion ion-ios-lightbulb-outline"></i></div>
															<div class="alert-body">
																<div class="alert-title">Secondary</div>
																This is a secondary alert.
															</div>
														</div>
														<div class="alert alert-success alert-has-icon">
															<div class="alert-icon"><i class="ion ion-ios-lightbulb-outline"></i></div>
															<div class="alert-body">
																<div class="alert-title">Success</div>
																This is a success alert.
															</div>
														</div>
														<div class="alert alert-danger alert-has-icon mb-lg-0">
															<div class="alert-icon"><i class="ion ion-ios-lightbulb-outline"></i></div>
															<div class="alert-body">
																<div class="alert-title">Danger</div>
																This is a danger alert.
															</div>
														</div>
													</div>
												</div>
												<div class="col-lg-6">
													<div  id="with-icon1">
														<div class="alert alert-warning alert-has-icon">
															<div class="alert-icon"><i class="ion ion-ios-lightbulb-outline"></i></div>
															<div class="alert-body">
																<div class="alert-title">Warning</div>
																This is a warning alert.
															</div>
														</div>
														<div class="alert alert-info alert-has-icon">
															<div class="alert-icon"><i class="ion ion-ios-lightbulb-outline"></i></div>
															<div class="alert-body">
																<div class="alert-title">Info</div>
																This is a info alert.
															</div>
														</div>
														<div class="alert alert-light alert-has-icon">
															<div class="alert-icon"><i class="ion ion-ios-lightbulb-outline"></i></div>
															<div class="alert-body">
																<div class="alert-title">Light</div>
																This is a light alert.
															</div>
														</div>
														<div class="alert alert-dark alert-has-icon mb-0">
															<div class="alert-icon"><i class="ion ion-ios-lightbulb-outline"></i></div>
															<div class="alert-body">
																<div class="alert-title">Dark</div>
																This is a dark alert.
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>-->
							<!--row closed-->

                            <!--row open-->
							<!--<div class="row">
								<div class="col-12">
									<div class="card">
										<div class="card-header">
											<h4>Dismiss Alerts</h4>
										</div>
										<div class="card-body">
											<div class="row">
												<div class="col-lg-6">
													<div  id="dismissible">
														<div class="alert alert-primary alert-has-icon alert-dismissible show fade">
															<div class="alert-icon"><i class="ion ion-ios-lightbulb-outline"></i></div>
															<div class="alert-body">
																<button class="close" data-dismiss="alert">
																	<span>&times;</span>
																</button>
																<div class="alert-title">Primary</div>
																This is a primary alert.
															</div>
														</div>
														<div class="alert alert-secondary alert-has-icon alert-dismissible show fade">
															<div class="alert-icon"><i class="ion ion-ios-lightbulb-outline"></i></div>
															<div class="alert-body">
																<button class="close" data-dismiss="alert">
																	<span>&times;</span>
																</button>
																<div class="alert-title">Secondary</div>
																This is a secondary alert.
															</div>
														</div>
														<div class="alert alert-success alert-has-icon alert-dismissible show fade">
															<div class="alert-icon"><i class="ion ion-ios-lightbulb-outline"></i></div>
															<div class="alert-body">
																<button class="close" data-dismiss="alert">
																	<span>&times;</span>
																</button>
																<div class="alert-title">Success</div>
																This is a success alert.
															</div>
														</div>
														<div class="alert alert-danger alert-has-icon alert-dismissible show fade mb-lg-0">
															<div class="alert-icon"><i class="ion ion-ios-lightbulb-outline"></i></div>
															<div class="alert-body">
																<button class="close" data-dismiss="alert">
																	<span>&times;</span>
																</button>
																<div class="alert-title">Danger</div>
																This is a danger alert.
															</div>
														</div>
													</div>
												</div>
												<div class="col-lg-6">
													<div  id="dismissible1">
														<div class="alert alert-warning alert-has-icon alert-dismissible show fade">
															<div class="alert-icon"><i class="ion ion-ios-lightbulb-outline"></i></div>
															<div class="alert-body">
																<button class="close" data-dismiss="alert">
																	<span>&times;</span>
																</button>
																<div class="alert-title">Warning</div>
																This is a warning alert.
															</div>
														</div>
														<div class="alert alert-info alert-has-icon alert-dismissible show fade">
															<div class="alert-icon"><i class="ion ion-ios-lightbulb-outline"></i></div>
															<div class="alert-body">
																<button class="close" data-dismiss="alert">
																	<span>&times;</span>
																</button>
																<div class="alert-title">Info</div>
																This is a info alert.
															</div>
														</div>
														<div class="alert alert-light alert-has-icon alert-dismissible show fade">
															<div class="alert-icon"><i class="ion ion-ios-lightbulb-outline"></i></div>
															<div class="alert-body">
																<button class="close" data-dismiss="alert">
																	<span>&times;</span>
																</button>
																<div class="alert-title">Light</div>
																This is a light alert.
															</div>
														</div>
														<div class="alert alert-dark alert-has-icon alert-dismissible show fade mb-0">
															<div class="alert-icon"><i class="ion ion-ios-lightbulb-outline"></i></div>
															<div class="alert-body">
																<button class="close" data-dismiss="alert">
																	<span>&times;</span>
																</button>
																<div class="alert-title">Dark</div>
																This is a dark alert.
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>-->
							<!--row closed-->

                            <!--row open-->
							<!--<div class="row">
								<div class="col-12 col-lg-6">
									<div class="card">
										<div class="card-header">
											<h4>Simple Carousel</h4>
										</div>
										<div class="card-body">
											<div class="active" id="carousel-simple">
												<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
													<ol class="carousel-indicators">
														<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
														<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
														<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
													</ol>
													<div class="carousel-inner">
														<div class="carousel-item active">
															<img class="d-block w-100" src="..//Nms_Simulator/resources/assets/img/news/img01.jpg" alt="First slide">
														</div>
														<div class="carousel-item">
															<img class="d-block w-100" src="..//Nms_Simulator/resources/assets/img/news/img07.jpg" alt="Second slide">
														</div>
														<div class="carousel-item">
															<img class="d-block w-100" src="..//Nms_Simulator/resources/assets/img/news/img08.jpg" alt="Third slide">
														</div>
													</div>
													<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
														<span class="carousel-control-prev-icon" aria-hidden="true"></span>
														<span class="sr-only">Previous</span>
													</a>
													<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
														<span class="carousel-control-next-icon" aria-hidden="true"></span>
														<span class="sr-only">Next</span>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-12 col-lg-6">
									<div class="card">
										<div class="card-header">
											<h4>With Caption Carousel</h4>
										</div>
										<div class="card-body">
											<div  id="carousel-caption">
												<div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel">
													<ol class="carousel-indicators">
														<li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
														<li data-target="#carouselExampleIndicators2" data-slide-to="1"></li>
														<li data-target="#carouselExampleIndicators2" data-slide-to="2"></li>
													</ol>
													<div class="carousel-inner ">
														<div class="carousel-item active">
															<img class="d-block w-100" src="..//Nms_Simulator/resources/assets/img/news/img05.jpg" alt="First slide">
															<div class="carouseltransparent d-none d-md-block"></div>
															<div class="carousel-caption d-none d-md-block">
																<h5>Carousel Heading1</h5>
																<p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized.</p>
															</div>
														</div>
														<div class="carousel-item">
															<img class="d-block w-100" src="..//Nms_Simulator/resources/assets/img/news/img04.jpg" alt="Second slide">
															<div class="carouseltransparent d-none d-md-block"></div>
															<div class="carousel-caption d-none  d-md-block">
																<h5>Carousel Heading2</h5>
																<p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized.</p>
															</div>
														</div>
														<div class="carousel-item">
															<img class="d-block w-100" src="..//Nms_Simulator/resources/assets/img/news/img03.jpg" alt="Third slide">
															<div class="carouseltransparent d-none d-md-block"></div>
															<div class="carousel-caption d-none  d-md-block">
																<h5>Carousel Heading3</h5>
																<p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized.</p>
															</div>
														</div>
													</div>
													<a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
														<span class="carousel-control-prev-icon" aria-hidden="true"></span>
														<span class="sr-only">Previous</span>
													</a>
													<a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
														<span class="carousel-control-next-icon" aria-hidden="true"></span>
														<span class="sr-only">Next</span>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>-->
							<!--row closed-->
						</div>
					</section>
				</div>
				<!--app-content closed-->

				<!-- Side-bar -->
				<div class="sidebar sidebar-right sidebar-animate">
					<div class="m-3">
						<a href="#" class="text-right float-right" data-toggle="sidebar-right" data-target=".sidebar-right"><i class="fe fe-x"></i></a>
					</div>
					<div class="rightmenu ">
						<h5>Latest News</h5>
						<div class="p-2">
							<div class="latest-timeline">
								<ul class="timeline mb-0">
									<li class="mt-0">
										<a target="_blank" href="#">Sed ut perspiciatis </a>
										<a href="#" class="float-right text-dark">23 Apr, 2019</a>
										<p class="text-muted">sed do eiusmod tempor incididunt ut labore et dolore. </p>
									</li>
									<li>
										<a href="#">Nam libero tempore</a>
										<a href="#" class="float-right text-dark">21 Apr, 2019</a>
										<p class="text-muted">sed do eiusmod tempor  magna aliqua nisi ut aliquip. </p>
									</li>
									<li>
										<a href="#">Excepteur  cupidatat</a>
										<a href="#" class="float-right text-dark">18 Apr, 2019</a>
										<p class="text-muted">sed do eiusmod tempor incididunt ut labore  ut aliquip.</p>
									</li>
									<li class="mb-0">
										<a href="#">Neque porro est</a>
										<a href="#" class="float-right text-dark">17 Apr, 2019</a>
										<p class="text-muted">sed do eiusmod tempor  aliqua nisi ut aliquip . </p>
									</li>
								</ul>
							</div>
							<div class="text-center">
								<a href="#" class="btn btn-primary btn-block mb-4">Adding</a>
							</div>
						</div>
						<h5>Visitors</h5>
						<div class="card-body p-0">
							<ul class="visitor list-unstyled list-unstyled-border">
								<li class="media p-2 mb-0">
									<img class="mr-3 rounded-circle" width="50" src="/Nms_Simulator/resources/assets/img/avatar/6.jpg" alt="avatar">
									<div class="media-body">
										<div class="float-right"><small>10-4-2018</small></div>
										<div class="media-title">Blake Vanessa</div>
										<small>sed do eiusmod tempor incididunt ut labore</small>
									</div>
								</li>
								<li class="media p-2 mb-0">
									<img class="mr-3 rounded-circle" width="50" src="/Nms_Simulator/resources/assets/img/avatar/2.jpg" alt="avatar">
									<div class="media-body">
										<div class="float-right"><small>15-4-2018</small></div>
										<div class="media-title">Keith Rutherford</div>
										<small>sed do eiusmod tempor incididunt ut labore</small>
									</div>
								</li>
								<li class="media p-2 mb-0">
									<img class="mr-3 rounded-circle" width="50" src="/Nms_Simulator/resources/assets/img/avatar/3.jpg" alt="avatar">
									<div class="media-body">
										<div class="float-right"><small>17-4-2018</small></div>
										<div class="media-title">Elizabeth Parsons</div>
										<small>sed do eiusmod tempor incididunt ut labore</small>
									</div>
								</li>
								<li class="media border-b0 p-2 mb-0">
									<img class="mr-3 rounded-circle" width="50" src="/Nms_Simulator/resources/assets/img/avatar/4.jpg" alt="avatar">
									<div class="media-body">
										<div class="float-right"><small>22-3-2018</small></div>
										<div class="media-title">Nicola Lambert</div>
										<small>sed do eiusmod tempor incididunt ut labore</small>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!--side-bar Closed -->

				<!--Footer-->
				<footer class="main-footer hor-footer">
					<div class="text-center">
						Copyright &copy; NMS 2021. 
					</div>
				</footer>
				<!--/Footer-->

			</div>
		</div>
		<!--app closed-->

		<!-- Back to top -->
		<a href="#top" id="back-to-top" ><i class="fa fa-long-arrow-up"></i></a>

		<!-- ================== BEGIN BASE JS LEVEL ================== -->
		<jsp:include page="../simulator/commonJs.jsp" />
		<!-- ================== END PAGE JS LEVEL ================== -->

	</body>
</html>
