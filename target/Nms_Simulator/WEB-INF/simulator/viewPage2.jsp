<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Meta data -->
		<meta charset="UTF-8">
		<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
		<meta name="keywords" content="SMS Alert"/>

		<!-- Title -->
		<title>NMS Simulator | Dashboard</title>
		<!-- ================== BEGIN BASE CSS STYLE ================== -->
		<jsp:include page="../simulator/commonCss.jsp" />
		<!-- ================== END PAGE LEVEL STYLE ================== -->
	</head>

	<body class="app ">
		<!--loader -->
		<div id="spinner">
			<img src="/Nms_Simulator/resources/assets/img/svgs/loader.svg" alt="loader">
		</div>

		<!--app open-->
		<div id="app" class="page">
			<div class="main-wrapper" >
				<div class="top-header" >
					<!--nav open-->
					<nav class="navbar navbar-expand-lg main-navbar" style="background-color: #5e10ac;">
						<div class="container">
							<a id="horizontal-navtoggle" class="animated-arrow hor-toggle" ><span></span></a>
							<h3 style="font-color: white;color: white;padding-left: 352px;"><b>NMS SIMULATOR</b></h3>
							<a class="header-brand" href="index.html"></a>
						</div>
					</nav>
					<!--nav closed-->
				</div>

                <!--app-content open-->
				<div class="container app-content">
					<section class="section">
						<div class="page-header">
							<div>
								<h4 class="page_header_h4">
									<ol class="breadcrumb"><!-- breadcrumb -->
										
									</ol><!-- End breadcrumb -->
								</h4>							
							</div>
						</div> 
						<div class="section-body">
                            <!--row open-->
							<div class="row">
								<div class="col-12">
									<div class="card">
										<div class="card-header">
											<h4>NMS Simulator</h4>
										</div>
										<div class="card-body">
											<div id="accordion">
												<div class="accordion">
													<div class="accordion-header" data-toggle="collapse" data-target="#panel-body-1" >
														<h4>${auditLog.ipAddress}</h4>
													</div>
													<div class="accordion-body collapse show border border-top-0" id="panel-body-1" data-parent="#accordion" >
														<table>
														<tr>
															<td width="0px">Status : </td>
															<td width="350px">${auditLog.interfaceName} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="badge badge-success badge-pill">${auditLog.status}</span></td>
														</tr>
														<tr>
															<td>IP Address :</td>
															<td width="350px">${auditLog.ipAddress}</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td>
																<div class="row">
																	<div class="col-12">
																		<div class="card">
																			<div class="card-header">
																				<h4>User Activity</h4>
																			</div>
																			<div class="card-body">
																				<ul class="nav nav-tabs" id="myTab" role="tablist">
																					<li class="nav-item">
																						<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">View</a>
																					</li>
																					<li class="nav-item">
																						<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">BringUp&Upgrade</a>
									
																					</li>
									
																					<li class="nav-item">
									
																						<a class="nav-link" id="contact-tab" data-toggle="tab" href="#reboot" role="tab" aria-controls="contact" aria-selected="false">Reboot</a>
									
																					</li>
									<li class="nav-item">
									
																						<a class="nav-link" id="contact-tab" data-toggle="tab" href="#sendconfig" role="tab" aria-controls="contact" aria-selected="false">SendConfig</a>
																					</li>
									<li class="nav-item">
									
																						<a class="nav-link" id="contact-tab" data-toggle="tab" href="#dashboard" role="tab" aria-controls="contact" aria-selected="false">Dashboard</a>
																					</li>
									
																				</ul>

																			<div class="tab-content  p-3" id="myTabContent">
																				<div class="tab-pane fade show active p-0" id="home" role="tabpanel" aria-labelledby="home-tab">
																					<table>
																					<tr>
																					<td width="900px">
																					Request Recived time : 12/01/2021 14:30 
																					<td><span class="badge badge-success badge-pill">Success</span></td>
																					</tr>
																					<tr>
																					<td>&nbsp;</td>
																					</tr>
																					<tr>
																					<td width="900px">
																					Request Recived time : 12/01/2021 16:30
																					<td><span class="badge badge-secondary badge-pill">Failed</span></td>
																					</tr>
																					
																					</table>

																				</div>

																				<div class="tab-pane fade p-0" id="profile" role="tabpanel" aria-labelledby="profile-tab">
																					<table>
																						<tr>
																						<td width="900px">
																						Request Recived time : 12/01/2021 14:30 
																						<td><span class="badge badge-success badge-pill">Success</span></td>
																						</tr>
																						<tr>
																						<td>&nbsp;</td>
																						</tr>
																						<tr>
																						<td>Check List <ol>chmod 777 /opt/resonous/" + fileName + ""&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;></ol><ol>tar zxvf /opt/resonous/" + fileName + " -C /opt/resonous/&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;></ol></td>
																						
																						</tr>
																						<tr>
																						<td>&nbsp;</td>
																						</tr>
																						<tr>
																						<td width="900px">
																						Response time : 12/01/2021 14:40 Status: Successfully Updated
																						<!--<td><span class="badge badge-secondary badge-pill">Failed</span></td>-->
																						</tr>
																						
																					</table>
												</div>
												<div class="tab-pane fade p-0" id="reboot" role="tabpanel" aria-labelledby="contact-tab">
													<table>
<tr>
<td width="900px">
Request Recived time : 14/01/2021 11:30 
<td><span class="badge badge-success badge-pill">Success</span></td>
</tr>
<tr>
<td>Check List <ol>Reboot Commands &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;></ol>
	
</td>

</tr>
<tr>
<td width="900px">
Response time : 14/01/2021 14:35 Status: Rebooted Successfully
<!--<td><span class="badge badge-secondary badge-pill">Failed</span></td>-->
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>


</table>
												</div>
												<div class="tab-pane fade p-0" id="sendconfig" role="tabpanel" aria-labelledby="contact-tab">
													<table>
<tr>
<td width="900px">
Request Recived time : 12/01/2021 14:30 
<td><span class="badge badge-success badge-pill">Success</span></td>
</tr>
<tr>
<td>Check List <ol>"config" + hostIpAddress + ".xml" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;></ol>
	<ol>/opt/resonous/config/" + configFileName&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;></ol>
</td>

</tr>
<tr>
<td width="900px">
Response time : 12/01/2021 14:40 Status: SendConfig Updated Successfully
<!--<td><span class="badge badge-secondary badge-pill">Failed</span></td>-->
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td width="900px">
Request Recived time : 12/01/2021 11:35
<td><span class="badge badge-secondary badge-pill">Failed</span></td>
</tr>
<tr>
<td>Check List <ol>"config" + hostIpAddress + ".xml" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" unchecked disabled background-color:red;></ol>
	<ol>/opt/resonous/config/" + configFileName&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox"  id="vehicle1" name="vehicle1" value="Bike" background-color:red;></ol>
</td>

</tr>
<tr>
<td width="900px">
Response time : 12/01/2021 14:40 Status: SendConfig Updated Failed
<!--<td><span class="badge badge-secondary badge-pill">Failed</span></td>-->
</tr>
<tr>


</table>
												</div>
												<div class="tab-pane fade p-0" id="dashboard" role="tabpanel" aria-labelledby="contact-tab">
													<table>
<tr>
<td width="900px">
Request Recived time : 12/01/2021 14:30 
<td><span class="badge badge-success badge-pill">Success</span></td>
</tr>
<tr>
<td>Check List <ol>dfe_power_temperature_get all &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;></ol>
	<ol>dfe_frequency_get &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" checked disabled background-color:red;></ol>
</td>

</tr>
<tr>
<td width="900px">
Response time : 12/01/2021 14:40 
<!--<td><span class="badge badge-secondary badge-pill">Failed</span></td>-->
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>


</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div></td>

</tr>
</table>
													</div>
												</div>
												</div>
												</div>
												</div>
												</div>
												</div>
												</div>
												
												
					</section>
				</div>
				<!--app-content closed-->

				<!-- Side-bar -->
				<div class="sidebar sidebar-right sidebar-animate">
					<div class="m-3">
						<a href="#" class="text-right float-right" data-toggle="sidebar-right" data-target=".sidebar-right"><i class="fe fe-x"></i></a>
					</div>
					<div class="rightmenu ">
						<h5>Latest News</h5>
						<div class="p-2">
							<div class="latest-timeline">
								<ul class="timeline mb-0">
									<li class="mt-0">
										<a target="_blank" href="#">Sed ut perspiciatis </a>
										<a href="#" class="float-right text-dark">23 Apr, 2019</a>
										<p class="text-muted">sed do eiusmod tempor incididunt ut labore et dolore. </p>
									</li>
									<li>
										<a href="#">Nam libero tempore</a>
										<a href="#" class="float-right text-dark">21 Apr, 2019</a>
										<p class="text-muted">sed do eiusmod tempor  magna aliqua nisi ut aliquip. </p>
									</li>
									<li>
										<a href="#">Excepteur  cupidatat</a>
										<a href="#" class="float-right text-dark">18 Apr, 2019</a>
										<p class="text-muted">sed do eiusmod tempor incididunt ut labore  ut aliquip.</p>
									</li>
									<li class="mb-0">
										<a href="#">Neque porro est</a>
										<a href="#" class="float-right text-dark">17 Apr, 2019</a>
										<p class="text-muted">sed do eiusmod tempor  aliqua nisi ut aliquip . </p>
									</li>
								</ul>
							</div>
							<div class="text-center">
								<a href="#" class="btn btn-primary btn-block mb-4">Adding</a>
							</div>
						</div>
						<h5>Visitors</h5>
						<div class="card-body p-0">
							<ul class="visitor list-unstyled list-unstyled-border">
								<li class="media p-2 mb-0">
									<img class="mr-3 rounded-circle" width="50" src="/Nms_Simulator/resources/assets/img/avatar/6.jpg" alt="avatar">
									<div class="media-body">
										<div class="float-right"><small>10-4-2018</small></div>
										<div class="media-title">Blake Vanessa</div>
										<small>sed do eiusmod tempor incididunt ut labore</small>
									</div>
								</li>
								<li class="media p-2 mb-0">
									<img class="mr-3 rounded-circle" width="50" src="/Nms_Simulator/resources/assets/img/avatar/2.jpg" alt="avatar">
									<div class="media-body">
										<div class="float-right"><small>15-4-2018</small></div>
										<div class="media-title">Keith Rutherford</div>
										<small>sed do eiusmod tempor incididunt ut labore</small>
									</div>
								</li>
								<li class="media p-2 mb-0">
									<img class="mr-3 rounded-circle" width="50" src="/Nms_Simulator/resources/assets/img/avatar/3.jpg" alt="avatar">
									<div class="media-body">
										<div class="float-right"><small>17-4-2018</small></div>
										<div class="media-title">Elizabeth Parsons</div>
										<small>sed do eiusmod tempor incididunt ut labore</small>
									</div>
								</li>
								<li class="media border-b0 p-2 mb-0">
									<img class="mr-3 rounded-circle" width="50" src="/Nms_Simulator/resources/assets/img/avatar/4.jpg" alt="avatar">
									<div class="media-body">
										<div class="float-right"><small>22-3-2018</small></div>
										<div class="media-title">Nicola Lambert</div>
										<small>sed do eiusmod tempor incididunt ut labore</small>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!--side-bar Closed -->

				<!--Footer-->
				<footer class="main-footer hor-footer">
					<div class="text-center">
						Copyright &copy; NMS 2021. 
					</div>
				</footer>
				<!--/Footer-->

			</div>
		</div>
		<!--app closed-->

		<!-- Back to top -->
		<a href="#top" id="back-to-top" ><i class="fa fa-long-arrow-up"></i></a>

		<!-- ================== BEGIN BASE JS LEVEL ================== -->
		<jsp:include page="../simulator/commonJs.jsp" />
		<!-- ================== END PAGE JS LEVEL ================== -->

	</body>
</html>
