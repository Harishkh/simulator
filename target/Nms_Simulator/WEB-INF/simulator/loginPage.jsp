
<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Meta data -->
		<meta charset="UTF-8">
		<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
		<meta name="keywords" content="SMS Alert"/>

		<!-- Title -->
		<title>NMS Simulator | Login</title>
		<!-- ================== BEGIN BASE CSS STYLE ================== -->
		<jsp:include page="../simulator/commonCss.jsp" />
		<!-- ================== END PAGE LEVEL STYLE ================== -->
	</head>

	<body class="app ">
		<!--loader -->
		<!-- <div id="spinner">
			<img src="/Nms_Simulator/resources/assets/img/svgs/loader.svg" alt="loader">
		</div>
 -->
		<!--app open-->
		<div id="app" class="page">
			<div class="main-wrapper" >
				<!--top-header open -->
				<div class="top-header" >
					<!--nav open-->
					<nav class="navbar navbar-expand-lg main-navbar" style="background-color: #5e10ac;">
						<div class="container">
							<a id="horizontal-navtoggle" class="animated-arrow hor-toggle" ><span></span></a>
							<h3 style="font-color: white;color: white;padding-left: 352px;"><b>NMS SIMULATOR</b></h3>
							<a class="header-brand" href="index.html"></a>
						</div>
					</nav>
					<!--nav closed-->
				</div>
				<!--top-header close -->
				
				
                <!--app-content open-->
				<div class="container app-content">
					<section class="section">
					<div class="page-header">
							<div>
								<h4 class="page_header_h4">
									<ol class="breadcrumb"><!-- breadcrumb -->
										
									</ol><!-- End breadcrumb -->
								</h4>							
							</div>
						</div> 
						<div class="section-body">
                            <!--row open-->
							<div class="row">
								<div class="col-12">
									<div class="card">
										<div class="card-header">
											<h4>NMS Simulator</h4>
										</div>
										<!--card-body open-->
										<div class="card-body">
											<div id="accordion">
												<div class="accordion">
													<div class="accordion-header" data-toggle="" data-target="#panel-body-1" style="background-color: #1997c7;">
														<h4 style="font-color: white; color: white;">List of Users</h4>
													</div>
													<div class="accordion-body collapse show border border-top-0" id="panel-body-1" data-parent="#accordion" >
														 <table id="data-table" class="table table-striped table-bordered">
															<thead>
			                                    				<tr>
			                                    					<th>Connected User</th>
			                                        				<th>IP Address</th>
			                                         				<th>Action</th>
			                                   					</tr>
			                                				</thead>
			                                				 <tbody>
			                                				 	<tr>
			                                				 		<td>NMS</td>
		                                				 			<td>192.168.1.5</td>
			                                				 		<td>
			                                				 			<a href="<%=request.getContextPath()%>/viewPage"><span class="badge badge-success badge-pill" >View</span></a>
			                                				 		</td>
			                                				 	</tr>
			                                				 	
			                                				 </tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
										<!--card-body close-->
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
				<!--app-content closed-->

				<!--Footer-->
				<footer class="main-footer hor-footer">
					<div class="text-center">
						Copyright &copy; NMS 2021. 
					</div>
				</footer>
				<!--/Footer-->

			</div>
		</div>
		<!--app closed-->

		<!-- Back to top -->
		<!-- <a href="#top" id="back-to-top" ><i class="fa fa-long-arrow-up"></i></a> -->

		<!-- ================== BEGIN BASE JS LEVEL ================== -->
		<jsp:include page="../simulator/commonJs.jsp" />
		<!-- ================== END PAGE JS LEVEL ================== -->

	</body>
</html>
